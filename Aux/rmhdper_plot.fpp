# include "define.inc"

program rmhdper_plot

  use file_utils, only: init_file_utils, finish_file_utils, get_unused_unit
  use file_utils, only: run_name
  use run_params, only: init_run_params, finish_run_params
  use fft, only: init_fft, finish_fft
  use four, only: init_four, finish_four
  use command_line, only: cl_getarg
  use pgplot_utils, only: pgkind
  use fields_plot_utils, only: init_datafile, finish_datafile
  use fields_plot_utils, only: init_readvar
  use fields_plot_utils, only: get_boxsize, get_dimensions
  use fields_plot_utils, only: get_field, get_time
  use fields_plot_utils, only: init_plot, finish_plot
  use fields_plot_utils, only: set_viewport, set_coordinate
  use fields_plot_utils, only: newpage, newpanel
  use fields_plot_utils, only: get_minmax
  use fields_plot_utils, only: infinity
  use netcdf_utils, only: kind_nf

  implicit none

  integer, parameter :: stdout=6, stderr=0

  integer :: i, j
  integer :: nkx, nky, nx, ny
  integer :: itime, ntime
  integer :: istatus
  integer, parameter :: nvar_field=4 ! number of field variables
  integer :: nvar_field_plot=nvar_field ! number of field variables to plot

  real :: time
  real :: lx, ly
  real (kind=pgkind), allocatable :: xp(:)
  real, allocatable :: field_var(:,:)

  character (len=100) :: filename_in, filename_out(2)

  logical :: do_field_plot=.false.
  logical :: do_field_cut_plot=.false.

  integer (kind=kind_nf) :: file_id 
# ifdef NETCDF
  integer (kind=kind_nf) :: time_id
  integer (kind=kind_nf) :: var_field_id(nvar_field), var_eq_id(nvar_field)
# else
# endif

  integer :: pg_ipanel
  integer, parameter :: pg_npanel_field=4
  integer :: pg_ptr_field(pg_npanel_field)=(/ (i,i=1,pg_npanel_field) /)
  integer :: pg_nrow_field=1
  integer :: pg_icut_field(pg_npanel_field)=(/ (1,i=1,pg_npanel_field) /)

  integer :: pg_idx

  character (len=100) :: pg_txttl ! title
  character (len=100) :: pg_xlab='\fix/\gr\d\fri\fi\u'
  character (len=100) :: pg_ylab='\fiy/\gr\d\fri\fi\u'
  character (len=200) :: pg_tlab
  character (len=100) :: pg_tlab_field(nvar_field)= (/ &
       & '\gf ', '\gw ', '\gq ', '\fiJ' /)
  character (len=2) :: pg_cwdg='BI' ! wedge
  !  logical, parameter :: pg_crange_global = .true.
  logical :: pg_crange_global = .false.
  real (kind=pgkind) :: pg_trc(6) ! transformation matrix
  real (kind=pgkind) :: pg_wedge(2) ! wedge position and height
  real (kind=pgkind), allocatable :: pg_var(:,:)
  real (kind=pgkind) :: pg_cmin, pg_cmax, pg_cminmax
  real (kind=pgkind) :: pg_cmin_global_field(nvar_field)
  real (kind=pgkind) :: pg_cmax_global_field(nvar_field)
  real (kind=pgkind) :: pg_field_min, pg_field_max
  integer, parameter :: pg_nc=20
  real (kind=pgkind) :: pg_clev(pg_nc), pg_dc
  real (kind=pgkind) :: pg_ccrop(4)=(/ -infinity,-infinity,infinity,infinity /)
  integer :: pg_nccrop(4)
  ! transform origin to center from left-bottom corner
  logical :: origin_center=.false.
  ! keep aspect raio
  logical :: keep_aspect=.true.
  ! plot perturbed part only
  logical :: perturbed=.false.
  logical :: list
  character (len=100) :: comment=''

  namelist /field_plot/ &
       & nvar_field_plot, &
       & pg_ptr_field, &
       & do_field_plot, &
       & do_field_cut_plot, &
       & pg_ccrop, &
       & keep_aspect, perturbed, origin_center, pg_crange_global

  read(5,field_plot)

  ! read parameter and initialize fft
  call init_file_utils(list)
  call init_run_params
  call init_fft
  call init_four

  filename_in=trim(run_name)//'.nc'
  filename_out(1)=trim(run_name)//'.ps/cps'
  filename_out(2)=trim(run_name)//'_cut.ps/cps'

  ! open file
  call init_datafile(filename_in,file_id)

  ! get dimensions
  call get_dimensions(file_id,nkx,nky,nx,ny)

  ! get box size
  call get_boxsize(file_id,lx,ly)

  ! initialize reading variables
  call init_readvar(file_id,time_id,ntime,var_field_id,var_eq_id)

  ! allocate coordinates and field variables
  allocate(pg_var(nx+1,ny+1),stat=istatus)
  if (istatus /= 0)  stop 'allocation error'
  allocate(field_var(nx,ny),stat=istatus)
  if (istatus /= 0)  stop 'allocation error'

!!! field plot
  if(do_field_plot) then

     ! setup pgplot
     call init_plot(filename_out(1))
     call set_viewport(nvar_field_plot, nvar_field_plot, pg_cwdg, pg_wedge)
     call set_coordinate(lx,ly,nx,ny,pg_trc,origin_center,pg_ccrop,pg_nccrop)
     ! contour range global
     if(pg_crange_global) then
        pg_cmin_global_field(1:nvar_field)=+infinity
        pg_cmax_global_field(1:nvar_field)=-infinity
        do itime = 1, ntime
           do pg_idx=1,nvar_field
              call get_field(file_id,itime, &
                   & var_field_id(pg_idx), var_eq_id(pg_idx), &
                   & nkx,nky,nx,ny,pg_ptr_field(pg_idx),field_var, &
                   & perturbed,origin_center)
              call get_minmax(field_var,pg_field_min,pg_field_max)
              pg_cmin_global_field(pg_idx)= &
                   & min(pg_cmin_global_field(pg_idx),pg_field_min)
              pg_cmax_global_field(pg_idx)= &
                   & max(pg_cmax_global_field(pg_idx),pg_field_max)
           end do
        end do
     endif

     do itime = 1, ntime
        ! get time
        call get_time(file_id,itime,time_id,time)
        write(stdout,*) 'time = ',time
        write(pg_txttl,'(a,f10.4,a)') 'time = ',time,' [\fiL/\fiv\d\frth\u]'

        ! new page and title
        call newpage(pg_txttl, showgrid=.false.)

        ! make pg_npanel panels in one page 
        do pg_ipanel=1,nvar_field_plot

           ! get variables
           call get_field(file_id,itime, &
                & var_field_id(pg_ptr_field(pg_ipanel)), &
                & var_eq_id(pg_ptr_field(pg_ipanel)), &
                & nkx,nky,nx,ny,pg_ptr_field(pg_ipanel),field_var, &
                & perturbed,origin_center)
           call get_minmax(field_var,pg_field_min,pg_field_max)

           pg_var(1:nx,1:ny)=field_var(1:nx,1:ny)

           ! apply periodic boundary condition
           pg_var(  nx+1,1:ny)= pg_var(1     ,1:ny)
           pg_var(1:nx+1,ny+1)= pg_var(1:nx+1,1   )

           if(pg_crange_global) then
              pg_cmin=pg_cmin_global_field(pg_ptr_field(pg_ipanel))
              pg_cmax=pg_cmax_global_field(pg_ptr_field(pg_ipanel))
           else
              pg_cmin=pg_field_min
              pg_cmax=pg_field_max
           end if

           pg_dc=(pg_cmax-pg_cmin)/(pg_nc-1)
           do i=1,pg_nc
              pg_clev(i)=pg_cmin+pg_dc*(i-1)
           end do
           call newpanel(pg_ipanel,pg_xlab,pg_ylab, &
                & pg_tlab_field(pg_ptr_field(pg_ipanel)),keep_aspect)

           call pgimag(pg_var(1:nx+1,1:ny+1),nx+1,ny+1, &
                & 1,nx+1,1,ny+1, &
                & pg_cmin,pg_cmax,pg_trc)

           call pgcont(pg_var(1:nx+1,1:ny+1),nx+1,ny+1, &
                & pg_nccrop(1), pg_nccrop(3), pg_nccrop(2), pg_nccrop(4), &
                & pg_clev,pg_nc,pg_trc)
           call pgwedg(pg_cwdg,pg_wedge(1),pg_wedge(2),pg_cmin,pg_cmax,'')
        end do

     end do

     ! finish plot
     call finish_plot

  end if

!!! field cut
  if(do_field_cut_plot) then

     ! setup pgplot
     call init_plot(filename_out(2))
     call set_viewport(nvar_field_plot,nvar_field_plot, pg_cwdg, pg_wedge)
     call set_coordinate(lx,ly,nx,ny,pg_trc,origin_center,pg_ccrop,pg_nccrop)
     if(.not.allocated(xp)) allocate(xp(nx+1))
     pg_icut_field(1:pg_npanel_field)=(/ ny/4+1, ny/4+1, ny/2+1, ny/2+1 /)

     ! contour range global
     if(pg_crange_global) then
        pg_cmin_global_field(1:nvar_field)=+infinity
        pg_cmax_global_field(1:nvar_field)=-infinity
        do itime = 1, ntime
           do pg_idx=1,nvar_field
              call get_field(file_id,itime, &
                   & var_field_id(pg_idx), var_eq_id(pg_idx), &
                   & nkx,nky,nx,ny,pg_ptr_field(pg_idx),field_var, &
                   & perturbed,origin_center)
              call get_minmax( &
                   & field_var( &
                   & pg_nccrop(1):min(nx,pg_nccrop(3)), &
                   & pg_icut_field(pg_idx):pg_icut_field(pg_idx)), & 
                   & pg_field_min,pg_field_max)
              pg_cmin_global_field(pg_idx)= &
                   & min(pg_cmin_global_field(pg_idx),pg_field_min)
              pg_cmax_global_field(pg_idx)= &
                   & max(pg_cmax_global_field(pg_idx),pg_field_max)
           end do
        end do
     endif

     do itime = 1, ntime
        ! get time
        call get_time(file_id,itime,time_id,time)
        write(stdout,*) 'time = ',time
        write(pg_txttl,'(a,f10.4,a)') 'time = ',time,' [\fiL/\fiv\d\frth\u]'

        ! new page and title
        call newpage(pg_txttl, showgrid=.false.)
        call pgbox('BCNST',0._pgkind,0,'BCNST',0._pgkind,0)

        ! make pg_npanel panels in one page 
        do pg_ipanel=1,nvar_field_plot

           ! get variables
           call get_field(file_id,itime, &
                & var_field_id(pg_ptr_field(pg_ipanel)), &
                & var_eq_id(pg_ptr_field(pg_ipanel)), &
                & nkx,nky,nx,ny,pg_ptr_field(pg_ipanel),field_var, &
                & perturbed,origin_center)
           call get_minmax( &
                & field_var(pg_nccrop(1):min(nx,pg_nccrop(3)),&
                & pg_icut_field(pg_ipanel):pg_icut_field(pg_ipanel)), &
                & pg_field_min,pg_field_max)

           pg_var(1:nx,1:ny)=field_var(1:nx,1:ny)

           ! apply periodic boundary condition
           pg_var(  nx+1,1:ny)= pg_var(1     ,1:ny)
           pg_var(1:nx+1,ny+1)= pg_var(1:nx+1,1   )

           if(pg_crange_global) then
              pg_cmin=pg_cmin_global_field(pg_ptr_field(pg_ipanel))
              pg_cmax=pg_cmax_global_field(pg_ptr_field(pg_ipanel))
           else
              pg_cmin=pg_field_min
              pg_cmax=pg_field_max
           end if
           
           if(origin_center) then
              xp(1:nx+1)=(/ ((i-1)*lx/nx-.5*lx,i=1,nx+1) /)
           else
              xp(1:nx+1)=(/ ((i-1)*lx/nx,i=1,nx+1) /)
           end if
           if(pg_cmax<=pg_cmin) pg_cmax=pg_cmin+1.
           call newpanel(pg_ipanel,pg_xlab, &
                & pg_tlab_field(pg_ptr_field(pg_ipanel)), &
                & '',.false., &
                & (/ xp(pg_nccrop(1)),xp(pg_nccrop(3)),pg_cmin,pg_cmax/))
           call pgline( &
                & pg_nccrop(3)-pg_nccrop(1)+1, &
                & xp(pg_nccrop(1):pg_nccrop(3)), &
                & pg_var(pg_nccrop(1):pg_nccrop(3),pg_icut_field(pg_ipanel)))
        end do

     end do

     deallocate(xp)
     ! finish plot
     call finish_plot

  endif

  ! finish fft, datafile
  call finish_datafile(file_id)

  ! deallocate field arrays
  deallocate(pg_var,stat=istatus)
  if (istatus /= 0)  stop 'deallocation error'
  deallocate(field_var,stat=istatus)
  if (istatus /= 0)  stop 'deallocation error'

  call finish_four
  call finish_fft
  call finish_run_params
  call finish_file_utils

  ! done
  stop

end program rmhdper_plot
