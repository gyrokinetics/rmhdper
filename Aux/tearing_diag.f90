module vars
  implicit none
  public :: aphi,apsi,vor,cur
  public :: aphi_pt,apsi_pt,vor_pt,cur_pt
  public :: xsheet, ysheet
  private

  complex, allocatable :: aphi(:,:),apsi(:,:),vor(:,:),cur(:,:)
  complex, allocatable :: aphi_pt(:,:),apsi_pt(:,:),vor_pt(:,:),cur_pt(:,:)
  real :: xsheet=.5,ysheet=.5

end module vars
  
module funcs
  implicit none
  public :: jpt_x, djpt_x
  public :: jfunc
  public :: psifunc
  public :: dpsifunc_dx, dpsifunc_dy
  public :: ddpsifunc_dxdx, ddpsifunc_dxdy, ddpsifunc_dydy
  public :: sub_dcur, sub_dcur_hess
  public :: sub_dpsi, sub_dpsi_hess
  private

contains

  subroutine sub_dcur(pos,val)
    real, intent(in) :: pos(:)
    real, intent(out) :: val(:)
    val(1)=djfunc_dx(pos(1),pos(2))
    val(2)=djfunc_dy(pos(1),pos(2))
  end subroutine sub_dcur

  subroutine sub_dcur_hess(pos,val)
    real, intent(in) :: pos(:)
    real, intent(out) :: val(:,:)
    val(1,1)=ddjfunc_dxdx(pos(1),pos(2))
    val(1,2)=ddjfunc_dxdy(pos(1),pos(2))
    val(2,1)=ddjfunc_dxdy(pos(1),pos(2))
    val(2,2)=ddjfunc_dydy(pos(1),pos(2))
  end subroutine sub_dcur_hess

  function jfunc(x,y)
    use vars, only: cur
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: jfunc
    real, intent(in) :: x,y
    integer :: it
    jfunc=0.
    do it=1,nkx
       jfunc=jfunc &
            & + real(cur(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & + sum(real(2.*cur(it,2:nky)*exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function jfunc

  function djfunc_dx(x,y)
    use vars, only: cur
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: djfunc_dx
    real, intent(in) :: x,y
    integer :: it
    djfunc_dx=0.
    do it=1,nkx
       djfunc_dx=djfunc_dx &
            & + real(zi*kx(it)*cur(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & + sum(real(2.*zi*kx(it)*cur(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function djfunc_dx

  function djfunc_dy(x,y)
    use vars, only: cur
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: djfunc_dy
    real, intent(in) :: x,y
    integer :: it
    djfunc_dy=0.
    do it=1,nkx
       djfunc_dy=djfunc_dy &
            & + real(zi*ky(1)*cur(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & + sum(real(2.*zi*ky(2:nky)*cur(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function djfunc_dy

  function ddjfunc_dxdx(x,y)
    use vars, only: cur
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: ddjfunc_dxdx
    real, intent(in) :: x,y
    integer :: it
    ddjfunc_dxdx=0.
    do it=1,nkx
       ddjfunc_dxdx=ddjfunc_dxdx &
            & - real(kx(it)**2*cur(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & - sum(real(2.*kx(it)**2*cur(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function ddjfunc_dxdx

  function ddjfunc_dxdy(x,y)
    use vars, only: cur
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: ddjfunc_dxdy
    real, intent(in) :: x,y
    integer :: it
    ddjfunc_dxdy=0.
    do it=1,nkx
       ddjfunc_dxdy=ddjfunc_dxdy &
            & - real(kx(it)*ky(1)*cur(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & - sum(real(2.*kx(it)*ky(2:nky)*cur(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function ddjfunc_dxdy

  function ddjfunc_dydy(x,y)
    use vars, only: cur
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: ddjfunc_dydy
    real, intent(in) :: x,y
    integer :: it
    ddjfunc_dydy=0.
    do it=1,nkx
       ddjfunc_dydy=ddjfunc_dydy &
            & - real(ky(1)**2*cur(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & - sum(real(2.*ky(2:nky)**2*cur(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function ddjfunc_dydy

  function jpt_x(x)
    use vars, only: cur_pt,ysheet
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: jpt_x
    real, intent(in) :: x
    integer :: it
    jpt_x=0.
    do it=1,nkx
       jpt_x=jpt_x &
            & + real(cur_pt(it,1)*exp(zi*(kx(it)*x+ky(1)*ysheet))) &
            & + sum(real(2.*cur_pt(it,2:nky)*exp(zi*(kx(it)*x+ky(2:nky)*ysheet))))
    end do
  end function jpt_x

  function djpt_x(x)
    use vars, only: cur_pt,ysheet
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: djpt_x
    real, intent(in) :: x
    integer :: it
    djpt_x=0.
    do it=1,nkx
       djpt_x=djpt_x &
            & + real(zi*kx(it)*cur_pt(it,1)*exp(zi*(kx(it)*x+ky(1)*ysheet))) &
            & + sum(real(2.*zi*kx(it)*cur_pt(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*ysheet))))
    end do
  end function djpt_x

  function psifunc(x,y)
    use vars, only: apsi
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: psifunc
    real, intent(in) :: x,y
    integer :: it
    psifunc=0.
    do it=1,nkx
       psifunc=psifunc &
            & + real(apsi(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & + sum(real(2.*apsi(it,2:nky)*exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function psifunc

  function dpsifunc_dx(x,y)
    use vars, only: apsi
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: dpsifunc_dx
    real, intent(in) :: x,y
    integer :: it
    dpsifunc_dx=0.
    do it=1,nkx
       dpsifunc_dx=dpsifunc_dx &
            & + real(zi*kx(it)*apsi(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & + sum(real(2.*zi*kx(it)*apsi(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function dpsifunc_dx

  function dpsifunc_dy(x,y)
    use vars, only: apsi
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: dpsifunc_dy
    real, intent(in) :: x,y
    integer :: it
    dpsifunc_dy=0.
    do it=1,nkx
       dpsifunc_dy=dpsifunc_dy &
            & + real(zi*ky(1)*apsi(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & + sum(real(2.*zi*ky(2:nky)*apsi(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function dpsifunc_dy

  function ddpsifunc_dxdx(x,y)
    use vars, only: apsi
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: ddpsifunc_dxdx
    real, intent(in) :: x,y
    integer :: it
    ddpsifunc_dxdx=0.
    do it=1,nkx
       ddpsifunc_dxdx=ddpsifunc_dxdx &
            & - real(kx(it)**2*apsi(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & - sum(real(2.*kx(it)**2*apsi(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function ddpsifunc_dxdx

  function ddpsifunc_dxdy(x,y)
    use vars, only: apsi
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: ddpsifunc_dxdy
    real, intent(in) :: x,y
    integer :: it
    ddpsifunc_dxdy=0.
    do it=1,nkx
       ddpsifunc_dxdy=ddpsifunc_dxdy &
            & - real(kx(it)*ky(1)*apsi(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & - sum(real(2.*kx(it)*ky(2:nky)*apsi(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function ddpsifunc_dxdy

  function ddpsifunc_dydy(x,y)
    use vars, only: apsi
    use constants, only: zi,pi
    use fft, only: nkx,nky
    use four, only: kx,ky
    real :: ddpsifunc_dydy
    real, intent(in) :: x,y
    integer :: it
    ddpsifunc_dydy=0.
    do it=1,nkx
       ddpsifunc_dydy=ddpsifunc_dydy &
            & - real(ky(1)**2*apsi(it,1)*exp(zi*(kx(it)*x+ky(1)*y))) &
            & - sum(real(2.*ky(2:nky)**2*apsi(it,2:nky) &
            & * exp(zi*(kx(it)*x+ky(2:nky)*y))))
    end do
  end function ddpsifunc_dydy

  subroutine sub_dpsi(pos,val)
    real, intent(in) :: pos(:)
    real, intent(out) :: val(:)
    val(1)=dpsifunc_dx(pos(1),pos(2))
    val(2)=dpsifunc_dy(pos(1),pos(2))
  end subroutine sub_dpsi
  
  subroutine sub_dpsi_hess(pos,val)
    real, intent(in) :: pos(:)
    real, intent(out) :: val(:,:)
    val(1,1)=ddpsifunc_dxdx(pos(1),pos(2))
    val(1,2)=ddpsifunc_dxdy(pos(1),pos(2))
    val(2,1)=ddpsifunc_dxdy(pos(1),pos(2))
    val(2,2)=ddpsifunc_dydy(pos(1),pos(2))
  end subroutine sub_dpsi_hess
  
end module funcs

program tearing_diag
  use constants, only: pi
  use netcdf, only: NF90_NOWRITE, NF90_NOERR
  use netcdf, only: nf90_open
  use netcdf, only: nf90_inquire_dimension, nf90_inq_dimid
  use netcdf, only: nf90_inq_varid, nf90_get_var
  use netcdf_utils, only: kind_nf, netcdf_error
  use file_utils, only: init_file_utils, run_name, stdout=>stdout_unit
  use file_utils, only: get_unused_unit
  use run_params, only: init_run_params, nx, ny
  use four, only: init_four, kperp2, laplacian
  use fft, only: init_fft, ktox, xtok
  use convert, only: r2c

  use vars, only: aphi,apsi,vor,cur,xsheet,ysheet
  use vars, only: aphi_pt,apsi_pt,vor_pt,cur_pt
  use funcs, only: jpt_x, djpt_x
  use funcs, only: jfunc
  use funcs, only: sub_dcur, sub_dcur_hess
  use funcs, only: sub_dpsi, sub_dpsi_hess
  use funcs, only: psifunc
  use funcs, only: dpsifunc_dx, dpsifunc_dy
  use funcs, only: ddpsifunc_dxdx, ddpsifunc_dxdy, ddpsifunc_dydy
  
  implicit none

  integer (kind=kind_nf) :: file_id
  integer (kind=kind_nf) :: kxdimid,kydimid
  integer (kind=kind_nf) :: kxvarid,kyvarid
  integer (kind=kind_nf) :: time_id, tdim_id
  integer (kind=kind_nf) :: aphi_id,apsi_id,aphi_eq_id,apsi_eq_id

  integer :: nkx,nky,nnx,nny
  integer :: itime,ntime
  integer :: i,j
  integer :: unit
  integer :: istatus
  integer :: xcut1,xcut2,xcut3,xcut4,ycut1,ycut2,ycut3,ycut4

  real, parameter :: del=1.e-3
  real :: width,width_guess,jmax_guess,xjmax,width2
  real :: time
  real :: dkx,dky,lx,ly,x,y
  real, allocatable :: aphi_xy(:,:),apsi_xy(:,:)
  real, allocatable :: vor_xy(:,:),cur_xy(:,:)
  real, allocatable :: aphi_pt_xy(:,:),apsi_pt_xy(:,:)
  real, allocatable :: vor_pt_xy(:,:),cur_pt_xy(:,:)
  real, allocatable :: aphi_ri(:,:,:),apsi_ri(:,:,:)
  real, allocatable :: aphi_eq_ri(:,:,:),apsi_eq_ri(:,:,:)
  real, allocatable :: apsi_old(:,:)
  real :: growth, time_old, apsi_pt_old, apsi_X, island_width, island_width2, epar_C, epar_X
  integer :: xpoint_x, xpoint_y
  real :: xp_guess(2), xp_sol(2), val(2)
  character (len=300) :: filename
  character (len=300) :: fname_cur_deb
  logical :: list, ex
  logical :: debug=.false.

  call init_file_utils(list)
  call init_run_params
  call init_fft
  call init_four

  xcut1=1
  xcut2=nx/4+1
  xcut3=nx/2+1
  xcut4=nx/2+nx/4+1
  ycut1=1
  ycut2=ny/4+1
  ycut3=ny/2+1
  ycut4=ny/2+ny/4+1

  filename=trim(trim(run_name)//'.nc')
  istatus=nf90_open(filename, NF90_NOWRITE, file_id)
  if (istatus /= NF90_NOERR) call netcdf_error (istatus, file_id)
  if(debug) write(stdout,*) 'DEBUG:: input file ', &
       & 'filename= ', trim(filename), &
       & ' file_id= ', file_id

  istatus = nf90_inq_dimid (file_id, "kx", kxdimid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dim='kx')
  istatus = nf90_inq_dimid (file_id, "ky", kydimid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dim='ky')
  istatus = nf90_inquire_dimension (file_id, kxdimid, len=nkx)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dimid=kxdimid)
  istatus = nf90_inquire_dimension (file_id, kydimid, len=nky)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dimid=kydimid)

  istatus = nf90_inq_varid (file_id, "kx", kxvarid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='kx')
  istatus = nf90_inq_varid (file_id, "ky", kyvarid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='ky')
  istatus = nf90_get_var (file_id, kxvarid, dkx, start=(/ 2 /))
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, kxvarid)
  istatus = nf90_get_var (file_id, kyvarid, dky, start=(/ 2 /))
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, kyvarid)
  lx=2.*pi/dkx; ly=2.*pi/dky
  xsheet=xsheet*lx
  ysheet=ysheet*ly

  if(debug) write(stdout,*) 'DEBUG:: inquire grid ', &
       & 'kxdimid= ', kxdimid, &
       & ' kydimid= ', kydimid, &
       & ' nkx =     ', nkx, &
       & ' nky =     ', nky, &
       & ' dkx =     ', dkx, &
       & ' dky =     ', dky, &
       & ' lx =     ', lx, &
       & ' ly =     ', ly

  istatus = nf90_inq_varid (file_id, "time", time_id)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='time')

  istatus = nf90_inq_dimid (file_id, "t", tdim_id)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='t')
  istatus = nf90_inquire_dimension (file_id, tdim_id, len=ntime)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dimid=tdim_id)
  if(debug) write(stdout,*) 'DEBUG:: inquire time ', &
       & 'time_id', time_id, &
       & ' ntime', ntime

  istatus = nf90_inq_varid (file_id, 'aphi', aphi_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='aphi')
  if(debug) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'aphi_id', aphi_id
  istatus = nf90_inq_varid (file_id, 'apsi', apsi_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='apsi')
  if(debug) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'apsi_id', apsi_id
  istatus = nf90_inq_varid (file_id, 'aphi_eq', aphi_eq_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='aphi_eq')
  if(debug) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'aphi_eq_id', aphi_eq_id
  istatus = nf90_inq_varid (file_id, 'apsi_eq', apsi_eq_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='apsi_eq')
  if(debug) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'apsi_eq_id', apsi_eq_id

  if(nx>nkx) then
     nnx=nx
  else
     nnx=(3*nkx+1)/2
  endif
  if(ny>nky) then
     nny=ny
  else
     nny=3*nky
  endif

  allocate(aphi_xy(1:ny+1,1:nx+1)); aphi_xy=0.
  allocate(apsi_xy(1:ny+1,1:nx+1)); apsi_xy=0.
  allocate(vor_xy(1:ny+1,1:nx+1)); vor_xy=0.
  allocate(cur_xy(1:ny+1,1:nx+1)); cur_xy=0.

  allocate(aphi_pt_xy(1:ny+1,1:nx+1)); aphi_pt_xy=0.
  allocate(apsi_pt_xy(1:ny+1,1:nx+1)); apsi_pt_xy=0.
  allocate(vor_pt_xy(1:ny+1,1:nx+1)); vor_pt_xy=0.
  allocate(cur_pt_xy(1:ny+1,1:nx+1)); cur_pt_xy=0.

  allocate(aphi_ri(1:2,1:nkx,1:nky)); aphi_ri=0.
  allocate(apsi_ri(1:2,1:nkx,1:nky)); apsi_ri=0.
  allocate(aphi_eq_ri(1:2,1:nkx,1:nky)); aphi_eq_ri=0.
  allocate(apsi_eq_ri(1:2,1:nkx,1:nky)); apsi_eq_ri=0.

  allocate(aphi(1:nkx,1:nky)); aphi=cmplx(0.,0.)
  allocate(apsi(1:nkx,1:nky)); apsi=cmplx(0.,0.)
  allocate(vor(1:nkx,1:nky)); vor=cmplx(0.,0.)
  allocate(cur(1:nkx,1:nky)); cur=cmplx(0.,0.)

  allocate(aphi_pt(1:nkx,1:nky)); aphi_pt=cmplx(0.,0.)
  allocate(apsi_pt(1:nkx,1:nky)); apsi_pt=cmplx(0.,0.)
  allocate(vor_pt(1:nkx,1:nky)); vor_pt=cmplx(0.,0.)
  allocate(cur_pt(1:nkx,1:nky)); cur_pt=cmplx(0.,0.)

  allocate(apsi_old(nx,ny)); apsi_old(:,:)=0.

  istatus = nf90_get_var (file_id, aphi_eq_id, aphi_eq_ri(1:2,1:nkx,1:nky), &
       & start=(/ 1,1,1,1 /),  count=(/ 2,nkx,nky,1 /))
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, aphi_eq_id)
  istatus = nf90_get_var (file_id, apsi_eq_id, apsi_eq_ri(1:2,1:nkx,1:nky), &
       & start=(/ 1,1,1,1 /),  count=(/ 2,nkx,nky,1 /))
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, apsi_eq_id)

  open(unit=51,file='xprof1.dat',status='unknown')
  open(unit=52,file='xprof2.dat',status='unknown')
  open(unit=53,file='xprof3.dat',status='unknown')
  open(unit=54,file='xprof4.dat',status='unknown')
  open(unit=55,file='yprof1.dat',status='unknown')
  open(unit=56,file='yprof2.dat',status='unknown')
  open(unit=57,file='yprof3.dat',status='unknown')
  open(unit=58,file='yprof4.dat',status='unknown')
  open(unit=61,file='growth_rate.dat',status='unknown')
  open(unit=71,file='current_width.dat',status='unknown')
  open(unit=81,file='xpoint.dat',status='unknown')
  
  if(debug) open(91,file='function.dat',status='unknown')

  write(51,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut1-1)
  write(52,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut2-1)
  write(53,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut3-1)
  write(54,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut4-1)
  write(55,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut1-1)
  write(56,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut2-1)
  write(57,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut3-1)
  write(58,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut4-1)
  write(51,'(a1,a19,5(1x,a19))') '#','time','x', &
       'phi','psi','vorticity','current'
  write(52,'(a1,a19,5(1x,a19))') '#','time','x', &
       'phi','psi','vorticity','current'
  write(53,'(a1,a19,5(1x,a19))') '#','time','x', &
       'phi','psi','vorticity','current'
  write(54,'(a1,a19,5(1x,a19))') '#','time','x', &
       'phi','psi','vorticity','current'
  write(55,'(a1,a19,5(1x,a19))') '#','time','y', &
       'phi','psi','vorticity','current'
  write(56,'(a1,a19,5(1x,a19))') '#','time','y', &
       'phi','psi','vorticity','current'
  write(57,'(a1,a19,5(1x,a19))') '#','time','y', &
       'phi','psi','vorticity','current'
  write(58,'(a1,a19,5(1x,a19))') '#','time','y', &
       'phi','psi','vorticity','current'

  write(61,'(a1,a19,7(1x,a19))') '#','1 time','2 growth','3 island width1','4 island width 2','5 epar at C','6 epar at X','7 apsi_pt at C','8 apsi_pt at X'
  write(71,'(a1,a19,4(1x,a19))') '#','1 time','2 width 1','3 width 2','4 jpar_pt at C','5 jpar_pt at X'
  write(81,'(a1,a19,4(1x,a19))') '#','1 time','2 x coord','3 y coord','4 x index','5 y index'
  
  xp_guess(1:2)=(/ .25*lx, .25*lx /)

  timeloop : do itime=1,ntime

     istatus = nf90_get_var (file_id, time_id, time, start=(/ itime /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, varid=time_id)
     istatus = nf90_get_var (file_id, aphi_id, aphi_ri(1:2,1:nkx,1:nky), &
          & start=(/ 1,1,1,itime /),  count=(/ 2,nkx,nky,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, aphi_id)
     istatus = nf90_get_var (file_id, apsi_id, apsi_ri(1:2,1:nkx,1:nky), &
          & start=(/ 1,1,1,itime /),  count=(/ 2,nkx,nky,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, apsi_id)

     ! full 
     call r2c(aphi,aphi_ri)
     call r2c(apsi,apsi_ri)
     call laplacian(aphi,vor)
     call laplacian(apsi,cur)

     call ktox(aphi,aphi_xy)
     call ktox(apsi,apsi_xy)
!     apsi=apsi*cmplx(0.,1.)
     call ktox(vor,vor_xy)
     call ktox(cur,cur_xy)

     ! perturbation
     call r2c(aphi_pt,aphi_ri-aphi_eq_ri)
     call r2c(apsi_pt,apsi_ri-apsi_eq_ri)
     call laplacian(aphi_pt,vor_pt)
     call laplacian(apsi_pt,cur_pt)

     call ktox(aphi_pt,aphi_pt_xy)
     call ktox(apsi_pt,apsi_pt_xy)
     call ktox(vor_pt,vor_pt_xy)
     call ktox(cur_pt,cur_pt_xy)
     
     do i=1,nx+1
        x=lx/nx*(i-1)
        write(51,'(10e20.12)') time,x, &
             & aphi_pt_xy(ycut1,i),apsi_pt_xy(ycut1,i), &
             & vor_pt_xy(ycut1,i),cur_pt_xy(ycut1,i), &
             & aphi_xy(ycut1,i),apsi_xy(ycut1,i), &
             & vor_xy(ycut1,i),cur_xy(ycut1,i)
        write(52,'(10e20.12)') time,x, &
             & aphi_pt_xy(ycut2,i),apsi_pt_xy(ycut2,i), &
             & vor_pt_xy(ycut2,i),cur_pt_xy(ycut2,i), &
             & aphi_xy(ycut2,i),apsi_xy(ycut2,i), &
             & vor_xy(ycut2,i),cur_xy(ycut2,i)
        write(53,'(10e20.12)') time,x, &
             & aphi_pt_xy(ycut3,i),apsi_pt_xy(ycut3,i), &
             & vor_pt_xy(ycut3,i),cur_pt_xy(ycut3,i), &
             & aphi_xy(ycut3,i),apsi_xy(ycut3,i), &
             & vor_xy(ycut3,i),cur_xy(ycut3,i)
        write(54,'(10e20.12)') time,x, &
             & aphi_pt_xy(ycut4,i),apsi_pt_xy(ycut4,i), &
             & vor_pt_xy(ycut4,i),cur_pt_xy(ycut4,i), &
             & aphi_xy(ycut4,i),apsi_xy(ycut4,i), &
             & vor_xy(ycut4,i),cur_xy(ycut4,i)
     end do
     write(51,*);write(52,*);write(53,*);write(54,*)
     do j=1,ny+1
        y=ly/ny*(j-1)
        write(55,'(10e20.12)') time,y, &
             & aphi_pt_xy(j,xcut1),apsi_pt_xy(j,xcut1), &
             & vor_pt_xy(j,xcut1),cur_pt_xy(j,xcut1), &
             & aphi_xy(j,xcut1),apsi_xy(j,xcut1), &
             & vor_xy(j,xcut1),cur_xy(j,xcut1)
        write(56,'(10e20.12)') time,y, &
             & aphi_pt_xy(j,xcut2),apsi_pt_xy(j,xcut2), &
             & vor_pt_xy(j,xcut2),cur_pt_xy(j,xcut2), &
             & aphi_xy(j,xcut2),apsi_xy(j,xcut2), &
             & vor_xy(j,xcut2),cur_xy(j,xcut2)
        write(57,'(10e20.12)') time,y, &
             & aphi_pt_xy(j,xcut3),apsi_pt_xy(j,xcut3), &
             & vor_pt_xy(j,xcut3),cur_pt_xy(j,xcut3), &
             & aphi_xy(j,xcut3),apsi_xy(j,xcut3), &
             & vor_xy(j,xcut3),cur_xy(j,xcut3)
        write(58,'(10e20.12)') time,y, &
             & aphi_pt_xy(j,xcut4),apsi_pt_xy(j,xcut4), &
             & vor_pt_xy(j,xcut4),cur_pt_xy(j,xcut4), &
             & aphi_xy(j,xcut4),apsi_xy(j,xcut4), &
             & vor_xy(j,xcut4),cur_xy(j,xcut4)
     end do
     write(55,*);write(56,*);write(57,*);write(58,*)

     !
     ! growth rate
     !

     growth=0.; epar_C=0.

     if(itime/=1) then
!        growth=(apsi_pt_xy(ycut3,xcut3)-apsi_pt_old)/(time-time_old)/apsi_pt_xy(ycut3,xcut3)
        growth=log(apsi_pt_xy(ycut3,xcut3)/apsi_pt_old)/(time-time_old)
        epar_C=-(apsi_xy(ycut3,xcut3)-apsi_old(ycut3,xcut3))/(time-time_old)

        island_width=4.*sqrt(apsi_pt_xy(ycut3,xcut3)/(cur_xy(ycut3,xcut3)-cur_pt_xy(ycut3,xcut3)))
        apsi_X=apsi_xy(ycut3,xcut3)

        ! Is secondary island forming?
        epar_X=epar_C
        xpoint_x=xcut3; xpoint_y=ycut3;
        if (apsi_X < apsi_xy(xcut3,ycut3+1)) then
           ! search the actual X point.
           xsearch : do j=ycut3+1,ny
              if (apsi_xy(j,xcut3)<apsi_xy(j+1,xcut3)) then
                 apsi_X=apsi_xy(j,xcut3)
                 epar_X=-(apsi_xy(j,xcut3)-apsi_old(j,xcut3))/(time-time_old)
                 xpoint_y=j

                 island_width=4.*sqrt(apsi_pt_xy(j,xcut3)/(cur_xy(j,xcut3)-cur_pt_xy(j,xcut3)))
                 exit xsearch
              endif
           end do xsearch
        endif
        
        search : do i=xcut3,nx
           if ((apsi_xy(ycut1,i)-apsi_X)*(apsi_xy(ycut1,i+1)-apsi_X) < 0.) then
              island_width2=2.*(i-xcut3+(apsi_xy(ycut1,i)-apsi_X)/(apsi_xy(ycut1,i)-apsi_xy(ycut1,i+1)))*lx/nx
              exit search
           endif
           island_width2=0.
        end do search
        
        write(61,'(8e20.12)') time,growth,island_width,island_width2, &
             & epar_C,epar_X,apsi_pt_xy(ycut3,xcut3),apsi_pt_xy(xpoint_y,xpoint_x)
        write(81,'(3e20.12,2i20)') time,lx/nx*(xpoint_x-1),ly/ny*(xpoint_y-1),xpoint_x,xpoint_y
     endif

     time_old=time; apsi_old=apsi_xy; apsi_pt_old=apsi_pt_xy(ycut3,xcut3)

     if(debug) then
        do i=1,nx
           x=lx/nx*(i-1)
           write(91,'(5e20.12)') time,x,cur_xy(ycut3,i),jpt_x(x),djpt_x(x)
        end do
        write(91,*)

        write(fname_cur_deb,'(i0)') itime
        fname_cur_deb='cur_debug.'//trim(fname_cur_deb)//'.dat'
        open(90,file=fname_cur_deb)
        write(90,'("# ", " itime = ",i10," time = ",f10.2)') itime,time
        do i=1,nx
           x=lx/nx*(i-1)
           do j=1,ny
              y=ly/ny*(j-1)
              write(90,'(4e20.12)') x,y,cur_xy(j,i),jfunc(x,y)
           end do
           write(90,*)
        end do
        close(90)
     endif

     !
     ! current width
     !

     if(itime/=1) then
        width_guess=xsheet
        guess1 : do while (jpt_x(xsheet)*jpt_x(width_guess)>=0.)
           width_guess=width_guess+del*lx
           if(width_guess>=lx) then
              width_guess=xsheet
              exit guess1
           endif
        end do guess1
        jmax_guess=width_guess
        guess2 : do while (djpt_x(width_guess)*djpt_x(jmax_guess)>=0.)
           jmax_guess=jmax_guess+del*lx
           if(jmax_guess>=lx) then
              jmax_guess=width_guess
              exit guess2
           endif
        end do guess2
        ! find position where dj/dx=0
        call root_finder_bisect(width_guess,jmax_guess, &
             0.,xjmax,djpt_x)

        width=xsheet; width2=xsheet
        call root_finder_bisect(xsheet,width_guess, &
             .5*jpt_x(xsheet),width,jpt_x)
        call root_finder_bisect(width,jmax_guess, &
             .5*(jpt_x(xsheet)+jpt_x(xjmax)),width2,jpt_x)
        width=2.*(width-xsheet)
        width2=2.*(width2-xsheet)

        write(71,'(5e20.12)') time,width,width2,cur_pt_xy(ycut3,xcut3),cur_pt_xy(xpoint_y,xpoint_x)
     end if

     
!!$     ! find x point and island width
!!$     ! actually find the position where dpdi/dx = 0, dpsi/dy=0. (can be X and O points)
!!$     call root_finder_newton(2, xp_guess, (/0.,0./), xp_sol, &
!!$          & sub_dpsi, sub_dpsi_hess)
!!$     call sub_dpsi(xp_sol,val)
!!$     if (sqrt(sum(val**2)) > 1.e-10) write(6,*) 'Warning: not zero'
        
  end do timeloop

  close(51); close(52); close(53); close(54)
  close(55); close(56); close(57); close(58)
  close(61)
  close(71)
  close(81)

  if(debug) close(91)

contains

  subroutine root_finder_newton (ndim, guess, val, sol, sub_func, sub_jacob)
    integer, intent(in) :: ndim
    real, intent(in) :: guess(ndim), val(ndim)
    real, intent(out) :: sol(ndim)
    real :: sol_old(ndim)

    interface
       subroutine sub_func(pos,val)
         real, intent(in) :: pos(:)
         real, intent(out) :: val(:)
       end subroutine sub_func
       subroutine sub_jacob(pos,val)
         real, intent(in) :: pos(:)
         real, intent(out) :: val(:,:)
       end subroutine sub_jacob
    end interface

    real :: jacob(ndim,ndim),inv_jacob(ndim,ndim)
    real :: func(ndim)
    real :: det
    real :: norm = 1.
    real :: eps=1.e-10
    
    integer :: count=0, count_max=1000

    norm=1.
    sol_old(1:ndim)=guess(1:ndim)

    iter : do while (norm > eps)

       call sub_jacob(sol_old,jacob)
       call sub_func(sol_old,func)

       if (ndim == 2) then
          det = jacob(1,1)*jacob(2,2)-jacob(1,2)*jacob(2,1)
          inv_jacob(1,1) = jacob(2,2)/det
          inv_jacob(1,2) =-jacob(1,2)/det
          inv_jacob(2,1) =-jacob(2,1)/det
          inv_jacob(2,2) = jacob(1,1)/det
       else
       end if

       do i=1,ndim
          sol(i)=sol_old(i)+sum(inv_jacob(i,1:ndim)*(val(1:ndim)-func(1:ndim)))
       end do

       norm=sum((sol(1:ndim)-sol_old(1:ndim))**2)

       if (norm < eps) exit iter

       sol_old(:)=sol(:)
       count = count + 1

       if (count > count_max) then
          stop 'Interation count exceeds max in root_finder_newton'
       end if

    end do iter

  end subroutine root_finder_newton

  subroutine root_finder_bisect (g0, g1, val, sol, func)
    ! solve func=val
    ! solution range:  g0 to g1
    real, intent(in) :: g0, g1, val
    real, intent(out) :: sol

    interface
       function func (x)
         real, intent(in) :: x
         real :: func
       end function func
    end interface

    real :: x0, x1, xc, y0, y1, yc

    x0 = g0; x1 = g1
    ! rhs - lhs
    y0 = func(x0) - val
    y1 = func(x1) - val

    if (y0*y1 > 0.0) then
       write (0,*) 'no solution in specified domain'
       !       stop
       return
    end if

    do while (abs((x0-x1)/max(abs(x0),abs(x1))) > 1.e-10)

       xc = (x0+x1)/2.0
       yc = func(xc) - val

       if (y0*yc > 0.0) then
          x0 = xc; y0 = yc
       else
          x1 = xc; y1 = yc
       end if

    end do

    if (abs(y0) < abs(y1)) then
       sol = x0
    else
       sol = x1
    end if

  end subroutine root_finder_bisect

end program tearing_diag
