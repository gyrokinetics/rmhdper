# include "define.inc"

module diagnostics

  use file_utils, only: stdout_unit, error_unit
# ifdef NETCDF
  use netcdf, only: NF90_NOERR
  use netcdf_utils, only: kind_nf
  use netcdf_utils, only: netcdf_error
# endif

  implicit none

  public :: init_diagnostics, finish_diagnostics
  public :: loop_diagnostics, final_diagnostics

  private

  interface gridout
     module procedure gridout_xspace, gridout_kspace
     module procedure gridout_file_kspace
!!$     module procedure gridout_file_kspace_2
!!$     module procedure gridout_file_kspace_4
  end interface

  logical :: initialized=.false.
  logical :: debug=.false.
  logical :: write_kspec, write_ktrans
  logical :: write_balance

  integer, parameter :: gridout_unit=10
  integer :: fpeng, fpfields, fpkspec, fpktrans, fpkflux, fpbal
  integer :: fprev
  
  real :: energy
# ifdef NETCDF
  integer (kind_nf) :: fpnc
  integer (kind_nf) :: st
  integer (kind_nf) :: nxid, nyid, lxid, lyid, nuid, etaid, kxid, kyid
  integer (kind_nf) :: nuhid, etahid, diid, muvid, mubid
  integer (kind_nf) :: nuparid, nuhparid, muvparid
  integer (kind_nf) :: etaparid, etahparid, mubparid
  integer (kind_nf) :: timeid
  integer (kind_nf) :: aphiid, apsiid, avzid, abzid
  integer (kind_nf) :: etid !, emid
  integer (kind_nf) :: aphieqid, apsieqid, avzeqid, abzeqid
# endif

contains

  subroutine init_diagnostics

    use run_params, only: magneto, nx, ny
    use file_utils, only: open_output_file, close_output_file
    use kspectra, only: init_polar_spectrum

    if(debug) write(stdout_unit,*) 'Initializing diagnostics ...'

    write_kspec = .false.
    write_ktrans = .false.
    write_balance = .false.
    call read_parameters
    write_kspec = write_kspec .and. (nx==ny)
    write_ktrans = write_ktrans .and. (nx==ny)

    call init_energy
    call init_ncfile

# define _GIT_HASH_ GIT_HASH
    call open_output_file (fprev,'.githash')
    write(fprev,'("Revision: ",a)') GIT_HASH
    call close_output_file(fprev)
    
    if (write_kspec) then
       call init_polar_spectrum
       ! Open file and make header
       call open_output_file (fpkspec, '.kspec')
       write (fpkspec,'(a1,a15,3(1x,a15))',advance='no') '#','time','kperp','|phik|^2','Ek(k)'
       if (magneto) then
          write (fpkspec,'(2(1x,a15))',advance='no') '|psik|^2','Em(k)'
       else
          write (fpkspec,'(1(1x,a15))',advance='no') '|omgk|^2'
       end if
       write (fpkspec, '()')
    end if

    if (write_ktrans) then
       call init_polar_spectrum
       ! Open file and make header
       call open_output_file (fpktrans, '.ktrans')
       write (fpktrans,'(a1,a15,4(1x,a15))') '#','time','from','to','enstrophy','energy'
       call open_output_file (fpkflux, '.kflux')
       write (fpkflux,'(a1,a15,5(1x,a15))') '#','time','kperp','arr(ens)','lef(ens)','arr(eng)','lef(eng)'
    end if

    if (write_balance) then
       ! initialize .bal file
       call open_output_file (fpbal, '.bal')
       ! write out legend
       if (magneto) then
          write (fpbal, '(a1,a15,6(1x,a15))') '#','1 time','2 energy (dif)','3 energy (sum)', &
               & '4 |A|^2 (dif)','5 |A|^2 (sum)','6 x. hel. (dif)','7 x. hel. (sum)'
       else
          write (fpbal, '(a1,a15,4(1x,a15))') '#','1 time','2 energy (dif)','3 energy (sum)', &
               & '4 enst. (dif)','5 enst. (sum)'
       end if
    end if
   
    initialized=.true.

    if(debug) write(stdout_unit,*) 'Initializing diagnostics done'

  contains

    subroutine read_parameters

      use file_utils, only: input_unit_exist
      logical :: ex
      integer :: unit

      namelist /diagnostics/ write_kspec, write_ktrans, write_balance
      unit = input_unit_exist('diagnostics', ex)
      if (ex) read(unit, diagnostics)

    end subroutine read_parameters

    subroutine init_energy

      use file_utils, only: open_output_file

      ! inialize .energy file
      call open_output_file (fpeng, '.energy')
      ! write out legend
      if (magneto) then
         write (stdout_unit, '(a1,a15,14(1x,a15))') &
              & '#','1 time','2 energy','3 |A|^2','4 cross hel','5 dE/dt','6 kin ens','7 mag ens','8 kin eng','9 mag eng','10 Ein v','11 Diss v','12 Fric v','13 Ein b','14 Diss b','15 Fric b'
         write (fpeng, '(a1,a15,14(1x,a15))') &
              & '#','1 time','2 energy','3 |A|^2','4 cross hel','5 dE/dt','6 kin ens','7 mag ens','8 kin eng','9 mag eng','10 Ein v','11 Diss v','12 Fric v','13 Ein b','14 Diss b','15 Fric b'
      else
         write (stdout_unit, '(a1,a15,6(1x,a15))') '#','1 time','2 energy','3 dE/dt','4 enstrophy','5 Ein v','6 Diss v','7 Fric v'
         write (fpeng, '(a1,a15,6(1x,a15))') '#','1 time','2 energy','3 dE/dt','4 enstrophy','5 Ein v','6 Diss v','7 Fric v'
      end if

      ! intialize .fields file
      call open_output_file (fpfields, '.fields')
      ! write out legend
      write (fpfields, '(a1,a15,3(1x,a15))',advance='no') '#','x','y','omg','phi'
      if (magneto) write (fpfields, '(2(1x,a15))',advance='no') 'cur','psi'
      write (fpfields,'()')

    end subroutine init_energy

    subroutine init_ncfile

# ifdef NETCDF
      use netcdf, only: NF90_CLOBBER, NF90_UNLIMITED
      use netcdf, only: NF90_INT
      use netcdf, only: nf90_create
      use netcdf, only: nf90_def_dim, nf90_def_var
      use netcdf, only: nf90_enddef
      use netcdf, only: nf90_put_var
      use netcdf_utils, only: netcdf_real, get_netcdf_code_precision
      use file_utils, only: run_name
      use fft, only: nkx, nky
      use four, only: lx, ly, kx, ky
      use run_params, only: nx, ny, nu, eta, nuh, etah, di, muv, mub
      use run_params, only: nupar, nuhpar, muvpar, etapar, etahpar, mubpar
      use time_fields_vars, only: is_equil
      use time_fields_vars, only: aphi_eq, apsi_eq, avz_eq, abz_eq
      use convert, only: c2r

      integer :: ridim, kxdim, kydim, timedim
      real, dimension (2,nkx,nky) :: ri

      ! initialize .nc file
      netcdf_real = get_netcdf_code_precision()

      ! open netcdf file
      st = nf90_create (trim(run_name)//'.nc', NF90_CLOBBER, fpnc)
      if (st /= NF90_NOERR) call netcdf_error(st,file=trim(run_name)//'.nc')

      ! define dimensions
      st = nf90_def_dim (fpnc, 'ri', 2, ridim)
      if (st /= NF90_NOERR) call netcdf_error(st, dim='ri')
      st = nf90_def_dim (fpnc, 'kx', nkx, kxdim)
      if (st /= NF90_NOERR) call netcdf_error(st, dim='kx')
      st = nf90_def_dim (fpnc, 'ky', nky, kydim)
      if (st /= NF90_NOERR) call netcdf_error(st, dim='ky')
      st = nf90_def_dim (fpnc, 't', NF90_UNLIMITED, timedim)
      if (st /= NF90_NOERR) call netcdf_error(st, dim='t')

      ! define variables
      st = nf90_def_var (fpnc, 'nx', NF90_INT, nxid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='nx')
      st = nf90_def_var (fpnc, 'ny', NF90_INT, nyid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='ny')
      st = nf90_def_var (fpnc, 'lx', netcdf_real, lxid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='lx')
      st = nf90_def_var (fpnc, 'ly', netcdf_real, lyid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='ly')
      st = nf90_def_var (fpnc, 'kx', netcdf_real, (/kxdim/), kxid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='kx')
      st = nf90_def_var (fpnc, 'ky', netcdf_real, (/kydim/), kyid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='ky')
      st = nf90_def_var (fpnc, 'nu', netcdf_real, nuid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='nu')
      st = nf90_def_var (fpnc, 'nuh', netcdf_real, nuhid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='nuh')
      st = nf90_def_var (fpnc, 'muv', netcdf_real, muvid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='muv')
      st = nf90_def_var (fpnc, 'time', netcdf_real, (/timedim/), timeid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='time')
      st = nf90_def_var (fpnc, 'E_t', netcdf_real, (/timedim/), etid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='E_t')
      st = nf90_def_var (fpnc, 'aphi', netcdf_real, &
           & (/ ridim, kxdim, kydim, timedim /), aphiid)
      if (st /= NF90_NOERR) call netcdf_error(st, var='aphi')
      if (magneto) then
         st = nf90_def_var (fpnc, 'eta', netcdf_real, etaid)
         if (st /= NF90_NOERR) call netcdf_error(st, var='eta')
         st = nf90_def_var (fpnc, 'etah', netcdf_real, etahid)
         if (st /= NF90_NOERR) call netcdf_error(st, var='etah')
         st = nf90_def_var (fpnc, 'mub', netcdf_real, mubid)
         if (st /= NF90_NOERR) call netcdf_error(st, var='mub')
         st = nf90_def_var (fpnc, 'apsi', netcdf_real, &
              & (/ ridim, kxdim, kydim, timedim /), apsiid)
         if (st /= NF90_NOERR) call netcdf_error(st, var='apsi')
         if (di /= 0.) then
            st = nf90_def_var (fpnc, 'di', netcdf_real, diid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='di')
            st = nf90_def_var (fpnc, 'nupar', netcdf_real, nuparid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='nupar')
            st = nf90_def_var (fpnc, 'nuhpar', netcdf_real, nuhparid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='nuhpar')
            st = nf90_def_var (fpnc, 'muvpar', netcdf_real, muvparid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='muvpar')
            st = nf90_def_var (fpnc, 'etapar', netcdf_real, etaparid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='etapar')
            st = nf90_def_var (fpnc, 'etahpar', netcdf_real, etahparid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='etahpar')
            st = nf90_def_var (fpnc, 'mubpar', netcdf_real, mubparid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='mub')
            st = nf90_def_var (fpnc, 'avz', netcdf_real, &
                 & (/ ridim, kxdim, kydim, timedim /), avzid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='avz')
            st = nf90_def_var (fpnc, 'abz', netcdf_real, &
                 & (/ ridim, kxdim, kydim, timedim /), abzid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='abz')
         endif
      end if
      if (is_equil) then
         st = nf90_def_var (fpnc, 'aphi_eq', netcdf_real, &
              & (/ ridim, kxdim, kydim /), aphieqid)
         if (st /= NF90_NOERR) call netcdf_error(st, var='aphi_eq')
         if(magneto) then
            st = nf90_def_var (fpnc, 'apsi_eq', netcdf_real, &
                 & (/ ridim, kxdim, kydim /), apsieqid)
            if (st /= NF90_NOERR) call netcdf_error(st, var='apsi_eq')
            if (di /= 0.) then
               st = nf90_def_var (fpnc, 'avz_eq', netcdf_real, &
                    & (/ ridim, kxdim, kydim /), avzeqid)
               if (st /= NF90_NOERR) call netcdf_error(st, var='avz_eq')
               st = nf90_def_var (fpnc, 'abz_eq', netcdf_real, &
                    & (/ ridim, kxdim, kydim /), abzeqid)
               if (st /= NF90_NOERR) call netcdf_error(st, var='abz_eq')
            endif
         endif
      endif

      st = nf90_enddef (fpnc)
      if (st /= NF90_NOERR) call netcdf_error(st)

      ! write parameters
      st = nf90_put_var (fpnc, nxid, nx)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, nxid)
      st = nf90_put_var (fpnc, nyid, ny)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, nyid)
      st = nf90_put_var (fpnc, lxid, lx)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, lxid)
      st = nf90_put_var (fpnc, lyid, ly)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, lyid)
      st = nf90_put_var (fpnc, kxid, kx)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, kxid)
      st = nf90_put_var (fpnc, kyid, ky)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, kyid)
      st = nf90_put_var (fpnc, nuid, nu)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, nuid)
      st = nf90_put_var (fpnc, nuhid, nuh)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, nuhid)
      st = nf90_put_var (fpnc, muvid, muv)
      if (st /= NF90_NOERR) call netcdf_error(st, fpnc, muvid)
      if (magneto) then
         st = nf90_put_var (fpnc, etaid, eta)
         if (st /= NF90_NOERR) call netcdf_error(st, fpnc, etaid)
         st = nf90_put_var (fpnc, etahid, etah)
         if (st /= NF90_NOERR) call netcdf_error(st, fpnc, etahid)
         st = nf90_put_var (fpnc, mubid, mub)
         if (st /= NF90_NOERR) call netcdf_error(st, fpnc, mubid)
         if (di /= 0.) then
            st = nf90_put_var (fpnc, diid, di)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, diid)
            st = nf90_put_var (fpnc, nuparid, nupar)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, nuparid)
            st = nf90_put_var (fpnc, nuhparid, nuhpar)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, nuhparid)
            st = nf90_put_var (fpnc, muvparid, muvpar)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, muvparid)
            st = nf90_put_var (fpnc, etaparid, etapar)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, etaparid)
            st = nf90_put_var (fpnc, etahparid, etahpar)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, etahparid)
            st = nf90_put_var (fpnc, mubparid, mubpar)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, mubparid)
         endif
      endif
      
      ! write equilibrium
      if (is_equil) then
         call c2r (aphi_eq, ri)
         st = nf90_put_var (fpnc, aphieqid, ri)
         if (st /= NF90_NOERR) call netcdf_error(st, fpnc, aphieqid)
         if (magneto) then
            call c2r (apsi_eq, ri)
            st = nf90_put_var (fpnc, apsieqid, ri)
            if (st /= NF90_NOERR) call netcdf_error(st, fpnc, apsieqid)
            if (di /= 0.) then
               call c2r (avz_eq, ri)
               st = nf90_put_var (fpnc, avzeqid, ri)
               if (st /= NF90_NOERR) call netcdf_error(st, fpnc, avzeqid)
               call c2r (abz_eq, ri)
               st = nf90_put_var (fpnc, abzeqid, ri)
               if (st /= NF90_NOERR) call netcdf_error(st, fpnc, abzeqid)
            endif
         endif
      endif

# endif

    end subroutine init_ncfile

  end subroutine init_diagnostics

  subroutine finish_diagnostics

    use file_utils, only: close_output_file
# ifdef NETCDF
    use netcdf, only: nf90_close
# endif
    use kspectra, only: finish_polar_spectrum

# ifdef NETCDF
    st = nf90_close (fpnc)
    if (st /= NF90_NOERR) call netcdf_error(st)
# endif

    call close_output_file (fpeng)
    call close_output_file (fpbal)
    call close_output_file (fpfields)
    if (write_kspec) then
       call finish_polar_spectrum
       call close_output_file (fpkspec)
    end if
    if (write_ktrans) then
       call close_output_file (fpktrans)
       call close_output_file (fpkflux)
    end if

  end subroutine finish_diagnostics

  subroutine final_diagnostics

    use run_params, only: magneto
    use time_fields_vars, only: aomg0, aphi, acur, apsi0

    if (magneto) then
       call gridout (fpfields, aomg0, aphi, acur, apsi0)
    else
       call gridout (fpfields, aomg0, aphi)
    end if

  end subroutine final_diagnostics

  subroutine loop_diagnostics (istep)

    use constants, only: pi
    use file_utils, only: flush_output_file
    use fft, only: ktox, xtok, nkx, nky
    use four, only: l2square, integrate, kperp2, get_vector, lapinv, laplacian
    use run_params, only: nx, ny, nu, eta, nuh, etah, magneto, nwrite
    use run_params, only: muv, mub
    use time_fields_vars, only: time, vx, vy, bx, by, aomg0, aphi, apsi0, acur, aomg, apsi
    use time_fields_vars, only: drive_v, drive_b
    use kspectra, only: nkpolar, kpavg, get_polar_spectrum, get_ktransfer, get_ktransfer2
    use time_fields_vars, only: gctmp1
    
    integer, intent (in) :: istep
    integer :: ip, ipto, ipfrom
    real, dimension (ny+1,nx+1) :: vectwx, vectwy
    real :: psi2, ch, edot, ensdot, psi2dot, chdot
    real :: enskin, ensmag, engkin, engmag
    real, dimension (nkpolar) :: phik2_1d, psik2_1d, ek1d, em1d, omgk2_1d
    real, dimension (nkx,nky) :: phik2_2d, psik2_2d, ek2d, em2d, omgk2_2d
    real, dimension (nkpolar,nkpolar) :: enstrans, engtrans
    real, save :: time_old = 0., energy_old = 0., enskin_old = 0., psi2_old = 0., ch_old = 0.
    real :: ebaldif, ebalsum, ensbaldif, ensbalsum, psi2baldif, psi2balsum, chbaldif, chbalsum
    real :: grdomg2, grdcur2
    real :: energy_in_v, energy_in_b
    real :: diss_v, diss_b, fric_v, fric_b
    real, save :: enesum=0., enssum=0., psi2sum=0., chsum=0.
    
    !================================!
    ! return here if not writing out !
    !================================!
    if (mod(istep,nwrite) /= 0) return
    !================================!
    ! return here if not writing out !
    !================================!

    ! write .energy file
    engkin = 0.5 * (l2square(vx)+l2square(vy))
    energy = engkin
    !energy = 0.5 * integrate(kperp2*aphi**2)
    enskin = 0.5 * l2square(aomg0)   ! l2square can take either real and complex argument.
    !enskin = 0.5 * integrate(aomg(:,:,1)**2)
    grdomg2 = integrate(kperp2*aomg0**2)
!!!    grdomg2 = integrate(cmplx(kperp2*cabs(aomg0)**2)) ! equivalent to the above expression
!!!    grdomg2 = l2square(sqrt(kperp2)*aomg0) ! equivalent to the above expression
    call lapinv (aomg(:,:,1), gctmp1)
    gctmp1=.5*(gctmp1+aphi) ! this is aphi at n+1/2
    energy_in_v = -integrate(gctmp1,drive_v(:,:,1))
    diss_v = 2.0 * nu * enskin + nuh * grdomg2
    fric_v = 2.0 * muv * engkin
    edot = energy_in_v - (diss_v + fric_v)

    if (magneto) then
       !energy = energy + 0.5 * (l2square(bx)+l2square(by))
       !energy = energy + 0.5 * integrate(kperp2*apsi0**2)
       !engmag = 0.5 * (l2norm(bx)+l2norm(by))
       engmag = 0.5 * (l2square(bx)+l2square(by))
       energy = engkin + engmag
       !call ktox (acur, curyx)
       ensmag = 0.5 * l2square(acur)       ! l2square can take either real and complex argument.
       !ensmag = 0.5 * integrate(acur**2)
       grdcur2 = integrate(kperp2*acur**2)
!       grdcur2 = integrate(cmplx(kperp2*cabs(acur)**2)) ! equivalent to the above expression
       call laplacian (apsi(:,:,1), gctmp1)
       gctmp1=.5*(gctmp1+acur) ! this is acur at n+1/2
       energy_in_b = -integrate(gctmp1,drive_b(:,:,1))
       diss_b = 2.0 * eta * ensmag + etah * grdcur2
       fric_b = 2.0 * mub * engmag
       edot = edot + energy_in_b - (diss_b + fric_b)
       psi2 = l2square(apsi0)
       ch = integrate(vx*bx+vy*by)

       write (stdout_unit,'(15(1x,es15.5))') &
            & time, energy, psi2, ch, edot, enskin, ensmag, engkin, engmag, energy_in_v, diss_v, fric_v, energy_in_b, diss_b, fric_b
       write (fpeng,'(15(1x,es15.5))') &
            & time, energy, psi2, ch, edot, enskin, ensmag, engkin, engmag, energy_in_v, diss_v, fric_v, energy_in_b, diss_b, fric_b
    else
       write (stdout_unit,'(7(1x,es15.5))') time, energy, edot, enskin, energy_in_v, diss_v, fric_v
       write (fpeng,'(7(1x,es15.5))') time, energy, edot, enskin, energy_in_v, diss_v, fric_v
    end if

    if (write_balance) then
!!! may contain O(diff*delt) error
!!! where diff = mu + nu * kperp2 + nuh * kperp4

!!! Balance of conserving quantities are checked using difference and summation.
!!! 'difference' evaluates dE/dt = C, while 'summation' compares E^n and (E^0 + int C dt).
!!!
!!! Note: cross helicity must be checked carefully, because it is not positive definite.
!!!  When it fluctuates around zero, nwrite must be resolving such a fluctuation.
!!!  Otherwise, difference will be averaged over during nwrite interval.
!!!
       if (istep == 0) then
          enesum = energy
          enssum = enskin
          if (magneto) then
             psi2sum = psi2
             chsum = ch
          end if
       end if
       
       if (istep /= 0) then
          enesum = enesum + edot*(time-time_old)
       end if
       ebalsum = abs((enesum-energy)/energy)
       ebaldif = abs(((energy-energy_old)/(time-time_old)-edot)/edot)

       if (magneto) then
          if (istep /= 0) then
             gctmp1=.5*sum(apsi(:,:,0:1),dim=3) ! this is apsi at n+1/2
             psi2dot = 2. * integrate(gctmp1,drive_b(:,:,1))
             chdot = - integrate(gctmp1,drive_v(:,:,1))
             
             gctmp1=.5*sum(aomg(:,:,0:1),dim=3) ! this is aomg at n+1/2
             chdot = chdot - integrate(gctmp1,drive_b(:,:,1))
             
             psi2dot = psi2dot &
                  & - 4. * eta * engmag - 4. * etah * ensmag - 2. * mub * psi2
             psi2sum = psi2sum + psi2dot*(time-time_old)
             
             chdot = chdot &
                  & - (nu + eta) * integrate(aomg0,acur) &
                  & - (nuh + etah) * integrate(kperp2*aomg0,acur) &
                  & - (muv + mub) * ch
             chsum = chsum + chdot*(time-time_old)
          end if
          psi2balsum = abs((psi2sum-psi2)/psi2)
          psi2baldif = abs(((psi2-psi2_old)/(time-time_old)-psi2dot)/psi2dot)
          chbalsum = abs((chsum-ch)/ch)
          chbaldif = abs(((ch-ch_old)/(time-time_old)-chdot)/chdot)

          write (fpbal,'(7(1x,es15.5))') time, ebaldif, ebalsum, psi2baldif, psi2balsum, chbaldif, chbalsum
       else
          if (istep /= 0) then
             ensdot = - nu * grdomg2 - nuh * l2square(kperp2*aomg0) - 2.0 * muv * enskin
             gctmp1=.5*sum(aomg(:,:,0:1),dim=3) ! this is aomg at n+1/2
             ensdot = ensdot + integrate(gctmp1,drive_v(:,:,1))
             enssum = enssum + ensdot*(time-time_old)
          end if
          ensbalsum = abs((enssum-enskin)/enskin)
          ensbaldif = abs(((enskin-enskin_old)/(time-time_old)-ensdot)/ensdot)
          write (fpbal,'(5(1x,es15.5))') time, ebaldif, ebalsum, ensbaldif,  ensbalsum
       end if
       
       time_old = time
       energy_old = energy
       if (magneto) then
          psi2_old = psi2
          ch_old = ch
       else
          enskin_old = enskin
       end if
    end if
    
    ! write .nc file
# ifdef NETCDF
    call nc_loop
# endif

    ! write k spectrum
    if (write_kspec) then
       ! phik2_2d = |phik|^2 : no 1/2 factor included
       phik2_2d(:,1) = real(conjg(aphi(:,1))*aphi(:,1))
       phik2_2d(:,2:) = 2.0 * real(conjg(aphi(:,2:))*aphi(:,2:))
       call get_polar_spectrum (phik2_2d, phik2_1d)
       ! ek2d = (1/2) (vx^2 + vy^2)
       ek2d(:,1) = 0.5 * kperp2(:,1) * real(conjg(aphi(:,1))*aphi(:,1))
       ek2d(:,2:) = kperp2(:,2:) * real(conjg(aphi(:,2:))*aphi(:,2:))
       call get_polar_spectrum (ek2d, ek1d)
       ! omgk2_2d = |omgk|^2 : no (1/2) factor included
       omgk2_2d(:,1) = real(conjg(aomg0(:,1))*aomg0(:,1))
       omgk2_2d(:,2:) = 2.0 * real(conjg(aomg0(:,2:))*aomg0(:,2:))
       call get_polar_spectrum (omgk2_2d, omgk2_1d)
       if (magneto) then
          ! psik2_2d = |psik|^2 : no 1/2 factor included
          psik2_2d(:,1) = real(conjg(apsi0(:,1))*apsi0(:,1))
          psik2_2d(:,2:) = 2.0 * real(conjg(apsi0(:,2:))*apsi0(:,2:))
          call get_polar_spectrum (psik2_2d, psik2_1d)
          ! em2d = (1/2) (Bx^2 + By^2)
          em2d(:,1) = 0.5 * kperp2(:,1) * real(conjg(apsi0(:,1))*apsi0(:,1))
          em2d(:,2:) = kperp2(:,2:) * real(conjg(apsi0(:,2:))*apsi0(:,2:))
          call get_polar_spectrum (em2d, em1d)
       end if
       do ip=1, nkpolar
          write (fpkspec, '(4(1x,es15.5))',advance='no') time, kpavg(ip), &
               & phik2_1d(ip), ek1d(ip)
          if (magneto) then
             write (fpkspec,'(2(1x,es15.5))',advance='no') psik2_1d(ip), em1d(ip)
          else
             write (fpkspec,'(1(1x,es15.5))',advance='no') omgk2_1d(ip)
          end if
          write (fpkspec, '()')
       end do
       write (fpkspec,*)
       call flush_output_file (fpkspec)
    end if

    ! write shell-to-shell energy or enstrophy transfer
    if (write_ktrans) then
       ! enstrophy transfer
       call lapinv (aomg0, aphi)
       call get_vector (aphi, vx, vy)
       call get_ktransfer (aomg0, vx, vy, enstrans)
!!$       call get_ktransfer2 (aomg0, enstrans)
       ! energy transfer
       call get_vector (aomg0, vectwx, vectwy)
       call get_ktransfer (aphi, vectwx, vectwy, engtrans)

       ! 0.2 sec w/ 64x64 in mac
       do ipfrom=1, nkpolar
!          norm = maxval(abs(enstrans(ipfrom,:)))
          do ipto=1, nkpolar
             write (fpktrans, '(5(1x,es15.5))') time, kpavg(ipfrom), &
                  & kpavg(ipto), enstrans(ipfrom,ipto), &
                  & engtrans(ipfrom,ipto) !/ maxval(abs(engtrans(ipfrom,:)))
          end do
          write (fpktrans, '()')
       end do
       write (fpktrans, '()')

       ! write flux
       do ip=1, nkpolar
          write (fpkflux, '(6(1x,es15.5))') time, kpavg(ip), &
               & sum(enstrans(1:ip,ip)), sum(enstrans(ip:nkpolar,ip)), &
               & sum(engtrans(1:ip,ip)), sum(engtrans(ip:nkpolar,ip))
       end do
       write (fpkflux, '()')

    end if

  end subroutine loop_diagnostics

  subroutine nc_loop

    use convert, only: c2r
    use fft, only: nkx, nky
    use run_params, only: magneto, di
    use file_utils, only: error_unit
    use time_fields_vars, only: time, aphi, apsi0, avz0, abz0
# ifdef NETCDF
    use netcdf, only: nf90_put_var
# endif
    integer, save :: nout=0
    real, dimension (2,nkx,nky) :: ri

    nout = nout + 1

# ifdef NETCDF
    ! write time
    st = nf90_put_var (fpnc, timeid, time, (/nout/))
    if (st /= NF90_NOERR) call netcdf_error(st, fpnc, timeid)
    ! write total energy
    st = nf90_put_var (fpnc, etid, energy, (/nout/))
    if (st /= NF90_NOERR) call netcdf_error(st, fpnc, etid)
    ! write phi
    call c2r (aphi, ri)
    st = nf90_put_var (fpnc, aphiid, ri, (/1,1,1,nout/))
    if (st /= NF90_NOERR) call netcdf_error(st, fpnc, aphiid)
    if (magneto) then
       ! write psi
       call c2r (apsi0, ri)
       st = nf90_put_var (fpnc, apsiid, ri, (/1,1,1,nout/))
       if (st /= NF90_NOERR) call netcdf_error(st, fpnc, apsiid)
       if (di /= 0.) then
          call c2r (avz0, ri)
          st = nf90_put_var (fpnc, avzid, ri, (/1,1,1,nout/))
          if (st /= NF90_NOERR) call netcdf_error(st, fpnc, avzid)
          call c2r (abz0, ri)
          st = nf90_put_var (fpnc, abzid, ri, (/1,1,1,nout/))
          if (st /= NF90_NOERR) call netcdf_error(st, fpnc, abzid)
       endif
    end if

# endif

  end subroutine nc_loop

  subroutine gridout_xspace (fyx)

    use run_params, only: nx, ny
    use four, only: x, y
    real, dimension (:,:), intent (in) :: fyx
    integer :: ix, iy
    integer :: unit=gridout_unit

    do ix=1, nx+1
       do iy=1, ny+1
          write (unit,'(3(1x,es15.5))') x(ix), y(iy), fyx(iy,ix)
       end do
       write (unit,*)
    end do

  end subroutine gridout_xspace

  subroutine gridout_kspace (fa)

    use fft, only: ktox
    use run_params, only: nx, ny
    use four, only: x, y
    complex, dimension (:,:), intent (in) :: fa
    integer :: ix, iy
    real, dimension (ny+1,nx+1) :: fyx
    integer :: unit=gridout_unit

    call ktox (fa, fyx)

    do ix=1, nx+1
       do iy=1, ny+1
          write (unit,'(3(1x,es15.5))') x(ix), y(iy), fyx(iy,ix)
       end do
       write (unit,*)
    end do

  end subroutine gridout_kspace

!!$  subroutine gridout_file_kspace (fp, fa)
!!$
!!$    use fft, only: ktox
!!$    use run_params, only: nx, ny
!!$    use four, only: x, y
!!$    integer, intent (in) :: fp
!!$    complex, dimension (:,:), intent (in) :: fa
!!$    integer :: ix, iy
!!$    real, dimension (ny+1,nx+1) :: fyx
!!$
!!$    call ktox (fa, fyx)
!!$
!!$    do ix=1, nx+1
!!$       do iy=1, ny+1
!!$          write (fp,'(2f10.5,es15.5)') x(ix), y(iy), fyx(iy,ix)
!!$       end do
!!$       write (fp,*)
!!$    end do
!!$
!!$  end subroutine gridout_file_kspace
!!$
!!$  subroutine gridout_file_kspace_2 (fp, fa1, fa2)
!!$
!!$    use fft, only: ktox
!!$    use run_params, only: nx, ny
!!$    use four, only: x, y
!!$    integer, intent (in) :: fp
!!$    complex, dimension (:,:), intent (in) :: fa1, fa2
!!$    integer :: ix, iy
!!$    real, dimension (ny+1,nx+1) :: fyx1, fyx2
!!$
!!$    call ktox (fa1, fyx1)
!!$    call ktox (fa2, fyx2)
!!$
!!$    do ix=1, nx+1
!!$       do iy=1, ny+1
!!$          write (fp,'(2f10.5,2es15.5)') x(ix), y(iy), fyx1(iy,ix), fyx2(iy,ix)
!!$       end do
!!$       write (fp,*)
!!$    end do
!!$
!!$  end subroutine gridout_file_kspace_2
!!$
!!$  subroutine gridout_file_kspace_4 (fp, fa1, fa2, fa3, fa4)
!!$
!!$    use fft, only: ktox
!!$    use run_params, only: nx, ny
!!$    use four, only: x, y
!!$    integer, intent (in) :: fp
!!$    complex, dimension (:,:), intent (in) :: fa1, fa2, fa3, fa4
!!$    integer :: ix, iy
!!$    real, dimension (ny+1,nx+1) :: fyx1, fyx2, fyx3, fyx4
!!$
!!$    call ktox (fa1, fyx1)
!!$    call ktox (fa2, fyx2)
!!$    call ktox (fa3, fyx3)
!!$    call ktox (fa4, fyx4)
!!$
!!$    do ix=1, nx+1
!!$       do iy=1, ny+1
!!$          write (fp,'(2f10.5,4es15.5)') x(ix), y(iy), fyx1(iy,ix), &
!!$               & fyx2(iy,ix), fyx3(iy,ix), fyx4(iy,ix)
!!$       end do
!!$       write (fp,*)
!!$    end do
!!$
!!$  end subroutine gridout_file_kspace_4

  ! for coding simplicity --- may be slow due to if statements in the loop
  subroutine gridout_file_kspace (fp, fa1, fa2, fa3, fa4)

    use fft, only: ktox
    use run_params, only: nx, ny
    use four, only: x, y
    integer, intent (in) :: fp
    complex, dimension (:,:), intent (in) :: fa1
    complex, dimension (:,:), intent (in), optional :: fa2, fa3, fa4
    integer :: ix, iy
    !real, dimension (ny+1,nx+1) :: fyx1, fyx2, fyx3, fyx4
    real, dimension (:,:), allocatable :: fyx1, fyx2, fyx3, fyx4

    allocate (fyx1(ny+1,nx+1), fyx2(ny+1,nx+1), fyx3(ny+1,nx+1), fyx4(ny+1,nx+1))

    call ktox (fa1, fyx1)
    if (present(fa2)) call ktox (fa2, fyx2)
    if (present(fa3)) call ktox (fa3, fyx3)
    if (present(fa4)) call ktox (fa4, fyx4)

    do ix=1, nx+1
       do iy=1, ny+1
          write (fp,'(3(1x,es15.5))',advance='no') x(ix), y(iy), fyx1(iy,ix)
          if (present(fa2)) write (fp, '(1x,es15.5)',advance='no') fyx2(iy,ix)
          if (present(fa3)) write (fp, '(1x,es15.5)',advance='no') fyx3(iy,ix)
          if (present(fa4)) write (fp, '(1x,es15.5)',advance='no') fyx4(iy,ix)
          write (fp, '()')
       end do
       write (fp,*)
    end do

    deallocate (fyx1, fyx2, fyx3, fyx4)

  end subroutine gridout_file_kspace

end module diagnostics
