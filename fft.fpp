# include "define.inc"

module fft

# if FFT == _FFTW3_
  use, intrinsic :: iso_c_binding
# endif

  use constants, only: kind_id

  implicit none
 
  public :: init_fft, finish_fft
  public :: xtok, ktox
  public :: nkx, nkxh, nky
# if FFT == _FFTW3_
  public :: make_fftw3_plan
# endif

  private

  logical :: initialized=.false.
  logical :: debug=.false.

  integer (kind=kind_id) :: p2df=0, p2db=0
  integer :: nkx, nky, nkxh
# if FFT == _FFTW3_
  ! support both single and double
# ifdef DBLE
  real (C_DOUBLE), pointer :: in2d(:,:)
  complex (C_DOUBLE_COMPLEX), pointer :: out2d(:,:)
# else
  real (C_FLOAT),  pointer :: in2d(:,:)
  complex (C_FLOAT_COMPLEX), pointer :: out2d(:,:)
# endif

  type (C_PTR) :: pr, pc

  ! FFTW variables
  integer :: fftw3_switch
  include 'fftw3.f03'
  ! Following parameters are defined in fftw3.f03
  !  integer(C_INT), parameter :: FFTW_FORWARD = -1
  !  integer(C_INT), parameter :: FFTW_BACKWARD = +1
  !  integer(C_INT), parameter :: FFTW_MEASURE = 0
  !  integer(C_INT), parameter :: FFTW_EXHAUSTIVE = 8
  !  integer(C_INT), parameter :: FFTW_PATIENT = 32
  !  integer(C_INT), parameter :: FFTW_ESTIMATE = 64
  !  integer(C_INT), parameter :: FFTW_WISDOM_ONLY = 2097152
  integer, parameter :: flag_estimate = FFTW_ESTIMATE, &
       flag_measure = FFTW_MEASURE, flag_patient = FFTW_PATIENT, &
       flag_exhaustive = FFTW_EXHAUSTIVE, flag_wisdom_only = FFTW_WISDOM_ONLY
# elif FFT == _FFTW_
  integer, parameter :: FFTW_FORWARD=-1, FFTW_BACKWARD=1
  integer, parameter :: FFTW_REAL_TO_COMPLEX=-1, FFTW_COMPLEX_TO_REAL=1
  integer, parameter :: FFTW_ESTIMATE=0, FFTW_MEASURE=1
  integer, parameter :: FFTW_IN_PLACE=8, FFTW_USE_WISDOM=16

  complex, dimension (:,:), allocatable :: out2d ! temporary work array
# endif

contains

  subroutine init_fft

    use run_params, only: nx, ny
    use file_utils, only: stdout_unit

# if FFT == _FFTW_
    integer :: fftw_flgs
# endif
    if (debug) write (stdout_unit,*) 'Initializing fft ...'

    nkx = (nx-1)/3*2 + 1   ! This is always odd
    nky = (ny-1)/3 + 1
    nkxh = (nkx+1) / 2

# if FFT == _FFTW3_
# ifdef DBLE
    pr = fftw_alloc_real (int(ny * nx, C_SIZE_T))
    pc = fftw_alloc_complex (int((ny/2+1) * nx, C_SIZE_T))
# else
    pr = fftwf_alloc_real (int(ny * nx, C_SIZE_T))
    pc = fftwf_alloc_complex (int((ny/2+1) * nx, C_SIZE_T))
# endif
    call c_f_pointer (pr, in2d, [ny,nx])
    call c_f_pointer (pc, out2d, [ny/2+1,nx])

    call read_fftw3_parameters    ! parse fftw flags

    call make_fftw3_plan          ! make FFTW3 plan
# elif FFT == _FFTW_
    fftw_flgs = FFTW_ESTIMATE
!    fftw_flgs = FFTW_MEASURE + FFTW_USE_WISDOM
    call rfftw2d_f77_create_plan (p2df, ny, nx, FFTW_REAL_TO_COMPLEX, fftw_flgs)
    call rfftw2d_f77_create_plan (p2db, ny, nx, FFTW_COMPLEX_TO_REAL, fftw_flgs)

    if(.not.allocated(out2d)) allocate(out2d(ny/2+1,nx))
    out2d(1:ny/2+1,1:nx)=cmplx(0.,0.)
# endif
    initialized = .true.

    if (debug) write(stdout_unit,*) 'Initializing fft done'

  end subroutine init_fft

  subroutine finish_fft
# if FFT == _FFTW3_
# ifdef DBLE
    call dfftw_destroy_plan (p2df)
    call dfftw_destroy_plan (p2db)
    call fftw_free (pr)
    call fftw_free (pc)
# else
    call sfftw_destroy_plan (p2df)
    call sfftw_destroy_plan (p2db)
    call fftwf_free (pr)
    call fftwf_free (pc)
# endif
# elif FFT == _FFTW_
    call rfftwnd_f77_destroy_plan (p2df)
    call rfftwnd_f77_destroy_plan (p2db)

    if(allocated(out2d)) deallocate(out2d)
# endif
  end subroutine finish_fft

  ! forward transform
  subroutine xtok (fyx, fa)

    use run_params, only: nx, ny, nthreads_ktox
    real, dimension (:,:), intent (in) :: fyx
    complex, dimension (:,:), intent (out) :: fa
    integer :: iky
!$OMP PARALLEL NUM_THREADS(nthreads_ktox)
# if FFT == _FFTW3_
!$OMP WORKSHARE
    in2d(:ny,:nx) = fyx(:ny,:nx)
!$OMP END WORKSHARE
!$OMP MASTER
# ifdef DBLE
    call dfftw_execute (p2df)
# else
    call sfftw_execute (p2df)
# endif
# elif FFT == _FFTW_
    out2d=cmplx(0.,0.)
!$OMP MASTER
    call rfftwnd_f77_one_real_to_complex (p2df, fyx(:ny,:nx), out2d)
# endif
!$OMP END MASTER
!$OMP BARRIER
!$OMP DO PRIVATE(iky)
    do iky=1, nky
       fa(1:nkxh,iky) = out2d(iky,1:nkxh) / (nx*ny)
       fa(nkxh+1:nkx,iky) = out2d(iky,nx-nkxh+2:nx) / (nx*ny)
    end do
!$OMP END DO
!$OMP END PARALLEL
  end subroutine xtok

  ! backward transform
  subroutine ktox (fa, fyx)

    use run_params, only: nx, ny, nthreads_ktox

    complex, dimension (:,:), intent(in) :: fa
    real, dimension (:,:), intent(out) :: fyx
    integer :: iky

    ! The following line slows down the code needlessly.  Commented out.
!    out2d=cmplx(0.,0.)
!$OMP PARALLEL DEFAULT(shared) NUM_THREADS(nthreads_ktox)
!$OMP DO PRIVATE(iky)
    do iky=1, nky
       out2d(iky,1:nkxh) = fa(1:nkxh,iky)
       out2d(iky,nkxh+1:nx-nkxh+1) = 0.0
       out2d(iky,nx-nkxh+2:nx) = fa(nkxh+1:nkx,iky)
    end do
!$OMP END DO NOWAIT
!$OMP WORKSHARE
    out2d(nky+1:, :) = 0.0
!$OMP END WORKSHARE
!$OMP MASTER
# if FFT == _FFTW3_
# ifdef DBLE
    call dfftw_execute (p2db)
# else
    call sfftw_execute (p2db)
# endif
    fyx(:ny,:nx) = in2d(:ny,:nx)
# elif FFT == _FFTW_
    call rfftwnd_f77_one_complex_to_real (p2db, out2d, fyx(:ny,:nx))
    ! The following line slows down the code needlessly.  Commented out.
!    out2d=cmplx(0.,0.)
# endif
!$OMP END MASTER
!$OMP BARRIER
!$OMP WORKSHARE
    fyx(:ny,nx+1) = fyx(:ny,1)
    fyx(ny+1,:) = fyx(1,:)
!$OMP END WORKSHARE
!$OMP END PARALLEL

  end subroutine ktox

# if FFT == _FFTW3_
! make FFTW3 plan
  subroutine make_fftw3_plan

    use run_params, only: nx, ny, nthreads_fft, fftw3_wisdom_file
    use file_utils, only: stdout_unit, error_unit

    integer :: fftw_flgs, iret
    integer (C_INT) :: ret
    character (len=256) :: fftw3_wisdom_file_name

    fftw3_wisdom_file_name = trim (fftw3_wisdom_file) // C_NULL_CHAR

!# ifdef OPENMP
    ! initialize FFTW thread routines
!$    call dfftw_init_threads (iret)
!$    if (iret == 0) then
!$       write (error_unit(), *) "Error in FFTW3 thread initialization."
!$       stop
!$    end if
    ! enable multi-thread calculation
!$    call dfftw_plan_with_nthreads(nthreads_fft)
!# endif
    fftw_flgs = fftw3_switch

    ! try to import wisdom file; if import fails, create new one
    ret = fftw_import_wisdom_from_filename (fftw3_wisdom_file_name)
    if (ret == 0) then
       write (stdout_unit, *) "No wisdom data found."
       write (stdout_unit, *) "Writing new FFTW wisdom data."
    end if
# ifdef DBLE
    call dfftw_plan_dft_r2c_2d (p2df, ny, nx, in2d, out2d, fftw_flgs)
    call dfftw_plan_dft_c2r_2d (p2db, ny, nx, out2d, in2d, fftw_flgs)
# else
    call sfftw_plan_dft_r2c_2d (p2df, ny, nx, in2d, out2d, fftw_flgs)
    call sfftw_plan_dft_c2r_2d (p2db, ny, nx, out2d, in2d, fftw_flgs)
# endif
    if (p2df*p2db == 0) then
       write (error_unit(), *) "Error creating FFTW plan."
       stop
    end if
    ret = fftw_export_wisdom_to_filename (fftw3_wisdom_file_name)
    if (ret == 0) then
       write (error_unit(), *) "Error exporting wisdom data to file."
       stop
    end if

  end subroutine make_fftw3_plan

  ! parse FFTW3 flags
  subroutine read_fftw3_parameters

    use run_params, only: fftw3_flag
    use file_utils, only: error_unit
    use text_options, only: text_option, get_option_value

    type (text_option), parameter :: fftw3opts(10) = (/ &
         & text_option ('FFTW_ESTIMATE', flag_estimate), &
         & text_option ('FFTW_MEASURE', flag_measure), &
         & text_option ('FFTW_PATIENT', flag_patient), &
         & text_option ('FFTW_EXHAUSTIVE', flag_exhaustive), &
         & text_option ('FFTW_WISDOM_ONLY', flag_wisdom_only), &
         & text_option ('ESTIMATE', flag_estimate), &
         & text_option ('MEASURE', flag_measure), &
         & text_option ('PATIENT', flag_patient), &
         & text_option ('EXHAUSTIVE', flag_exhaustive), &
         & text_option ('WISDOM_ONLY', flag_wisdom_only) /)

    call get_option_value (fftw3_flag, fftw3opts, fftw3_switch, &
         & error_unit(), "fftw3_flag in params")

  end subroutine read_fftw3_parameters
# endif

end module fft
