module four

  use file_utils, only: stdout_unit, error_unit

  implicit none

  public :: init_four, finish_four
  public :: get_vector, laplacian, lapinv
  public :: poisson_bracket, l2square, integrate
  public :: lx, ly, x, y, kx, ky, ikx, iky
  public :: delx, dely
  public :: kperp2

  private

  interface l2square
     module procedure l2square_xspace, l2square_kspace
  end interface
  interface integrate
     module procedure integrate_xspace, integrate_kspace
     module procedure integrate_kspace2
  end interface

  logical :: initialized=.false.
  logical :: debug=.false.

  real :: lx, ly
  real :: delx, dely
  real, dimension (:), allocatable :: x, y, kx, ky
  real, dimension (:,:), allocatable :: kperp2
  integer, allocatable :: ikx(:), iky(:)

contains

  subroutine init_four

    use constants, only: pi, zi
    use fft, only: nkx, nkxh, nky
    use run_params, only: nx, ny, x0, y0

    integer :: ix, iy

    if(debug) write(stdout_unit,*) 'Initializing four ...'

    lx = 2.*pi*x0
    ly = 2.*pi*y0
    delx = lx/nx
    dely = ly/ny
    if(.not.allocated(x)) allocate (x(nx+1))
    if(.not.allocated(y)) allocate (y(ny+1))
    x(1:nx+1) = (/ (delx*ix, ix=0,nx) /)
    y(1:ny+1) = (/ (dely*iy, iy=0,ny) /)

    if(.not.allocated(kx)) allocate (kx(nkx))
    if(.not.allocated(ky)) allocate (ky(nky))
    if(.not.allocated(kperp2)) allocate(kperp2(nkx,nky))

    if(.not.allocated(ikx)) allocate (ikx(nkx))
    if(.not.allocated(iky)) allocate (iky(nky))

    ikx(1:nkx)=(/ (ix-1 , ix=1,nkxh), (ix-nkx-1,ix=nkxh+1,nkx) /)
    iky(1:nky)=(/ (iy-1 , iy=1,nky) /)

    kx(1:nkx) = ikx(1:nkx)/x0
    ky(1:nky) = iky(1:nky)/y0

    kperp2(1:nkx,1:nky) = spread(kx**2, 2, nky) + spread(ky**2, 1, nkx)

    initialized=.true.

    if(debug) write(stdout_unit,*) 'Initializing four done'

  end subroutine init_four

  subroutine finish_four

    if(allocated(x)) deallocate (x)
    if(allocated(y)) deallocate (y)
    if(allocated(kx)) deallocate (kx)
    if(allocated(ky)) deallocate (ky)
    if(allocated(kperp2)) deallocate (kperp2)
    if (allocated(ikx)) deallocate (ikx)
    if (allocated(iky)) deallocate (iky)

  end subroutine finish_four

  subroutine ddx (in, out)

    use constants, only: zi
    use fft, only: nky
    complex, dimension(:,:), intent(in) :: in
    complex, dimension(:,:), intent(out) :: out

    out = zi * spread(kx,2,nky) * in

  end subroutine ddx

  subroutine ddy (in, out)

    use constants, only: zi
    use fft, only: nkx
    complex, dimension (:,:), intent (in) :: in
    complex, dimension (:,:), intent (out) :: out

    out = zi * spread(ky,1,nkx) * in

  end subroutine ddy

  subroutine laplacian (in, out)

    complex, dimension (:,:), intent (in) :: in
    complex, dimension (:,:), intent (out) :: out

    out = - kperp2 * in

  end subroutine laplacian

  subroutine lapinv (in, out)

    complex, dimension (:,:), intent (in) :: in
    complex, dimension (:,:), intent (out) :: out

    where (kperp2 ==0)
       out = 0.0
    elsewhere
       out = - in / kperp2
    end where

  end subroutine lapinv

  subroutine poisson_bracket (vectx, vecty, gin, hout)

    use fft, only: ktox, nkx, nky
    use run_params, only: nx, ny
    real, dimension (:,:), intent (in) :: vectx, vecty
    complex, dimension (:,:), intent (in) :: gin
    real, dimension (:,:), intent (out) :: hout
    complex, dimension (nkx,nky) :: ctmp
    real, dimension (ny+1,nx+1) :: rtmp

    call ddx (gin, ctmp)
    call ktox (ctmp, rtmp)
    hout = vectx * rtmp

    call ddy (gin, ctmp)
    call ktox (ctmp, rtmp)
    hout = hout + vecty * rtmp

  end subroutine poisson_bracket

  subroutine get_vector (aa, vectx, vecty)

    use fft, only: ktox, nkx, nky
    complex, dimension (:,:), intent (in) :: aa
    real, dimension (:,:), intent (out) :: vectx, vecty
    complex, dimension (nkx,nky) :: ctmp

    call ddy (aa, ctmp)
    call ktox (ctmp, vectx)
    call ddx (-aa, ctmp)
    call ktox (ctmp, vecty)

  end subroutine get_vector

  function l2square_xspace (fyx) result (l2square)
    !  1    Lx     1    Ly
    ! -- int  dx  -- int  dy  |fyx|^2
    ! Lx    0     Ly    0
    use run_params, only: nx, ny
    real, dimension (:,:), intent (in) :: fyx
    real :: l2square

    l2square = integrate_xspace(fyx(:ny,:nx)**2)

  end function l2square_xspace

  function l2square_kspace (fa) result (l2square)
    !  1    Lx     1    Ly
    ! -- int  dx  -- int  dy  |f|^2
    ! Lx    0     Ly    0
    complex, dimension (:,:), intent (in) :: fa
    real :: l2square

    l2square = ( sum(real(conjg(fa(:,1))*fa(:,1))) &
         & + 2.0 * sum(real(conjg(fa(:,2:))*fa(:,2:))) )

  end function l2square_kspace

  ! This is slow.  Should avoid heavy use.
  function integrate_xspace (fyx) result (integral)
    !  1    Lx     1    Ly
    ! -- int  dx  -- int  dy  fyx
    ! Lx    0     Ly    0
    use run_params, only: nx, ny
    real, dimension (:,:), intent (in) :: fyx
    real :: integral

    integral = sum(fyx(:ny,:nx)) / (nx*ny)

  end function integrate_xspace

  function integrate_kspace (fa) result (integral)
    !  1    Lx     1    Ly
    ! -- int  dx  -- int  dy  |f|
    ! Lx    0     Ly    0
    complex, dimension (:,:), intent (in) :: fa
    real :: integral

    integral = ( sum(cabs(fa(:,1))) + 2.0 * sum(cabs(fa(:,2:))) )

  end function integrate_kspace

  function integrate_kspace2 (fa,fb) result (integral)
    !  1    Lx     1    Ly
    ! -- int  dx  -- int  dy  fa*fb
    ! Lx    0     Ly    0
    complex, dimension (:,:), intent (in) :: fa,fb
    real :: integral
    
    integral = real(sum(fa(:,1)*conjg(fb(:,1)))) + 2.*real(sum(fa(:,2:)*conjg(fb(:,2:))))
    
  end function integrate_kspace2

end module four
