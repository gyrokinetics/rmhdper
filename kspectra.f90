module kspectra

  implicit none

  public :: init_polar_spectrum, get_polar_spectrum, finish_polar_spectrum
  public :: get_ktransfer, get_ktransfer2
  public :: nkpolar, kpavg

  private

  integer :: nkpolar ! number of k's in shell-averaged spectra
  integer, dimension (:,:), allocatable :: polar_index ! (1:nkx,1:nky)->(1:nkpolar)
  real, dimension (:), allocatable :: kpavg

contains

  subroutine init_polar_spectrum

    use file_utils, only: open_output_file
    use fft, only: nkx, nky
    use four, only: ky, kperp2

    real, dimension (:), allocatable :: kpavg_lim
    real :: dkp, kperp
    integer :: ik, it, j

! NOTE: In this routine, a square domain is assumed! (nx=ny)
! In read_parameters, write_kspec, write_ktrans => .false. if nx /= ny

    if (allocated(kpavg)) return

    ! Determine limits of kperp spectra bins
    dkp = ky(2)
    nkpolar = int(ky(nky)/dkp) ! is equal to nky-1
    allocate (kpavg_lim(nkpolar+1))
    kpavg_lim = (/ (dkp*(j-0.5), j=1, nkpolar+1) /)

    allocate (kpavg(nkpolar))
    kpavg = (/ (dkp * j, j=1, nkpolar) /)

    allocate (polar_index(nkx,nky))
    polar_index = 0

    do ik=1, nky
       do it=1, nkx
          kperp = sqrt(kperp2(it,ik))
          do j=1, nkpolar
             if ( (kperp >= kpavg_lim(j)) .and. (kperp < kpavg_lim(j+1)) ) then
                polar_index(it,ik) = j
                exit
             end if
          end do
       end do
    end do

    deallocate (kpavg_lim)

  end subroutine init_polar_spectrum

! NOTE: polar_index connects a given mode to the correct kperp shell
! NOTE: It is assumed here that weighting factors for ky=0 are already 
! incorporated in ee
  subroutine get_polar_spectrum (ee, ebin)

    use fft, only: nkx, nky

    real, dimension (:,:), intent (in) :: ee   !variable ee by (kx,ky) mode
    real, dimension (:), intent (out) :: ebin  !variable ee in kperp bins
    integer :: ik, it, ip

    ! Initialize ebin
    ebin = 0.0

    ! Loop through all modes and sum number at each kperp
    do ik=1, nky
       do it=1, nkx
          ip = polar_index(it,ik)
          if (ip/=0) ebin(ip) = ebin(ip) + ee(it,ik)
       end do
    end do

  end subroutine get_polar_spectrum

  subroutine finish_polar_spectrum

    ! Deallocate variables
    deallocate (polar_index, kpavg)

  end subroutine finish_polar_spectrum

  ! get shell-to-shell transfer of scalar field sf
  subroutine get_ktransfer2 (sf, ktrans)

    use constants, only: zi
    use fft, only: nkx, nkxh, nky
    use four, only: kx, ky
    use time_fields_vars, only: aphi
    complex, dimension (:,:), intent (in) :: sf
    real, dimension (:,:), intent (out) :: ktrans
    integer :: ikfrom, itfrom, ikto, itto, ikmed, itmed, ipfrom, ipto
    complex :: kdotv
    complex, dimension (nkx,nky) :: avx, avy

    ktrans = 0.0
    avx = zi * spread(ky,1,nkx) * aphi
    avy = - zi * spread(kx,2,nky) * aphi

    do itto=1, nkx
       do ikto=1, nky
          ipto = polar_index(itto,ikto)
          if (ipto==0) cycle

          do itfrom=1, nkx
             do ikfrom=1, nky

                if (itfrom==itto .and. ikfrom==ikto) cycle

                ipfrom = polar_index(itfrom,ikfrom)
                if (ipfrom==0) cycle

                ! first set: k+p+q=0 or k+p-q=0 (k: to, p: from, q: med)
                ! k_med always in the ky<=0 plane
                if ( abs(kx(itfrom) + kx(itto)) <= kx(nkxh) .and. &
                     ky(ikfrom) + ky(ikto) <= ky(nky) ) then
                   ! k_to + k_from = k_med: Use complex conjugate mediator
                   !                        since there is no ky<0 mode
                   itmed = sum( minloc( abs(kx(:) - kx(itto) - kx(itfrom)) ) )
                   ikmed = sum( minloc( abs(ky(:) - ky(ikto) - ky(ikfrom)) ) )
                   if (abs(kx(itfrom) + kx(itto) - kx(itmed)) > 1.e-10) then
                      print *, 'kx not in good shape in the first set', &
                           kx(itfrom), kx(itto), kx(itmed)
                      stop
                   end if
                   if (abs(ky(ikfrom) + ky(ikto) - ky(ikmed)) > 1.e-10) then
                      print *, 'ky not in good shape in the first set', &
                           ky(ikfrom), ky(ikto), ky(ikmed)
                      stop
                   end if

!!$                   write (99,*) 'from: ', itfrom, ikfrom, kx(itfrom), ky(ikfrom)
!!$                   write (99,*) 'to:   ', itto,   ikto,   kx(itto),   ky(ikto)
!!$                   write (99,*) 'med:  ', itmed,  ikmed,  kx(itmed),  ky(ikmed)
!!$                   write (99,*) '==========================================='

!!$                   kdotv = conjg( kx(itfrom) * avx(itmed,ikmed) &
!!$                        + ky(ikfrom) * avy(itmed,ikmed) )
!!$                   ktrans(ipfrom,ipto) = ktrans(ipfrom,ipto) &
!!$                        - real(zi * kdotv * sf(itfrom,ikfrom) * sf(itto,ikto))
                   kdotv = conjg( kx(itto) * avx(itmed,ikmed) &
                        + ky(ikto) * avy(itmed,ikmed) )
                   ktrans(ipfrom,ipto) = ktrans(ipfrom,ipto) &
                        + real(zi * kdotv * sf(itfrom,ikfrom) * sf(itto,ikto))
                end if

                ! second set: k-p+q=0 or k-p-q=0 (k: to, p: from, q: med)
                if ( abs(kx(itto) - kx(itfrom)) <= kx(nkxh) ) then
                   if (ikfrom <= ikto) then
                      ! k_to = k_from + k_med
                      ikmed = sum( minloc( abs(ky(:) - ky(ikto) + ky(ikfrom)) ) )
                      itmed = sum( minloc( abs(kx(:) - kx(itto) + kx(itfrom)) ) )
                      if (abs(kx(itfrom) - kx(itto) + kx(itmed)) > 1.e-10) then
                         print *, 'kx not in good shape in the second set A', &
                              kx(itfrom), kx(itto), kx(itmed)
                         stop
                      end if
                      if (abs(ky(ikfrom) - ky(ikto) + ky(ikmed)) > 1.e-10) then
                         print *, 'ky not in good shape in the second set A', &
                              ky(ikfrom), ky(ikto), ky(ikmed)
                         stop
                      end if
                      kdotv = kx(itfrom) * avx(itmed,ikmed) &
                           + ky(ikfrom) * avy(itmed,ikmed)
                   else
                      ! k_to = k_from - k_med
                      ikmed = sum( minloc( abs(ky(:) + ky(ikto) - ky(ikfrom)) ) )
                      itmed = sum( minloc( abs(kx(:) + kx(itto) - kx(itfrom)) ) )
                      if (abs(-kx(itfrom) + kx(itto) + kx(itmed)) > 1.e-10) then
                         print *, 'kx not in good shape in the second set B', &
                              kx(itfrom), kx(itto), kx(itmed)
                         stop
                      end if
                      if (abs(-ky(ikfrom) + ky(ikto) + ky(ikmed)) > 1.e-10) then
                         print *, 'ky not in good shape in the second set B', &
                              ky(ikfrom), ky(ikto), ky(ikmed)
                         stop
                      end if
                      kdotv = conjg( kx(itfrom) * avx(itmed,ikmed) &
                           + ky(ikfrom) * avy(itmed,ikmed) )
                   end if
                   ktrans(ipfrom,ipto) = ktrans(ipfrom,ipto) &
                        - real(zi * kdotv * sf(itfrom,ikfrom) * conjg(sf(itto,ikto)))
                end if

             end do
          end do

       end do
    end do

!    stop

  end subroutine get_ktransfer2

  ! get shell-to-shell transfer of enstrophy
  subroutine get_ktransfer (sf, vectx, vecty, ktrans)

    use run_params, only: nx, ny
    use fft, only: nkx, nky, ktox
    use four, only: get_vector, poisson_bracket !, integrate
    complex, dimension (:,:), intent (in) :: sf
    real, dimension (:,:), intent (in) :: vectx, vecty
    real, dimension (:,:), intent (out) :: ktrans
    integer :: ipfrom, ipto
    complex, dimension (nkx,nky) :: sffrom, sfto
    ! if memory allows, it's quicker to prepare field for each shell
    real, dimension (ny+1,nx+1,nkpolar) :: sfr, pbr

    ! 0.4 sec w/ 64x64 in mac
    do ipto=1, nkpolar
       sfto = (0.0,0.0)
       where (polar_index == ipto) sfto = sf
       call ktox (sfto, sfr(:,:,ipto))
    end do

    ! 0.8 sec w/ 64x64 in mac
    do ipfrom=1, nkpolar
       sffrom = (0.0,0.0)
       where (polar_index == ipfrom) sffrom = sf
       call poisson_bracket (vectx, vecty, sffrom, pbr(:,:,ipfrom))
    end do

    ! 1.6 sec w/ 64x64 in mac when function used negligible when function unused
    ktrans=0.0
    do ipto=1, nkpolar
       do ipfrom=1, nkpolar
!!$          ktrans(ipfrom,ipto) = -integrate(sfr(:,:,ipto)*pbr(:,:,ipfrom))
          ktrans(ipfrom,ipto) = - sum(sfr(:ny,:nx,ipto)*pbr(:ny,:nx,ipfrom)) &
               / (nx*ny)
       end do
    end do

  end subroutine get_ktransfer

end module kspectra
