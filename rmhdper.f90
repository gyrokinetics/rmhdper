program rmhdper
  use job_manage, only: time_message
  use file_utils, only: stdout_unit, error_unit
  use run_params, only: nstep
  use tint, only: time_advance
  use tint, only: dt_cfl, ndtcheck, dtupdate
  use diagnostics, only: loop_diagnostics, final_diagnostics
  use save_restore, only: nsave, save_status

  implicit none

  logical :: debug=.false.
  integer :: it=0
!  integer :: time_begin, time_end, t2, t_rate
  real :: time_total(2) = 0.

  call init_modules

  ! cpu_time returns incorrect values for multi-threaded code,
  ! replaced it with system_clock
  !  call system_clock (time_begin)
  call time_message(.false.,time_total,' Total')

  if(debug) write(stdout_unit,*) 'Initial diagnostics ...'
  call loop_diagnostics (0)
  if(debug) write(stdout_unit,*) 'Initial diagnostics done'

  do it=1, nstep
     if(debug .and. mod(it,100)==1) write(stdout_unit,*) 'step = ', it
     call time_advance (it)
     call loop_diagnostics (it)
     if(ndtcheck>0) then
        if(mod(it, ndtcheck)==0) call dtupdate
     end if
     if(nsave>0) then
        if(mod(it, nsave)==0) call save_status
     endif
  end do

  !  call system_clock (time_end, t_rate)
  call time_message(.false.,time_total,' Total')
  
  ! system_clock has lower precision than cpu_time, adjusted output
!  write (*,"('# result:', /,' CPU time: ', F10.5, ' sec')") &
!       & (time_end - time_begin)/real(t_rate)
  write (*,"('# result:', /,' elapse time: ', g12.5, ' sec')") &
       & time_total(1)

  if (ndtcheck>0) write(stdout_unit,'(1x,a,es10.3)') 'dt_cfl = ',dt_cfl

  if(debug) write(stdout_unit,*) 'Final diagnostics ...'
  call final_diagnostics
  if(debug) write(stdout_unit,*) 'Final diagnostics done'

  call finish_modules

contains

  subroutine init_modules
    use file_utils, only: init_file_utils
    use fft, only: init_fft
    use four, only: init_four
    use run_params, only: init_run_params
    use tint, only: init_tint
    use diagnostics, only: init_diagnostics
    use save_restore, only: init_save_restore
    use time_fields_vars, only: init_time_fields_vars
!$    use threads, only: init_threads

    logical :: list

    call init_file_utils(list,name='rmhdper')
    if(list) stop 'job fork is not supported currently'
    call init_run_params
    call init_fft
    call init_four
    call init_time_fields_vars

    ! Initializing Automatic Tuning System:
    ! Since init_threads uses the true initial condition and breaks it, we need
    ! to invoke init_tint twice in order to set and reset initial conditions.
    ! No harm as of this revision: 2016/4/13
    !$ call init_tint
    !$ call init_threads

    call init_tint
    call init_diagnostics
    call init_save_restore

  end subroutine init_modules

  subroutine finish_modules
    use file_utils, only: finish_file_utils
    use fft, only: finish_fft
    use four, only: finish_four
    use run_params, only: finish_run_params
    use tint, only: finish_tint
    use diagnostics, only: finish_diagnostics
    use save_restore, only: finish_save_restore
    use time_fields_vars, only: finish_time_fields_vars

    call finish_save_restore
    call finish_diagnostics
    call finish_tint
    call finish_time_fields_vars
    call finish_four
    call finish_fft
    call finish_run_params
    call finish_file_utils

  end subroutine finish_modules
end program rmhdper
