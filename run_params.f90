module run_params

  !$ use omp_lib, only: omp_get_max_threads
  use file_utils, only: stdout_unit, error_unit

  implicit none

  public :: init_run_params, finish_run_params

  public :: nx, ny
  public :: x0, y0
  public :: delt
  public :: nu, eta, nuh, etah, di
  public :: muv, mub
  public :: is_diss_omg, is_diss_psi
  public :: nupar, nuhpar, muvpar, is_diss_vz
  public :: etapar, etahpar, mubpar, is_diss_bz
  public :: magneto
  public :: nstep, nwrite
  public :: autotune, nthreads_fft, nthreads_ktox, nthreads_ab3
  public :: fftw3_flag, fftw3_wisdom_file

  private

  logical :: initialized=.false.
  logical :: debug=.false.

  integer :: nx, ny  ! grid points
  real :: x0, y0     ! box size/2pi
  real :: delt       ! time step
  real :: nu, eta    ! dissipations
  real :: nuh, etah  ! hyper-dissipations
  real :: muv, mub   ! friction
  real :: di         ! Hall effect
  real :: nupar, nuhpar, muvpar ! dissipation for vz
  real :: etapar, etahpar, mubpar ! dissipation for bz
  logical :: is_diss_omg ! w/ or w/o dissipation
  logical :: is_diss_psi ! w/ or w/o dissipation
  logical :: is_diss_vz  ! w/ or w/o dissipation
  logical :: is_diss_bz  ! w/ or w/o dissipation
  logical :: magneto ! electromagnetic
  integer :: nstep   ! number of steps
  integer :: nwrite  ! interval of data write
  logical :: autotune          ! switch for automatic tuning
  integer :: nthreads_fft      ! number of threads for FFTW3
  integer :: nthreads_ktox     ! number of threads for ktox OpenMP
  integer :: nthreads_ab3      ! number of threads for ab3 OpenMP
  character (len=128) :: fftw3_flag         ! FFTW3 planning flag
  character (len=256) :: fftw3_wisdom_file  ! FFTW3 wisdom file location

contains

  subroutine init_run_params

    if(debug) write(stdout_unit,*) 'Initializing run_params ...'

    ! set up default values
    x0 = 1.0
    y0 = 1.0
    nx = 32
    ny = 32
    magneto = .true.
    delt = 1.e-3
    nu = 1.e-3
    eta = 1.e-3
    nuh = 0.
    etah = 0.
    muv = 0.
    mub = 0.
    di = 0.
    nupar = 0.
    nuhpar = 0.
    muvpar = 0.
    etapar = 0.
    etahpar = 0.
    mubpar = 0.
    is_diss_omg = .false.
    is_diss_psi = .false.
    is_diss_vz = .false.
    is_diss_bz = .false.
    nwrite = 100
    nstep = 1000
    autotune = .true.
    nthreads_fft = 1
    ! Set default number of threads from the environmental variable
    ! OMP_NUM_THREADS
!$  nthreads_fft = omp_get_max_threads()
    nthreads_ktox = nthreads_fft
    nthreads_ab3 = nthreads_fft
    fftw3_flag = 'FFTW_ESTIMATE'
    fftw3_wisdom_file = './my_wisdom.dat'

    call read_parameters

    ! Hall-MHD
    if(di/=0.) magneto=.true.

    ! w/ or w/o dissipation
    if (nu+nuh+muv /= 0.) is_diss_omg=.true.
    if (eta+etah+mub /= 0.) is_diss_psi = .true.
    if (nupar+nuhpar+muvpar /= 0.) is_diss_vz=.true.
    if (etapar+etahpar+mubpar /= 0.) is_diss_bz=.true.

    initialized = .true.

    if(debug) write(stdout_unit,*) 'Initializing run_params done'

  end subroutine init_run_params

  subroutine finish_run_params
    ! nothing to do
  end subroutine finish_run_params

  subroutine read_parameters

    use file_utils, only: input_unit_exist

    logical :: ex
    integer :: unit

    namelist /params/ x0, y0, nx, ny, magneto, &
         & delt, nu, eta, nuh, etah, muv, mub, di, nwrite, nstep

    namelist /perform/ autotune, nthreads_fft, nthreads_ktox, nthreads_ab3, &
         & fftw3_flag, fftw3_wisdom_file

    unit = input_unit_exist('params',ex)
    if(ex) read(unit,params)

    unit = input_unit_exist ('perform',ex)
    if (ex) read (unit, perform)

  end subroutine read_parameters

end module run_params
