# include "define.inc"

module save_restore

  use file_utils, only: stdout_unit, error_unit
# ifdef NETCDF
  use netcdf, only: NF90_NOERR
  use netcdf_utils, only: kind_nf
  use netcdf_utils, only: netcdf_error
# endif

  implicit none

  public :: init_save_restore, finish_save_restore
  public :: save_status, restore_status
  public :: nsave
  public :: ns_saved
  
  private

  logical :: initialized=.false.
  logical :: debug=.false.

  integer :: nsave
  integer :: ns_save    ! Number of time steps to save (1<= ns_save <= nsmax)
  integer :: ns_saved=1 ! Number of time steps saved in restart file.
                        ! This can be different from ns_save.
  
# ifdef NETCDF
  integer (kind_nf) :: ncsave
  integer (kind_nf) :: st
  integer (kind_nf) :: timeid, dtimeid
  integer (kind_nf) :: aomgid, apsiid, avzid, abzid
  integer (kind_nf) :: domgid, dpsiid, dvzid, dbzid
  integer (kind_nf) :: frcvid, frcbid
  integer (kind_nf) :: seedid, lseedid
  integer (kind_nf) :: aphieqid, apsieqid, abzeqid, avzeqid
  integer (kind_nf) :: equilid
# endif

contains

  subroutine init_save_restore
    
    use time_fields_vars, only: nsmax

    if(debug) write(stdout_unit,*) 'Initializing save ...'
    
    nsave = 0
    ns_save = nsmax
    call read_parameters

    if (ns_save > nsmax .or. ns_save < 1) then
       write(stdout_unit,*) 'Warning: ns_save must be in [1,nsmax]. makes it nsmax'
       ns_save = nsmax
    endif

    initialized = .true.

    if(debug) write(stdout_unit,*) 'Initializing save done'

  contains

    subroutine read_parameters

      use file_utils, only: input_unit_exist
      logical :: ex
      integer :: unit

      namelist /save_restore/ nsave, ns_save
      unit = input_unit_exist('save_restore', ex)
      if (ex) read(unit, save_restore)

    end subroutine read_parameters

  end subroutine init_save_restore

  subroutine finish_save_restore
    ! nothing to do
  end subroutine finish_save_restore

  subroutine save_status

# ifdef NETCDF
    use netcdf, only: NF90_CLOBBER
    use netcdf, only: NF90_BYTE
    use netcdf, only: nf90_create, nf90_close
    use netcdf, only: nf90_def_dim, nf90_def_var
    use netcdf, only: nf90_enddef
    use netcdf, only: nf90_put_var
    use netcdf_utils, only: netcdf_real, get_netcdf_code_precision
    use netcdf_utils, only: netcdf_int
    use file_utils, only: run_name
    use fft, only: nkx, nky
    use run_params, only: magneto, di
    use time_fields_vars, only: time, dtime, aomg, apsi, avz, abz
    use time_fields_vars, only: domg, dpsi, dvz, dbz
    use time_fields_vars, only: drive_v, drive_b
    use time_fields_vars, only: aphi_eq, apsi_eq, abz_eq, avz_eq
    use time_fields_vars, only: is_equil
    use convert, only: c2r
    use ran, only: get_rnd_seed_length, get_rnd_seed
    
    integer (kind_nf) :: ridim, kxdim, kydim, nssdim, nssdim1
    real :: ri(2,nkx,nky),ri2(2,nkx,nky,ns_save)
    integer, allocatable :: seed(:)
    integer :: lseed
    integer :: i
    
    if(debug) write(stdout_unit,*) 'Save Status at t = ',time
    
    ! initialize .nc file
    netcdf_real = get_netcdf_code_precision()
    
    ! open netcdf file
    st = nf90_create(trim(run_name)//'.save.nc', NF90_CLOBBER, ncsave)
    if (st /= NF90_NOERR) call netcdf_error(st,file=trim(run_name)//'.save.nc')
    
    ! define dimensions
    st = nf90_def_dim (ncsave, 'ri', 2, ridim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='ri')
    st = nf90_def_dim (ncsave, 'kx', nkx, kxdim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='kx')
    st = nf90_def_dim (ncsave, 'ky', nky, kydim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='ky')
    st = nf90_def_dim (ncsave, 'ns_save', ns_save, nssdim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='ns_save')
    if (ns_save /= 1) then
       st = nf90_def_dim (ncsave, 'ns_save1', ns_save-1, nssdim1)
       if (st /= NF90_NOERR) call netcdf_error(st, dim='ns_save1')
    end if

    ! define variables
    st = nf90_def_var (ncsave, 'time', netcdf_real, timeid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='time')
    st = nf90_def_var (ncsave, 'dtime', netcdf_real, (/ nssdim /), dtimeid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='dtime')
    
    st = nf90_def_var (ncsave, 'aomg', netcdf_real, &
         & (/ ridim, kxdim, kydim, nssdim /), aomgid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='aomg')
    if (magneto) then
       st = nf90_def_var (ncsave, 'apsi', netcdf_real, &
            & (/ ridim, kxdim, kydim, nssdim /), apsiid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='apsi')
       if (di /= 0.) then
          st = nf90_def_var (ncsave, 'avz', netcdf_real, &
               & (/ ridim, kxdim, kydim, nssdim /), avzid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='avz')
          st = nf90_def_var (ncsave, 'abz', netcdf_real, &
               & (/ ridim, kxdim, kydim, nssdim /), abzid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='abz')
       endif
    endif

    if (ns_save /= 1) then
       st = nf90_def_var (ncsave, 'domg', netcdf_real, &
            & (/ ridim, kxdim, kydim, nssdim1 /), domgid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='domg')
       if (magneto) then
          st = nf90_def_var (ncsave, 'dpsi', netcdf_real, &
               & (/ ridim, kxdim, kydim, nssdim1 /), dpsiid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='dpsi')
          if (di /= 0.) then
             st = nf90_def_var (ncsave, 'dvz', netcdf_real, &
                  & (/ ridim, kxdim, kydim, nssdim1 /), dvzid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='dvz')
             st = nf90_def_var (ncsave, 'dbz', netcdf_real, &
                  & (/ ridim, kxdim, kydim, nssdim1 /), dbzid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='dbz')
          end if
       end if
    end if

    st = nf90_def_var (ncsave, 'drive_v', netcdf_real, &
         & (/ ridim, kxdim, kydim, nssdim1 /), frcvid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='drive_v')
    if (magneto) then
       st = nf90_def_var (ncsave, 'drive_b', netcdf_real, &
            & (/ ridim, kxdim, kydim, nssdim1 /), frcbid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='drive_b')
    end if

    lseed = get_rnd_seed_length()
    st = nf90_def_dim (ncsave, 'lseed', lseed, lseedid)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='lseed')

    st = nf90_def_var (ncsave, 'seed', netcdf_int, &
         & (/ lseedid /), seedid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='seed')

    st = nf90_def_var (ncsave, 'is_equil', NF90_BYTE, equilid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='is_equil')

    if (is_equil) then
       st = nf90_def_var (ncsave, 'aphi_eq', netcdf_real, &
            & (/ ridim, kxdim, kydim /), aphieqid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='aphi_eq')
       if(magneto) then
          st = nf90_def_var (ncsave, 'apsi_eq', netcdf_real, &
               & (/ ridim, kxdim, kydim /), apsieqid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='apsi_eq')
          if (di /= 0.) then
             st = nf90_def_var (ncsave, 'avz_eq', netcdf_real, &
                  & (/ ridim, kxdim, kydim /), avzeqid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='avz_eq')
             st = nf90_def_var (ncsave, 'abz_eq', netcdf_real, &
                  & (/ ridim, kxdim, kydim /), abzeqid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='abz_eq')
          endif
       endif
    endif

    st = nf90_enddef (ncsave)
    if (st /= NF90_NOERR) call netcdf_error(st)

    ! write time
    st = nf90_put_var (ncsave, timeid, time)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, timeid)
    st = nf90_put_var (ncsave, dtimeid, dtime(1:ns_save))
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dtimeid)

    ! write fields
    call c2r (aomg(:,:,0:ns_save-1), ri2(:,:,:,1:ns_save))
    st = nf90_put_var (ncsave, aomgid, ri2)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, aomgid)
    if (magneto) then
       call c2r (apsi(:,:,0:ns_save-1), ri2(:,:,:,1:ns_save))
       st = nf90_put_var (ncsave, apsiid, ri2)
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, apsiid)
       if (di /= 0.) then
          call c2r (avz(:,:,0:ns_save-1), ri2(:,:,:,1:ns_save))
          st = nf90_put_var (ncsave, avzid, ri2)
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, avzid)
          call c2r (abz(:,:,0:ns_save-1), ri2(:,:,:,1:ns_save))
          st = nf90_put_var (ncsave, abzid, ri2)
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, abzid)
       endif
    end if
    
    if (ns_save /= 1) then
       call c2r (domg(:,:,1:ns_save-1), ri2(:,:,:,1:ns_save-1))
       st = nf90_put_var (ncsave, domgid, ri2(:,:,:,1:ns_save-1))
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, domgid)
       if (magneto) then
          call c2r (dpsi(:,:,1:ns_save-1), ri2(:,:,:,1:ns_save-1))
          st = nf90_put_var (ncsave, dpsiid, ri2(:,:,:,1:ns_save-1))
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dpsiid)
          if (di /= 0.) then
             call c2r (dvz(:,:,1:ns_save-1), ri2(:,:,:,1:ns_save-1))
             st = nf90_put_var (ncsave, dvzid, ri2(:,:,:,1:ns_save-1))
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dvzid)
             call c2r (dbz(:,:,1:ns_save-1), ri2(:,:,:,1:ns_save-1))
             st = nf90_put_var (ncsave, dbzid, ri2(:,:,:,1:ns_save-1))
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dbzid)
          end if
       end if
    end if

    call c2r (drive_v(:,:,1:ns_save-1), ri2(:,:,:,1:ns_save-1))
    st = nf90_put_var (ncsave, frcvid, ri2(:,:,:,1:ns_save-1))
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, frcvid)
    if (magneto) then
       call c2r (drive_b(:,:,1:ns_save-1), ri2(:,:,:,1:ns_save-1))
       st = nf90_put_var (ncsave, frcbid, ri2(:,:,:,1:ns_save-1))
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, frcbid)
    end if

    if (.not.allocated(seed)) allocate(seed(lseed))
    call get_rnd_seed(seed)
    st = nf90_put_var (ncsave, seedid, seed)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, seedid)
    if (allocated(seed)) deallocate(seed)

    if (is_equil) then
       i=1
    else
       i=0
    end if
    st = nf90_put_var (ncsave, equilid, i)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, equilid)
       
    if (is_equil) then
       call c2r (aphi_eq, ri)
       st = nf90_put_var (ncsave, aphieqid, ri)
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, aphieqid)
       if (magneto) then
          call c2r (apsi_eq, ri)
          st = nf90_put_var (ncsave, apsieqid, ri)
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, apsieqid)
          if (di /= 0.) then
             call c2r (avz_eq, ri)
             st = nf90_put_var (ncsave, avzeqid, ri)
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, avzeqid)
             call c2r (abz_eq, ri)
             st = nf90_put_var (ncsave, abzeqid, ri)
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, abzeqid)
          endif
       endif
    endif
    
    st = nf90_close (ncsave)
    if (st /= NF90_NOERR) call netcdf_error(st)

# endif

  end subroutine save_status

  subroutine restore_status

# ifdef NETCDF
    use netcdf, only: NF90_NOWRITE
    use netcdf, only: nf90_open, nf90_close
    use netcdf, only: nf90_inq_dimid, nf90_inquire_dimension
    use netcdf, only: nf90_inq_varid, nf90_get_var
    use netcdf_utils, only: netcdf_real, get_netcdf_code_precision
    use netcdf_utils, only: check_netcdf_file_precision
    use file_utils, only: run_name
    use fft, only: nkx, nky
    use run_params, only: magneto, di
    use run_params, only: nu, nuh, eta, etah
    use run_params, only: muv, mub
    use time_fields_vars, only: time, dtime, aomg, apsi, avz, abz
    use time_fields_vars, only: domg, dpsi, dvz, dbz
    use time_fields_vars, only: drive_v, drive_b
    use time_fields_vars, only: aphi_eq, apsi_eq, abz_eq, avz_eq
    use time_fields_vars, only: is_equil, init_equil_vars
    use time_fields_vars, only: nsmax
    use convert, only: r2c
    use ran, only: init_ranf
    
    integer (kind_nf) :: ridim, kxdim, kydim, nssdim
    real :: ri(2,nkx,nky),ri2(2,nkx,nky,nsmax+1)
    integer :: i
    integer :: lseed
    integer, allocatable :: seed(:)
    
    if(debug) write(stdout_unit,*) 'Restore Status ...'
    
    ! open netcdf file
    st = nf90_open(trim(run_name)//'.save.nc', NF90_NOWRITE, ncsave)
    if (st /= NF90_NOERR) call netcdf_error(st,file=trim(run_name)//'.save.nc')
    
    ! check real
    if (netcdf_real == 0) netcdf_real = get_netcdf_code_precision()
    
    ! inquire dimension id
    st = nf90_inq_dimid (ncsave, 'ri', ridim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='ri')
    st = nf90_inq_dimid (ncsave, 'kx', kxdim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='kx')
    st = nf90_inq_dimid (ncsave, 'ky', kydim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='ky')
    st = nf90_inq_dimid (ncsave, 'ns_save', nssdim)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='ns_save')
    
    ! inquire dimension size
    st = nf90_inquire_dimension (ncsave, ridim, len=i)
    if (st /= NF90_NOERR) call netcdf_error(st, dimid=ridim)
    if (i /= 2 ) write(error_unit(),*) 'Restart error: ri=? ',i
    st = nf90_inquire_dimension (ncsave, kxdim, len=i)
    if (st /= NF90_NOERR) call netcdf_error(st, dimid=kxdim)
    if (i /= nkx ) write(error_unit(),*) 'Restart error: kx=? ',i
    st = nf90_inquire_dimension (ncsave, kydim, len=i)
    if (st /= NF90_NOERR) call netcdf_error(st, dimid=kydim)
    if (i /= nky ) write(error_unit(),*) 'Restart error: ky=? ',i

    st = nf90_inquire_dimension (ncsave, nssdim, len=ns_saved)
    if (st /= NF90_NOERR) call netcdf_error(st, dimid=nssdim)

    ! inquire variable id
    st = nf90_inq_varid (ncsave, 'time', timeid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='time')
    st = nf90_inq_varid (ncsave, 'dtime', dtimeid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='dtime')

    st = nf90_inq_varid (ncsave, 'aomg', aomgid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='aomg')
    if (magneto) then
       st = nf90_inq_varid (ncsave, 'apsi', apsiid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='apsi')
       if (di /= 0.) then
          st = nf90_inq_varid (ncsave, 'avz', avzid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='avz')
          st = nf90_inq_varid (ncsave, 'abz', abzid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='abz')
       endif
    endif

    if (ns_saved /= 1) then
       st = nf90_inq_varid (ncsave, 'domg', domgid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='domg')
       if (magneto) then
          st = nf90_inq_varid (ncsave, 'dpsi', dpsiid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='dpsi')
          if (di /= 0.) then
             st = nf90_inq_varid (ncsave, 'dvz', dvzid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='dvz')
             st = nf90_inq_varid (ncsave, 'dbz', dbzid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='dbz')
          end if
       end if
    end if
    
    st = nf90_inq_varid (ncsave, 'drive_v', frcvid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='drive_v')
    if (magneto) then
       st = nf90_inq_varid (ncsave, 'drive_b', frcbid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='drive_b')
    end if

    st = nf90_inq_dimid (ncsave, 'lseed', lseedid)
    if (st /= NF90_NOERR) call netcdf_error(st, dim='lseed')
    st = nf90_inquire_dimension (ncsave, lseedid, len=lseed)
    if (st /= NF90_NOERR) call netcdf_error(st, dimid=lseedid)
    st = nf90_inq_varid (ncsave, 'seed', seedid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='seed')
    
    st = nf90_inq_varid (ncsave, 'is_equil', equilid)
    if (st /= NF90_NOERR) call netcdf_error(st, var='is_equil')
    
    ! read variables
    st = nf90_get_var (ncsave, timeid, time)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, timeid)
    st = nf90_get_var (ncsave, dtimeid, dtime(1:ns_saved))
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dtimeid)
    
    st = nf90_get_var (ncsave, aomgid, ri2(:,:,:,1:ns_saved))
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, aomgid)
    call r2c (aomg(:,:,0:ns_saved-1), ri2(:,:,:,1:ns_saved))
    if (magneto) then
       st = nf90_get_var (ncsave, apsiid, ri2(:,:,:,1:ns_saved))
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, apsiid)
       call r2c (apsi(:,:,0:ns_saved-1), ri2(:,:,:,1:ns_saved))
       if (di /= 0.) then
          st = nf90_get_var (ncsave, avzid, ri2(:,:,:,1:ns_saved))
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, avzid)
          call r2c (avz(:,:,0:ns_saved-1), ri2(:,:,:,1:ns_saved))
          st = nf90_get_var (ncsave, abzid, ri2(:,:,:,1:ns_saved))
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, abzid)
          call r2c (abz(:,:,0:ns_saved-1), ri2(:,:,:,1:ns_saved))
       end if
    end if
    
    if (ns_saved /= 1) then
       st = nf90_get_var (ncsave, domgid, ri2(:,:,:,1:ns_saved-1))
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, domgid)
       call r2c (domg(:,:,1:ns_saved-1), ri2(:,:,:,1:ns_saved-1))
       if (magneto) then
          st = nf90_get_var (ncsave, dpsiid, ri2(:,:,:,1:ns_saved-1))
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dpsiid)
          call r2c (dpsi(:,:,1:ns_saved-1), ri2(:,:,:,1:ns_saved-1))
          if (di /= 0.) then
             st = nf90_get_var (ncsave, dvzid, ri2(:,:,:,1:ns_saved-1))
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dvzid)
             call r2c (dvz(:,:,1:ns_saved-1), ri2(:,:,:,1:ns_saved-1))
             st = nf90_get_var (ncsave, dbzid, ri2(:,:,:,1:ns_saved-1))
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, dbzid)
             call r2c (dbz(:,:,1:ns_saved-1), ri2(:,:,:,1:ns_saved-1))
          end if
       end if
    end if

    st = nf90_get_var (ncsave, frcvid, ri2(:,:,:,1:ns_saved-1))
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, frcvid)
    call r2c (drive_v(:,:,1:ns_saved-1), ri2(:,:,:,1:ns_saved-1))
    if (magneto) then
       st = nf90_get_var (ncsave, frcbid, ri2(:,:,:,1:ns_saved-1))
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, frcbid)
       call r2c (drive_b(:,:,1:ns_saved-1), ri2(:,:,:,1:ns_saved-1))
    end if

    if (.not.allocated(seed)) allocate(seed(lseed))
    st = nf90_get_var (ncsave, seedid, seed)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, seedid)
    call init_ranf(randomize=.false.,init_seed=seed)
    if (allocated(seed)) deallocate(seed)

    st = nf90_get_var (ncsave, equilid, i)
    if (st /= NF90_NOERR) call netcdf_error(st, ncsave, equilid)
    if (i == 1) then
       is_equil = .true.
    else
       if (is_equil) then
          write(error_unit(),*) 'No equilibrium data is stored!'
          stop
       endif
    endif

    if (is_equil) then

       call init_equil_vars
       
       st = nf90_inq_varid (ncsave, 'aphi_eq', aphieqid)
       if (st /= NF90_NOERR) call netcdf_error(st, var='aphi_eq')
       if (magneto) then
          st = nf90_inq_varid (ncsave, 'apsi_eq', apsieqid)
          if (st /= NF90_NOERR) call netcdf_error(st, var='apsi_eq')
          if (di /= 0.) then
             st = nf90_inq_varid (ncsave, 'abz_eq', abzeqid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='abz_eq')
             st = nf90_inq_varid (ncsave, 'avz_eq', avzeqid)
             if (st /= NF90_NOERR) call netcdf_error(st, var='avz_eq')
          endif
       endif

       st = nf90_get_var (ncsave, aphieqid, ri)
       if (st /= NF90_NOERR) call netcdf_error(st, ncsave, aphieqid)
       call r2c (aphi_eq, ri)
       if (magneto) then
          st = nf90_get_var (ncsave, apsieqid, ri)
          if (st /= NF90_NOERR) call netcdf_error(st, ncsave, apsieqid)
          call r2c (apsi_eq, ri)
          if (di /= 0.) then
             st = nf90_get_var (ncsave, abzeqid, ri)
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, abzeqid)
             call r2c (abz_eq, ri)
             st = nf90_get_var (ncsave, avzid, ri)
             if (st /= NF90_NOERR) call netcdf_error(st, ncsave, avzeqid)
             call r2c (avz_eq, ri)
          endif
       endif
    endif
    
    st = nf90_close (ncsave)
    if (st /= NF90_NOERR) call netcdf_error(st)

    if(debug) write(stdout_unit,*) 'Status at t = ',time,' is Restored'

# endif

  end subroutine restore_status

end module save_restore
