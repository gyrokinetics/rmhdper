set terminal postscript enhanced color eps "Times-Roman" 30 size 8in,7in
set output "ot_magneto_balcheck.eps"

set multiplot layout 2,2

set title "OT magneto\nwith {/Symbol n}, {/Symbol n}_h, {/Symbol m}_{/Times-Italic v}, {/Symbol h},  {/Symbol h}_h, {/Symbol m}_{/Times-Italic b}\ndif"
set xlabel "Time"
set ylabel "Error [%]"
set key top right Left samplen 2 width -5
set yrange [0:]
plot \
	"ot_magneto_balcheck.bal" u 1:($2*100.) w l \
	lw 4 lc 1 title "Energy" , \
	"ot_magneto_balcheck.bal" u 1:($4*100.) w l \
	lw 4 lc 2 title "{/Symbol y}^2", \
	"ot_magneto_balcheck.bal" u 1:($6*100.) w l \
	lw 4 lc 3 title "Cross Helicity", \

        set title "OT magneto\nwith {/Symbol n}, {/Symbol n}_h, {/Symbol m}_{/Times-Italic v}, {/Symbol h},  {/Symbol h}_h, {/Symbol m}_{/Times-Italic b}\nsum"
set xlabel "Time"
set ylabel "Error [%]"
set key top right Left samplen 2 width -5
set yrange [0:]
plot \
	"ot_magneto_balcheck.bal" u 1:($3*100.) w l \
	lw 4 lc 1 title "Energy" , \
	"ot_magneto_balcheck.bal" u 1:($5*100.) w l \
	lw 4 lc 2 title "{/Symbol y}^2", \
	"ot_magneto_balcheck.bal" u 1:($7*100.) w l \
	lw 4 lc 3 title "Cross Helicity", \

set title "OT magneto\nwith {/Symbol n}, {/Symbol n}_h, {/Symbol m}_{/Times-Italic v}, {/Symbol h},  {/Symbol h}_h, {/Symbol m}_{/Times-Italic b}\nwith random forcing\ndif"
set xlabel "Time"
set ylabel "Error [%]"
set key top right Left samplen 2 width -5
#set yrange [0:]
plot \
	"ot_magneto_balcheck_force.bal" u 1:($2*100.) w l \
	lw 4 lc 1 title "Energy" , \
	"ot_magneto_balcheck_force.bal" u 1:($4*100.) w l \
	lw 4 lc 2 title "{/Symbol y}^2", \
	"ot_magneto_balcheck_force.bal" u 1:($6*100.) w l \
	lw 4 lc 3 title "Cross Helicity", \

set title "OT magneto\nwith {/Symbol n}, {/Symbol n}_h, {/Symbol m}_{/Times-Italic v}, {/Symbol h},  {/Symbol h}_h, {/Symbol m}_{/Times-Italic b}\nwith random forcing\nsum"
set xlabel "Time"
set ylabel "Error [%]"
set key top right Left samplen 2 width -5
#set yrange [0:]
plot \
	"ot_magneto_balcheck_force.bal" u 1:($3*100.) w l \
	lw 4 lc 1 title "Energy" , \
	"ot_magneto_balcheck_force.bal" u 1:($5*100.) w l \
	lw 4 lc 2 title "{/Symbol y}^2", \
	"ot_magneto_balcheck_force.bal" u 1:($7*100.) w l \
	lw 4 lc 3 title "Cross Helicity", \
