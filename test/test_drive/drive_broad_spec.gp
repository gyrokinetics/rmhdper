set terminal postscript enhanced color eps "Times-Roman" 30
set output "drive_broad_spec.eps"

power=1.e-2
diss=1.e-2

kf=3
krange=2

fac=2.*erfc(-kf/krange)*sqrt(pi)/2.

f(x)=power/(2*diss*fac)*x**(-2)*exp(-(x-kf)**2/krange**2)

set log
set format x "10^{%L}"
set format y "10^{%L}"
set xtics add ("1" 1)
set ytics add ("1" 1)
set xrange [1.:20.]
set yrange [1.e-16:]

set xlabel "{/Times-Italic k}"
set ylabel "{/Times-Italic E}({/Times-Italic k})"

set title "Fully linear driven test simulation"
set key bottom left Left samplen 2 width -5
plot \
	"drive_fluid_broad.kspec" every :100 u 2:4 w lp ps 2 lw 4 title "", \
	f(x) lw 4 dt 2 title "Theoretical Curve"
