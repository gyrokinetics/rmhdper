set terminal postscript enhanced color eps "Times-Roman" 30 size 8in,6in
set output "drive_constpow.eps"

set multiplot layout 2,2

### Fluid case

power=1.e-2
diss=1.e-2
kf=3

set title "Neutral Fluid Case\n{/Symbol n}=0.01, {/Symbol D}{/Times-Italic t}=2{/Symbol \264}10^{-3}, {/Times-Italic k}_{drive}=3{/Symbol \261}0.01"
set xlabel "Time"
set ylabel "Energy"
E_sat=power/(diss*2*kf*kf)
set y2tics ("{/Times-Italic E}_{sat}" E_sat)
set yrange [0.:E_sat*1.1]
set key at 50,0.02 Left samplen 2 width -1
plot \
	"drive_fluid.energy" u 1:2 w l lw 4 title "Energy", \
	power*x title "{/Times-Italic P t}" lw 4, \
	E_sat title "" lc 0 lw 4 dt 2

unset y2tics
set auto
unset title
unset ylabel

set title "Reduced MHD Case\n{/Symbol h}=0.01, {/Symbol D}{/Times-Italic t}=2{/Symbol \264}10^{-3}, {/Times-Italic k}_{drive}=3{/Symbol \261}0.01"
set xlabel "Time"
set ylabel "Energy"
E_sat=power/(diss*2*kf*kf)
set y2tics ("{/Times-Italic E}_{sat}" E_sat)
set yrange [0.:E_sat*1.1]
set key at 50,0.02 Left samplen 2 width -1
plot \
	"drive_magneto.energy" u 1:9 w l lw 4 title "Energy", \
	power*x title "{/Times-Italic P t}" lw 4 , \
	E_sat title "" lc 0 lw 4 dt 2

unset y2tics
set auto
unset title


set ylabel 'Energy Change'
set key at 50,0.006 Left samplen 2 width -1
plot \
	"drive_fluid.energy" u 1:3 w l lw 4 title "d{/Times-Italic E}/d{/Times-Italic t}", \
	"drive_fluid.energy" u 1:5 w l lw 4 title "Input Power", \
	"drive_fluid.energy" u 1:($6+$7) w l lw 4 title "Dissipation", \

set key at 50,0.006 Left samplen 2 width -1
plot \
	"drive_magneto.energy" u 1:5 w l lw 4 title "d{/Times-Italic E}/d{/Times-Italic t}", \
	"drive_magneto.energy" u 1:13 w l lw 4 title "Input Power", \
	"drive_magneto.energy" u 1:($14+$15) w l lw 4 title "Dissipation", \

unset multiplot
reset
