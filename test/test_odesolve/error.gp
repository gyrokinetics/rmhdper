set terminal postscript enhanced color eps "Times-Roman" 30
set output "error.eps"

set log
set format x "10^{%L}"
set format y "10^{%L}"

set xlabel "{/Symbol D}{/Times-Italic t}"
set ylabel "Error"

set key bottom right Left samplen 2 width 0

plot \
	x title "{/Symbol \265}{/Symbol D}{/Times-Italic t}" lc 0 dt 1, \
	x**2 title "{/Symbol \265}{/Symbol D}{/Times-Italic t}^2" lc 0 dt 2, \
	x**3 title "{/Symbol \265}{/Symbol D}{/Times-Italic t}^3" lc 0 dt 3, \
	x**4 title "{/Symbol \265}{/Symbol D}{/Times-Italic t}^4" lc 0 dt 4, \
	"error.dat" w lp lc 1 ps 2 pt 6 title "" \
