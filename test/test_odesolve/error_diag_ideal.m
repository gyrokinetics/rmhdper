1;

pkg load io netcdf;

clear all; clc; close all;
is_plot=0;

arg_list=argv();
i=str2num(arg_list{1});
printf("Run = %d\n",i);

basename=sprintf('test_odesolve_ideal%02d',i);
infile=[basename,'.in'];
ncfile=[basename,'.nc'];

namelist=read_namelist(infile);
delt=namelist.params.delt;
modeamp=namelist.tint_knobs.phi_pt_modeamp;
modenum=namelist.tint_knobs.phi_pt_modenum;
nu=namelist.params.nu;

kx=zeros(3,1);
ky=zeros(3,1);
for i=1:length(modenum)/2;
  kx(i)=modenum((i-1)*2+1);
  ky(i)=modenum(i*2);
  fprintf('(kx,ky)=(%d,%d)\n',kx(i),ky(i));
end %for
phip100amp=complex(zeros);
phim100amp=complex(zeros);
phi00p1amp=complex(zeros);
phip1p1amp=complex(zeros);
phim1p1amp=complex(zeros);

for i=1:length(kx)
  if (kx(i) == 1 && ky(i) == 0)
    phip100amp=modeamp(i);
  elseif (kx(i) == 0 && ky(i) == 1)
    phi00p1amp=0.5*modeamp(i);
  elseif (kx(i) == 1 && ky(i) == 1)
    phip1p1amp=0.5*modeamp(i);
  elseif (kx(i) == -1 && ky(i) == 1)
    phim1p1amp=0.5*modeamp(i);
  elseif (kx(i) == -1 && ky(i) == 0)
    phim100amp=modeamp(i);
  else
  end %if
end %for
%%% check reality
if (abs(phip100amp-conj(phim100amp)) != 0.)
  fprintf('Reality condition does not hold. exit\n');
  return;
end %if
fprintf('phi+1 0amp = %f + I %f\n',real(phip100amp),imag(phip100amp));
fprintf('phi 0+1amp = %f + I %f\n',real(phi00p1amp),imag(phi00p1amp));
fprintf('phi+1+1amp = %f + I %f\n',real(phip1p1amp),imag(phip1p1amp));
fprintf('phi-1+1amp = %f + I %f\n',real(phim1p1amp),imag(phim1p1amp));

time=ncread(ncfile,'time');
ntime=length(time);

aphi=ncread(ncfile,'aphi');

nkx=size(aphi,2);
nky=size(aphi,3);

phik=complex(zeros(nkx,nky));
phikp100=complex(zeros(ntime,1));
phik00p1=complex(zeros(ntime,1));
phikp1p1=complex(zeros(ntime,1));
phikm1p1=complex(zeros(ntime,1));
phikp100_soln=complex(zeros(ntime,1));
phik00p1_soln=complex(zeros(ntime,1));
phikp1p1_soln=complex(zeros(ntime,1));
phikm1p1_soln=complex(zeros(ntime,1));

%%% analytic solution
if (abs(phim1p1amp) == 0.)
  fprintf("Driven by (1,1) mode\n");
  c1=0.5*(phip100amp+I*phip1p1amp/abs(phip1p1amp)*conj(phi00p1amp));
  c2=0.5*(phip100amp-I*phip1p1amp/abs(phip1p1amp)*conj(phi00p1amp));
  omg=abs(phip1p1amp);

  phikp100_soln=c1*exp(I*omg*time(:))+c2*exp(-I*omg*time(:));
  phik00p1_soln=I*phip1p1amp/abs(phip1p1amp)*(conj(c1)*exp(-I*omg*time(:))-conj(c2)*exp(I*omg*time(:)));
  phikp1p1_soln=phip1p1amp;
elseif (abs(phip1p1amp) == 0.)
  fprintf("Driven by (-1,1) mode\n");
  c1=0.5*(phip100amp-I*conj(phim1p1amp)/abs(phim1p1amp)*phi00p1amp);
  c2=0.5*(phip100amp+I*conj(phim1p1amp)/abs(phim1p1amp)*phi00p1amp);
  omg=abs(phim1p1amp);

  phikp100_soln=c1*exp(I*omg*time(:))+c2*exp(-I*omg*time(:));
  phik00p1_soln=I*phim1p1amp/abs(phim1p1amp)*(c1*exp(I*omg*time(:))-c2*exp(-I*omg*time(:)));
  phikm1p1_soln=phim1p1amp;
else
  fprintf("No solution. exit\n");
  return;
end %if

ene_t=zeros(ntime,1);
ene_t_soln=zeros(ntime,1);
for itime=1:ntime
  phik(:,:)=complex(aphi(1,:,:,itime),aphi(2,:,:,itime));
  phikp100(itime)=phik(2,1);
  phik00p1(itime)=phik(1,2);
  phikp1p1(itime)=phik(2,2);
  phikm1p1(itime)=phik(3,2);

  ene1=abs(phikp100(itime))**2+abs(phik00p1(itime))**2;
  ene2=2.*abs(phikp1p1(itime))**2+2.*abs(phikm1p1(itime))**2;
  ene_t(itime)=ene1+ene2;
  if (itime==1)
    ene01=ene1;
    ene02=ene2;
  end %if
  ene_t_soln(itime)=ene01+ene02;
  ene_error=abs(ene_t(itime)/ene_t_soln(itime)-1.);
end %for
fprintf("Energy Error : %14.6e %14.6e\n",delt,ene_error);

if (is_plot)
  plot(time,real(phikp100),'r+;Re(\phi_{10});',time,imag(phikp100),'ro;Im(\phi_{10});',
       time,real(phik00p1),'b+;Re(\phi_{01});',time,imag(phik00p1),'bo;Im(\phi_{01});',
       time,real(phikp100_soln),'c-','linewidth',2,time,imag(phikp100_soln),'c--','linewidth',2,
       time,real(phik00p1_soln),'m-','linewidth',2,time,imag(phik00p1_soln),'m--','linewidth',2);
  legend('location','northeastoutside');
  axis([time(1), time(ntime)]);
  xlabel('Time');
  ylabel('Phi');
  drawnow;
  pause;
end %if

error=sqrt(sum(abs((phikp100-phikp100_soln).**2))/ntime) ...
      + sqrt(sum(abs((phik00p1-phik00p1_soln).**2))/ntime) ...
      + sqrt(sum(abs((phikp1p1-phikp1p1_soln).**2))/ntime) ...
      + sqrt(sum(abs((phikm1p1-phikm1p1_soln).**2))/ntime);
fprintf("RMS diff. from anal. soln. :  %14.6e\n",error);

fp=fopen("error.dat","a");
fprintf(fp,"###         Dt          Error\n");
fprintf(fp,"%14.6e %14.6e\n",delt,error);
fclose(fp);
