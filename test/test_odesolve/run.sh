#!/bin/sh

show_usage () {
    echo "Usage: ` basename $0` options [ideal|diss]"
    echo ""
    echo "options:"
    echo " -c(--clean): clean up after run"
    echo " -h(--help): show this help"
    exit 0
}

### check options

# emulate gnu long-options
for arg
do
    delim=""
    case "$arg" in
        --help) args="${args}-h ";;
	--clean) args="${args}-c ";;
	*)
            args="${args} ${delim}${arg}${delim} ";;
    esac
done

# reset the positional parameters to the short options
eval set -- $args
 
while getopts "ch" OPT
do
    case ${OPT} in
	c) OPT_CLEAN=T
	    ;;
	h) show_usage ;;
	\?) show_usage ;;
    esac
done

# more arguments
shift $((${OPTIND} - 1))
if [ $# -ne 0 ]; then
    TYPE=${1}
    iarg=0
    while [ "${*}" ]
    do
	iarg=$((${iarg} + 1))
	argval=${1}
	shift
    done
fi

if [ "X${TYPE}" != "Xdiss" -a "X${TYPE}" != "Xideal" ]; then
    echo "Invalid argument!\n"
    exit
fi

export OMP_NUM_THREADS=1
EXEC=../../rmhdper
BASE=test_odesolve_${TYPE}

DELT=0.1024
NWRITE=1
NSTEP=200
for i in 1 2 3 4 5 6 7 8 9 10 11
#for i in 1
do
	printf -v IN "${BASE}%02d.in" ${i}
	sed "s/_DELT_/${DELT}/;s/_NWRITE_/${NWRITE}/;s/_NSTEP_/${NSTEP}/" ${BASE}.in > ${IN}
	time ${EXEC} ${IN}
	octave -qf error_diag_${TYPE}.m ${i}

	# update params
	DELT=`echo "scale=7;${DELT} / 2.0" | bc`
	NWRITE=`expr ${NWRITE} \* 2`
	NSTEP=`expr ${NSTEP} \* 2`
done

# clean up
if [ ${OPT_CLEAN} ]; then
    for SFX in energy error nc svnrev fields in
    do
	rm -rf ${BASE}??.${SFX}
    done
    rm -rf .${BASE}??.in
fi
