1;

runname=[ "ot_magneto"; ];

for i=1:size(runname,1)

  file0=sprintf('%s/%s.energy',deblank(runname(i,:)),deblank(runname(i,:)));
  file1=sprintf('%s_run1/%s.energy',deblank(runname(i,:)),deblank(runname(i,:)));
  file2=sprintf('%s_run2/%s.energy',deblank(runname(i,:)),deblank(runname(i,:)));
  
  energy_all = load(file0);
  energy_run1 = load(file1);
  energy_run2 = load(file2);
  energy_cont = vertcat(energy_run1,energy_run2(2:size(energy_run2,1),:));

  diff=energy_all-energy_cont;

  fprintf('Test energy of continuation runs %s: ',runname(i,:));
  if (sum(sum(abs(diff))) < 1.e-10)
    fprintf('PASS\n');
  else
    fprintf('FAIL\n');
  end %if

  plot(
      energy_all(:,1),energy_all(:,8),'k-;Kin. Energy;',
      energy_cont(:,1),energy_cont(:,8),'ko',
      energy_all(:,1),energy_all(:,9),'r-;Mag. Energy;',
      energy_cont(:,1),energy_cont(:,9),'ro',
      energy_all(:,1),energy_all(:,3),'g-;A^2;',
      energy_cont(:,1),energy_cont(:,3),'go',
      energy_all(:,1),energy_all(:,4),'b-;Cross Helicity;',
      energy_cont(:,1),energy_cont(:,4),'bo'
    )
  title("Solid line: Single run, Dots: Two separate runs.");
  xlabel("Time");
  print("check_energy.pdf");

end %for

