#!/bin/sh

export OMP_NUM_THREADS=1
EXEC=../../../rmhdper
BASE="ot_magneto"
IN=${BASE}.in
RESTART=${BASE}.save.nc
CLEAN_LIST=".${IN} my_wisdom.dat ${BASE}.error ${BASE}.fields ${BASE}.nc ${BASE}.svnrev"

CWD=`pwd`

### single run
cd ${CWD}/ot_magneto
time ${EXEC} ${IN}
rm -f ${CLEAN_LIST}
cd ${CWD}

### two separate runs
cd ${CWD}/ot_magneto_run1
time ${EXEC} ${IN}
rm -f ${CLEAN_LIST}
cd ${CWD}

cp ot_magneto_run1/${RESTART} ot_magneto_run2
cd ${CWD}/ot_magneto_run2
time ${EXEC} ${IN}
rm -f ${CLEAN_LIST}
cd ${CWD}

### check difference
echo ""
octave -qf check_energy.m
