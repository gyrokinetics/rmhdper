#include "define.inc"

module threads

  implicit none

  public :: init_threads

  private

  logical :: debug=.false.
  integer :: nsteps      ! number of steps used for runtime thread measurements

contains

  subroutine init_threads

    use file_utils, only: stdout_unit
    use run_params, only: autotune, nthreads_fft, nthreads_ab3, nthreads_ktox
    use run_params, only: fftw3_flag, nx, ny
    use time_fields_vars, only: init_time_fields_vars

    integer :: fft_opt, ab3_opt, ktox_opt

    if (debug) write (stdout_unit,*) 'Checking optimal number of threads ...'

    if (autotune) then
       write (*, '(a)') '# searching optimal number of threads.'
    else
       write (*, '(a)') '# using given number of threads.'
    end if

    fft_opt = nthreads_fft
    ab3_opt = nthreads_ab3
    ktox_opt = nthreads_ktox

    ! Larger grid size uses less time steps to find the optimal number
    ! of threads.  Adjusted to take ~1 sec on standard desktop PC.
    nsteps = ((2048/nx)**2.1)+2

    if (autotune) then
# if FFT == _FFTW3_
       call opt_threads ('fft', fft_opt)
# endif
       call opt_threads ('ab3', ab3_opt)
       call opt_threads ('ktox', ktox_opt)
    end if

    call init_time_fields_vars

    write (*, "('# number of threads used :')")
# if FFT == _FFTW3_
    write (*, "(5x ,'fft :', I3)", advance='no') fft_opt
# endif
    write (*, "(5x, 'ab3 :', I3, 5x, 'ktox :', I3)") ab3_opt, ktox_opt

    if (debug) write (stdout_unit,*) 'Calculating number of threads - done.'

  end subroutine init_threads

  subroutine opt_threads (calc_part, omp_num)

    use run_params, only: nthreads_fft, nthreads_ab3, nthreads_ktox
# if FFT == _FFTW3_
    use fft, only: make_fftw3_plan
# endif
    use tint, only: time_advance

    character (*), intent (in) :: calc_part
    integer, intent (inout) :: omp_num    ! optimal number of threads
    integer :: nthreads                   ! current number of threads
    integer :: ln                         ! 2**ln = maximum number of threads
    integer :: i, j
    real :: tmin                          ! shortest execution time
    integer :: t1, t2, t_rate

    select case (calc_part)
    case ("fft")
       write (*, "(' nthreads_fft:', 1x, 'threads', 5x, 'time')")
       nthreads_ktox = 1
       nthreads_ab3 = 1
    case ("ab3")
       write (*, "(' nthreads_ab3:', 1x, 'threads', 5x, 'time')")
       nthreads_ktox = 1
    case ("ktox")
       write (*, "('nthreads_ktox:', 1x, 'threads', 5x, 'time')")
    end select

    ln = int(log(real(omp_num))/log(2.0) + 0.5)

    tmin = 1.0e+10
    do i=0, ln
       nthreads = 2**i

       select case (calc_part)
# if FFT == _FFTW3_
       case ("fft")
          nthreads_fft = nthreads
          call make_fftw3_plan
# endif
       case ("ab3")
          nthreads_ab3 = nthreads
          nthreads_ktox = nthreads
       case ("ktox")
          nthreads_ktox = nthreads
       end select

       call system_clock (t1)
       do j=1, nsteps
          call time_advance (j)
       end do
       call system_clock (t2, t_rate)

       if (tmin > t2-t1) then
          tmin = t2-t1
          omp_num = nthreads
       end if

       write (*, "(12x, I3, 3x, F10.4)") nthreads, (t2-t1)/real(t_rate)
    end do

    select case (calc_part)
# if FFT == _FFTW3_
    case ("fft")
       nthreads_fft = omp_num
       call make_fftw3_plan
# endif
    case ("ab3")
       nthreads_ab3 = omp_num
    case ("ktox")
       nthreads_ktox = omp_num
    end select

  end subroutine opt_threads

end module threads
