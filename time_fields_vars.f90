module time_fields_vars

  use file_utils, only: error_unit, stdout_unit

  implicit none

  public :: init_time_fields_vars, finish_time_fields_vars
  public :: time, dtime, vx, vy, bx, by, aomg, aphi, acur, apsi, avz, abz
  public :: vex, vey
  public :: aomg_eq, aphi_eq, apsi_eq, acur_eq, avz_eq, abz_eq
  public :: nsmax
  public :: drive_v, drive_b
  public :: diss_v, diss_b, diss_bpar, diss_vpar
  public :: init_equil_vars, finish_equil_vars
  public :: is_equil
  
  integer, parameter :: nsmax=3 ! maximum # of steps of scheme
  real :: time
  real :: dtime(nsmax) ! variable time step
  real, dimension (:,:), allocatable :: vx, vy, bx, by
  real, dimension (:,:), allocatable :: vex, vey
  complex, dimension (:,:), allocatable :: aphi, acur
  complex, dimension (:,:,:), allocatable, target :: aomg, apsi, avz, abz
  complex, dimension (:,:,:), allocatable :: domg, dpsi, dvz, dbz
  complex, pointer :: aomg0(:,:), apsi0(:,:), avz0(:,:), abz0(:,:)
  complex, allocatable :: aomg_eq(:,:), apsi_eq(:,:)
  complex, allocatable :: aphi_eq(:,:), acur_eq(:,:)
  complex, allocatable :: avz_eq(:,:), abz_eq(:,:)
  complex, allocatable :: drive_v(:,:,:), drive_b(:,:,:)
  logical :: is_equil = .false.

  real, allocatable :: diss_v(:,:), diss_b(:,:)
  real, allocatable :: diss_bpar(:,:), diss_vpar(:,:)

  ! global-defined temporary variable
  ! this is to reduce frequent allocat/deallocat-ion of temporary variables
  complex, allocatable :: gctmp1(:,:)
  real, allocatable :: grtmp1(:,:)
  
  logical :: initialized=.false.
  logical :: debug=.false.

contains
  
  subroutine init_time_fields_vars

    use fft, only: nkx, nky
    use run_params, only: nx, ny, magneto, di
    use run_params, only: delt

    if(debug) write(stdout_unit,*) 'Initializing time_fields_vars ...'

    if(.not.allocated(aomg)) allocate (aomg(nkx,nky,0:nsmax))
    if(.not.allocated(domg)) allocate (domg(nkx,nky,nsmax))
    if(.not.allocated(aphi)) allocate (aphi(nkx,nky))
    aomg(1:nkx,1:nky,0:nsmax) = cmplx(0.,0.)
    domg(1:nkx,1:nky,1:nsmax) = cmplx(0.,0.)
    aphi(1:nkx,1:nky) =  cmplx(0.,0.)
    aomg0 => aomg(:,:,0)
    
    if(.not.allocated(vx)) allocate (vx(ny+1,nx+1))
    if(.not.allocated(vy)) allocate (vy(ny+1,nx+1))
    vx(1:ny+1,1:nx+1)=0.; vy(1:ny+1,1:nx+1)=0.

    if (magneto) then
       if (.not.allocated(vex)) allocate(vex(ny+1,nx+1))
       if (.not.allocated(vey)) allocate(vey(ny+1,nx+1))
       vex(1:ny+1,1:nx+1)=0.; vey(1:ny+1,1:nx+1)=0.
    end if

    if (magneto) then
       if(.not.allocated(apsi)) allocate (apsi(nkx,nky,0:nsmax))
       if(.not.allocated(dpsi)) allocate (dpsi(nkx,nky,nsmax))
       if(.not.allocated(acur)) allocate (acur(nkx,nky))
       apsi(1:nkx,1:nky,0:nsmax) = cmplx(0.,0.)
       dpsi(1:nkx,1:nky,1:nsmax) = cmplx(0.,0.)
       acur(1:nkx,1:nky) =  cmplx(0.,0.)
       apsi0 => apsi(:,:,0)

       if(.not.allocated(bx)) allocate (bx(ny+1,nx+1))
       if(.not.allocated(by)) allocate (by(ny+1,nx+1))
       bx(1:ny+1,1:nx+1)=0.; by(1:ny+1,1:nx+1)=0.

       if (di /= 0.) then
          if(.not.allocated(avz)) allocate(avz(nkx,nky,0:nsmax))
          if(.not.allocated(dvz)) allocate(dvz(nkx,nky,nsmax))
          if(.not.allocated(abz)) allocate(abz(nkx,nky,0:nsmax))
          if(.not.allocated(dbz)) allocate(dbz(nkx,nky,nsmax))
          avz(1:nkx,1:nky,0:nsmax)=cmplx(0.,0.)
          dvz(1:nkx,1:nky,nsmax)=cmplx(0.,0.)
          abz(1:nkx,1:nky,0:nsmax)=cmplx(0.,0.)
          dbz(1:nkx,1:nky,nsmax)=cmplx(0.,0.)
          avz0 => avz(:,:,0)
          abz0 => abz(:,:,0)
       end if
    end if

    if (.not.allocated(drive_v)) allocate(drive_v(nkx,nky,nsmax))
    drive_v=cmplx(0.,0.)
    if (magneto) then
       if (.not.allocated(drive_b)) allocate(drive_b(nkx,nky,nsmax))
       drive_b=cmplx(0.,0.)
    end if

    if (.not.allocated(diss_v)) allocate(diss_v(nkx,nky))
    diss_v=0.
    if (magneto) then
       if (.not.allocated(diss_b)) allocate(diss_b(nkx,nky))
       diss_b=0.
       if (di /= 0.) then
          if (.not.allocated(diss_bpar)) allocate(diss_bpar(nkx,nky))
          if (.not.allocated(diss_vpar)) allocate(diss_vpar(nkx,nky))
          diss_bpar=0.
          diss_vpar=0.
       end if
    end if
   
    if (.not.allocated(gctmp1)) allocate(gctmp1(nkx,nky))
    gctmp1 = cmplx(0.,0.)
    if (.not.allocated(grtmp1)) allocate(grtmp1(nkx,nky))
    grtmp1 = 0.
    
    time = 0.
    dtime(1:nsmax) = delt
    
    initialized = .true.
    
    if(debug) write(stdout_unit,*) 'Initializing time_fields_vars done'
    
  end subroutine init_time_fields_vars

  subroutine init_equil_vars

    use fft, only: nkx, nky
    use run_params, only: nx, ny, magneto, di
    
    if (is_equil) then
       if(.not.allocated(aomg_eq)) allocate(aomg_eq(nkx,nky))
       if(.not.allocated(aphi_eq)) allocate(aphi_eq(nkx,nky))
       aomg_eq(1:nkx,1:nky) = cmplx(0.,0.)
       aphi_eq(1:nkx,1:nky) = cmplx(0.,0.)
       
       if (magneto) then
          if(.not.allocated(apsi_eq)) allocate(apsi_eq(nkx,nky))
          if(.not.allocated(acur_eq)) allocate(acur_eq(nkx,nky))
          apsi_eq(1:nkx,1:nky) = cmplx(0.,0.)
          acur_eq(1:nkx,1:nky) = cmplx(0.,0.)
          
          if (di /= 0.) then
             if(.not.allocated(avz_eq)) allocate(avz_eq(nkx,nky))
             if(.not.allocated(abz_eq)) allocate(abz_eq(nkx,nky))
             avz_eq(1:nkx,1:nky) = cmplx(0.,0.)
             abz_eq(1:nkx,1:nky) = cmplx(0.,0.)
          end if
       end if
    end if
    
  end subroutine init_equil_vars
  
  subroutine finish_time_fields_vars

    use run_params, only: magneto, di

    if (allocated(aomg)) deallocate (aomg)
    if (allocated(domg)) deallocate (domg)
    if (allocated(aphi)) deallocate (aphi)
    if (allocated(vx)) deallocate (vx)
    if (allocated(vy)) deallocate (vy)
    if (magneto) then
       if (allocated(vex)) deallocate (vex)
       if (allocated(vey)) deallocate (vey)
       if (allocated(apsi)) deallocate (apsi)
       if (allocated(dpsi)) deallocate (dpsi)
       if (allocated(acur)) deallocate (acur)
       if (allocated(bx)) deallocate (bx)
       if (allocated(by)) deallocate (by)
       if (di /= 0.) then
          if (allocated(avz)) deallocate(avz)
          if (allocated(dvz)) deallocate(dvz)
          if (allocated(abz)) deallocate(abz)
          if (allocated(dbz)) deallocate(dbz)
       end if
    endif

    if (allocated(drive_v)) deallocate(drive_v)
    if (magneto) then
       if (allocated(drive_b)) deallocate(drive_b)
    end if

    if (allocated(diss_v)) deallocate(diss_v)
    if (magneto) then
       if (allocated(diss_b)) deallocate(diss_b)
       if (di /= 0.) then
          if (allocated(diss_bpar)) deallocate(diss_bpar)
          if (allocated(diss_vpar)) deallocate(diss_vpar)
       end if
    end if
    
    if (allocated(gctmp1)) deallocate(gctmp1)
    if (allocated(grtmp1)) deallocate(grtmp1)
    
    call finish_equil_vars
    
  end subroutine finish_time_fields_vars

  subroutine finish_equil_vars

    use run_params, only: magneto, di
    
    if (is_equil) then
       if (allocated(aomg_eq)) deallocate (aomg_eq)
       if (allocated(aphi_eq)) deallocate (aphi_eq)
       if (magneto) then
          if (allocated(apsi_eq)) deallocate(apsi_eq)
          if (allocated(acur_eq)) deallocate(acur_eq)
          if (di /= 0.) then
             if (allocated(avz_eq)) deallocate(avz_eq)
             if (allocated(abz_eq)) deallocate(abz_eq)
          end if
       end if
    end if
    
  end subroutine finish_equil_vars
  
end module time_fields_vars
