module tint

  use file_utils, only: error_unit, stdout_unit
  use time_fields_vars, only: nsmax

  implicit none

  public :: init_tint, finish_tint, time_advance
  public :: dt_cfl, ndtcheck, dtupdate

  private

  logical :: initialized=.false.
  logical :: debug=.false.

  logical, parameter :: new_method = .true.
  
  integer :: init_switch
  integer, parameter :: initopt_none = 1, initopt_restart=2, &
       & initopt_ot = 3, &
       & initopt_recs = 4, initopt_recp = 5, initopt_rech = 6, &
       & initopt_zero = 7
  character (len=500) :: init_option
  
  real :: phi_eq_amp, psi_eq_amp, prof_width
  integer :: prof_mode
  logical :: normalize_b
  real :: phi_pt_amp, psi_pt_amp
  integer, parameter :: no_pt_modes=3
  complex :: phi_pt_modeamp(no_pt_modes), psi_pt_modeamp(no_pt_modes)
  integer :: phi_pt_modenum(2,no_pt_modes), psi_pt_modenum(2,no_pt_modes)
  real :: noise(2), noise_sp(2)

  logical :: nonlin = .true.
  logical :: eqzip

  integer :: drv_switch
  integer, parameter :: drvopt_none = 1, &
       & drvopt_random=2, drvopt_constpow=3, drvopt_test=4
  character (len=500) :: drv_option

  real :: drv_amp(2) = 0.
  real :: drv_k = 0., drv_krange = 0.
  real :: drv_norm = 0.
  real, parameter :: drv_krange_sigma=5.

  integer :: nstep_rkg ! number of RKG advance steps
                       ! default: minimal number of steps to achieve deisred order
                       !          with restart data taken into account (nsmax-ns_saved)
                       !      -1: all steps

  integer :: ndtcheck
  real :: dt_cfl=0.
  type :: scheme_factor
     character (len=20) :: name
     real :: a(0:nsmax), b(0:nsmax) ! a(0) is an inverse of the factor
     real :: c(1:nsmax)
  end type scheme_factor
  type (scheme_factor), target :: fac_euler, &
       & fac_ab2, fac_ab2bdf2, fac_ab3, fac_ab3bdf3
  
  character (len=20), parameter :: &
       & name_euler = 'Euler', &
       & name_ab2 = 'AB2', name_ab2bdf2 = 'AB2BDF2', &
       & name_ab3 = 'AB3', name_ab3bdf3 = 'AB3BDF3'

  type (scheme_factor), pointer :: &
       & fac_omg=>null(), fac_psi=>null(), fac_bz=>null(), fac_vz=>null()
  
contains

  subroutine init_tint

    use save_restore, only: ns_saved

    integer :: i

    if(debug) write(stdout_unit,*) 'Initializing tint ...'

    ! set up default values

    nstep_rkg = -2
    
    ndtcheck = 0
    init_option = 'none'
    phi_eq_amp = 0.
    psi_eq_amp = 0.
    prof_width = -.1 ! if specified with init_option='Rec_Porcelli' or
                     ! 'Rec_Harris', this gives scale length of initial
                     ! equilibrium. If negative, it's absolute value gives 
                     ! scale length relative to lx (|prof_width|*lx).
    prof_mode = 1    ! if specified with init_option='Rec_sin',
                     ! this gives mode number of profile.
    normalize_b = .false.

    phi_pt_amp = 0. ! perturbation amplitude of phi
    psi_pt_amp = 0. ! perturbation amplitude of psi
    ! relative amplitude of modes
    phi_pt_modeamp(1:3) = (/ cmplx(1.,1.), cmplx(1.,1.), cmplx(1.,1.) /) 
    psi_pt_modeamp(1:3) = (/ cmplx(1.,1.), cmplx(1.,1.), cmplx(1.,1.) /) 
    ! mode numbers (mode idx, x/y mode)
    do i=1,no_pt_modes
       phi_pt_modenum(1:2,i) = (/ 0, 0 /)
       psi_pt_modenum(1:2,i) = (/ 0, 0 /)
    end do
    noise(2) = 0.0
    noise_sp(2) = 0.
    
    eqzip = .false.

    drv_option='none'
    drv_amp=(/ 0., 0. /)
    drv_k = 0.
    drv_krange = 0.

    nonlin = .true.
    
    call read_parameters
    
    call update_scheme_factor

    call init_cond
    if (nstep_rkg == -2) then
       nstep_rkg = nsmax-ns_saved
    end if
    
    call update_auxiliary

    if (drv_switch == drvopt_constpow .and. sum(drv_amp) /= 0.) call init_forcing

    call init_diss

    initialized = .true.

    if(debug) write(stdout_unit,*) 'Initializing tint done'

  contains

    subroutine init_diss
      use four, only: kperp2
      use time_fields_vars, only: diss_v, diss_b, diss_bpar, diss_vpar
      use run_params, only: magneto, di
      use run_params, only: nu, nuh, muv, eta, etah, mub
      use run_params, only: nupar, nuhpar, muvpar, etapar, etahpar, mubpar
      diss_v(:,:) = muv + nu*kperp2(:,:) + nuh*kperp2(:,:)**2
      if (magneto) then
         diss_b(:,:) = mub + eta*kperp2(:,:) + etah*kperp2(:,:)**2
         if (di /= 0.) then
            diss_bpar(:,:) = mubpar + etapar*kperp2(:,:) + etahpar*kperp2(:,:)**2
            diss_vpar(:,:) = muvpar + nupar*kperp2(:,:) + nuhpar*kperp2(:,:)**2
         end if
      end if
    end subroutine init_diss

  end subroutine init_tint

  subroutine finish_tint
    ! nothing to do
  end subroutine finish_tint

  subroutine time_advance (istep)

    use time_fields_vars, only: time, dtime

    integer, intent (in) :: istep

    time = time + dtime(1)
    if (debug) write(stdout_unit,*) 'time_advance: renew_fields'
    call renew_fields
    if (debug) write(stdout_unit,*) 'time_advance: set_forcing'
    call set_forcing
    if (istep <= nstep_rkg) then
       if (debug) write(stdout_unit,'(1x,a,i0)') 'time_advance: rkg until nstep=',nstep_rkg
       call rkg
       if (debug) write(stdout_unit,*) 'time_advance: add_forcing'
       call add_forcing(rkg_mode=.true.)
    else
       call select_scheme(istep)
       
       if (debug) write(stdout_unit,*) 'time_advance: advect'
       call advect (istep)
       if (debug) write(stdout_unit,*) 'time_advance: dissipate'
       call dissipate (istep)
       if (debug) write(stdout_unit,*) 'time_advance: add_forcing'
       call add_forcing
    end if
    if (debug) write(stdout_unit,*) 'time_advance: update_aux'
    call update_auxiliary
    if (debug) write(stdout_unit,*) 'time_advance done'

  contains

    subroutine select_scheme(istep)

      use time_fields_vars, only: diss_v, diss_b, diss_bpar, diss_vpar
      use run_params, only: magneto, di
      use save_restore, only: ns_saved

      integer, intent(in) :: istep

      if (nsmax == 1) then

         fac_omg => fac_euler
         if (magneto) then
            fac_psi => fac_euler
            if (di /= 0.) then
               fac_bz => fac_euler
               fac_vz => fac_euler
            end if
         end if

      else if (nsmax == 2) then

         if (istep+(ns_saved-1)==1) then
            fac_omg => fac_euler
            if (magneto) then
               fac_psi => fac_euler
               if (di /= 0.) then
                  fac_bz => fac_euler
                  fac_vz => fac_euler
               end if
            end if
         else
            if (sum(abs(diss_v)) < epsilon(0.0)) then
               fac_omg => fac_ab2
            else
               fac_omg => fac_ab2bdf2
            end if
            if (magneto) then
               if (sum(abs(diss_b)) < epsilon(0.0)) then
                  fac_psi => fac_ab2
               else
                  fac_psi => fac_ab2bdf2
               end if
               if (di /= 0.) then
                  if (sum(abs(diss_bpar)) < epsilon(0.0)) then
                     fac_bz => fac_ab2
                  else
                     fac_bz => fac_ab2bdf2
                  end if
                  if (sum(abs(diss_vpar)) < epsilon(0.0)) then
                     fac_vz => fac_ab2
                  else
                     fac_vz => fac_ab2bdf2
                  end if
               end if
            end if
         end if

      else if (nsmax == 3) then

         if (istep+(ns_saved-1)==1) then
            fac_omg => fac_euler
            if (magneto) then
               fac_psi => fac_euler
               if (di /= 0.) then
                  fac_bz => fac_euler
                  fac_vz => fac_euler
               end if
            end if
         else if (istep+(ns_saved-1)==2) then
            if (sum(abs(diss_v)) < epsilon(0.0)) then
               fac_omg => fac_ab2
            else
               fac_omg => fac_ab2bdf2
            end if
            if (magneto) then
               if (sum(abs(diss_b)) < epsilon(0.0)) then
                  fac_psi => fac_ab2
               else
                  fac_psi => fac_ab2bdf2
               end if
               if (di /= 0.) then
                  if (sum(abs(diss_bpar)) < epsilon(0.0)) then
                     fac_bz => fac_ab2
                  else
                     fac_bz => fac_ab2bdf2
                  end if
                  if (sum(abs(diss_vpar)) < epsilon(0.0)) then
                     fac_vz => fac_ab2
                  else
                     fac_vz => fac_ab2bdf2
                  end if
               end if
            end if
         else
            if (sum(abs(diss_v)) < epsilon(0.0)) then
               fac_omg => fac_ab3
            else
               fac_omg => fac_ab3bdf3
            end if
            if (magneto) then
               if (sum(abs(diss_b)) < epsilon(0.0)) then
                  fac_psi => fac_ab3
               else
                  fac_psi => fac_ab3bdf3
               end if
               if (di /= 0.) then
                  if (sum(abs(diss_bpar)) < epsilon(0.0)) then
                     fac_bz => fac_ab3
                  else
                     fac_bz => fac_ab3bdf3
                  end if
                  if (sum(abs(diss_vpar)) < epsilon(0.0)) then
                     fac_vz => fac_ab3
                  else
                     fac_vz => fac_ab3bdf3
                  end if
               end if
            end if
         end if

      end if
      
      if (debug) then
         write(6,'(1x,"  multistep scheme at nstep=",i0)',advance='no') istep
         write(6,'(1x," omg: ",a7)',advance='no') fac_omg%name
         if (magneto) then
            write(6,'(1x," psi: ",a7)',advance='no') fac_psi%name
            if (di /= 0.) then
               write(6,'(1x," vz: ",a7)',advance='no') fac_bz%name
               write(6,'(1x," bz: ",a7)',advance='no') fac_vz%name
            end if
         end if
         write(6,*)
      endif

    end subroutine select_scheme
     
    subroutine renew_fields

      use time_fields_vars, only: aomg, domg, apsi, dpsi
      use time_fields_vars, only: avz, dvz, abz, dbz
      use run_params, only: magneto, di

      aomg(:,:,1:nsmax) = aomg(:,:,0:nsmax-1)
      domg(:,:,2:nsmax) = domg(:,:,1:nsmax-1)
      if (magneto) then
         apsi(:,:,1:nsmax) = apsi(:,:,0:nsmax-1)
         dpsi(:,:,2:nsmax) = dpsi(:,:,1:nsmax-1)
         if (di /= 0.) then
            avz(:,:,1:nsmax) = avz(:,:,0:nsmax-1)
            dvz(:,:,2:nsmax) = dvz(:,:,1:nsmax-1)
            abz(:,:,1:nsmax) = abz(:,:,0:nsmax-1)
            dbz(:,:,2:nsmax) = dbz(:,:,1:nsmax-1)
         end if
      end if

    end subroutine renew_fields

  end subroutine time_advance

  subroutine update_auxiliary
    
    use time_fields_vars, only: aomg0, aphi, apsi0, acur, vx, vy, bx, by
    use run_params, only: magneto
    use four, only: get_vector, laplacian, lapinv

    call lapinv (aomg0, aphi)
    call get_vector (aphi, vx, vy)
    if (magneto) then
       call get_vector (apsi0, bx, by)
       call laplacian (apsi0, acur)
    end if
  end subroutine update_auxiliary

  subroutine rkg
    ! fourth order Runge-Kutta-Gill advance
    use run_params, only: magneto, di
    use fft, only: nkx, nky
    use time_fields_vars, only: aomg, domg, apsi, dpsi
    use time_fields_vars, only: abz, dbz, avz, dvz
    use time_fields_vars, only: dtime
    use time_fields_vars, only: diss_v, diss_b, diss_bpar, diss_vpar
    integer :: irkg
    integer, parameter :: nrkg=4
    real, parameter :: &
         & rk1(nrkg)=(/  .5, 1.-   sqrt(.5), 1.+   sqrt(.5), 1./6. /), &
         & rk2(nrkg)=(/ 0. ,-1.+   sqrt(.5),-1.-   sqrt(.5),-1./3. /), &
         & rk3(nrkg)=(/ 1. , 2.-2.*sqrt(.5), 2.+2.*sqrt(.5), 0.    /), &
         & rk4(nrkg)=(/ 0. ,-2.+3.*sqrt(.5),-2.-3.*sqrt(.5), 0.    /)
    
    complex, dimension (:,:,:), allocatable :: domg_rkg, dpsi_rkg, dbz_rkg, dvz_rkg
    complex, dimension (:,:), allocatable :: aomg_save, apsi_save, abz_save, avz_save
    complex, dimension (:,:), allocatable :: domg_save, dpsi_save, dbz_save, dvz_save

    allocate(domg_rkg(nkx,nky,2)); domg_rkg=0.
    allocate(aomg_save(nkx,nky)); aomg_save=0.
    allocate(domg_save(nkx,nky)); domg_save=0.
    if (magneto) then
       allocate(dpsi_rkg(nkx,nky,2)); dpsi_rkg=0.
       allocate(apsi_save(nkx,nky)); apsi_save=0.
       allocate(dpsi_save(nkx,nky)); dpsi_save=0.
       if (di /= 0.) then
          allocate(dbz_rkg(nkx,nky,2)); dbz_rkg=0.
          allocate(abz_save(nkx,nky)); abz_save=0.
          allocate(dbz_save(nkx,nky)); dbz_save=0.
          allocate(dvz_rkg(nkx,nky,2)); dvz_rkg=0.
          allocate(avz_save(nkx,nky)); avz_save=0.
          allocate(dvz_save(nkx,nky)); dvz_save=0.
       end if
    end if

    ! values of (:,:,1) will be modified during RKG steps
    ! but, it must be saved for the following multistep method.
    aomg_save(:,:)=aomg(:,:,1)
    if (magneto) then
       apsi_save(:,:)=apsi(:,:,1)
       if (di /= 0.) then
          abz_save(:,:)=abz(:,:,1)
          avz_save(:,:)=avz(:,:,1)
       end if
    end if

    do irkg = 1,nrkg
       if (nonlin) call nonlinear_terms

       domg_rkg(:,:,1)=domg(:,:,1)
       if (magneto) then
          dpsi_rkg(:,:,1)=dpsi(:,:,1)
          if (di /= 0.) then
             dbz_rkg(:,:,1)=dbz(:,:,1)
             dvz_rkg(:,:,1)=dvz(:,:,1)
          end if
       end if
       
       if (irkg == 1) then
          domg_save(:,:)=domg(:,:,1)
          if (magneto) then
             dpsi_save(:,:)=dpsi(:,:,1)
             if (di /= 0.) then
                dbz_save(:,:)=dbz(:,:,1)
                dvz_save(:,:)=dvz(:,:,1)
             end if
          end if
       end if
       
       domg_rkg(:,:,1)=domg_rkg(:,:,1)-aomg(:,:,1)*diss_v(:,:)
       aomg(:,:,0) = aomg(:,:,1) + dtime(1) * (rk1(irkg)*domg_rkg(:,:,1)+rk2(irkg)*domg_rkg(:,:,2))
       domg_rkg(:,:,2)=rk3(irkg)*domg_rkg(:,:,1)+rk4(irkg)*domg_rkg(:,:,2)

       if (magneto) then
          dpsi_rkg(:,:,1)=dpsi_rkg(:,:,1)-apsi(:,:,1)*diss_b(:,:)
          apsi(:,:,0) = apsi(:,:,1) + dtime(1) * (rk1(irkg)*dpsi_rkg(:,:,1)+rk2(irkg)*dpsi_rkg(:,:,2))
          dpsi_rkg(:,:,2)=rk3(irkg)*dpsi_rkg(:,:,1)+rk4(irkg)*dpsi_rkg(:,:,2)

          if (di /= 0.) then
             dbz_rkg(:,:,1)=dbz_rkg(:,:,1)-abz(:,:,1)*diss_bpar(:,:)
             abz(:,:,0) = abz(:,:,1) + dtime(1) * (rk1(irkg)*dbz_rkg(:,:,1)+rk2(irkg)*dbz_rkg(:,:,2))
             dbz_rkg(:,:,2)=rk3(irkg)*dbz_rkg(:,:,1)+rk4(irkg)*dbz_rkg(:,:,2)
             
             dvz_rkg(:,:,1)=dvz_rkg(:,:,1)-avz(:,:,1)*diss_vpar(:,:)
             avz(:,:,0) = avz(:,:,1) + dtime(1) * (rk1(irkg)*dvz_rkg(:,:,1)+rk2(irkg)*dvz_rkg(:,:,2))
             dvz_rkg(:,:,2)=rk3(irkg)*dvz_rkg(:,:,1)+rk4(irkg)*dvz_rkg(:,:,2)
          end if
       end if

       call update_auxiliary

       aomg(:,:,1)=aomg(:,:,0)
       if (magneto) then
          apsi(:,:,1)=apsi(:,:,0)
          if (di /= 0.) then
             abz(:,:,1)=abz(:,:,0)
             avz(:,:,1)=avz(:,:,0)
          end if
       end if
    end do

    ! save for multistep method
    domg(:,:,1) = domg_save(:,:)
    aomg(:,:,1) = aomg_save(:,:)
    if (magneto) then
       dpsi(:,:,1) = dpsi_save(:,:)
       apsi(:,:,1) = apsi_save(:,:)
       if (di /= 0.) then
          dbz(:,:,1) = dbz_save(:,:)
          abz(:,:,1) = abz_save(:,:)
          dvz(:,:,1) = dvz_save(:,:)
          avz(:,:,1) = avz_save(:,:)
       end if
    end if

    if (allocated(domg_rkg)) deallocate(domg_rkg)
    if (allocated(dpsi_rkg)) deallocate(dpsi_rkg)
    if (allocated(dbz_rkg)) deallocate(dbz_rkg)
    if (allocated(dvz_rkg)) deallocate(dvz_rkg)
    
    if (allocated(aomg_save)) deallocate(aomg_save)
    if (allocated(apsi_save)) deallocate(apsi_save)
    if (allocated(abz_save)) deallocate(abz_save)
    if (allocated(avz_save)) deallocate(avz_save)
    
    if (allocated(domg_save)) deallocate(domg_save)
    if (allocated(dpsi_save)) deallocate(dpsi_save)
    if (allocated(dbz_save)) deallocate(dbz_save)
    if (allocated(dvz_save)) deallocate(dvz_save)

  end subroutine rkg
  
  subroutine advect (istep)

    use time_fields_vars, only: aomg, domg
    use time_fields_vars, only: apsi, dpsi
    use time_fields_vars, only: avz, dvz, abz, dbz
    use time_fields_vars, only: diss_v, diss_b, diss_bpar, diss_vpar
    use run_params, only: magneto, di
    use save_restore, only: ns_saved

    integer, intent (in) :: istep

    if (nonlin) then
       call nonlinear_terms
    end if

    if (new_method) then

       call multistep_explicit (aomg, domg, fac_omg)
       if (magneto) then
          call multistep_explicit (apsi, dpsi, fac_psi)
          if (di /= 0.) then
             call multistep_explicit (abz, dbz, fac_bz)
             call multistep_explicit (avz, dvz, fac_vz)
          end if
       end if

    else

       !    if (istep == 1) then ! Euler
       if (istep+(ns_saved-1) == 1) then ! Euler
          if (debug) &
               write(6,'(1x,"  multistep scheme at nstep=",i0,1x,"Euler")') istep
          call eulerf (aomg, domg(:,:,1))
          !call multistep_explicit (aomg, domg, fac_euler)
          if (magneto) then
             call eulerf (apsi, dpsi(:,:,1))
             if (di /= 0.) then
                call eulerf (abz, dbz(:,:,1))
                call eulerf (avz, dvz(:,:,1))
             end if
          end if
          !    else if (istep == 2) then ! AB2
       else if (istep+(ns_saved-1) == 2) then ! AB2
          if (debug) &
               write(6,'(1x,"  multistep scheme at nstep=",i0,1x,"AB2")') istep
          call ab2 (aomg, domg, diss_v)
          !call multistep_explicit(aomg, domg, fac_ab2bdf2)
          if (magneto) then
             call ab2 (apsi, dpsi, diss_b)
             if (di /= 0.) then
                call ab2 (abz, dbz, diss_bpar)
                call ab2 (avz, dvz, diss_vpar)
             end if
          end if
       else ! AB3
          if (debug) &
               write(6,'(1x,"  multistep scheme at nstep=",i0,1x,"AB3")') istep
          call ab3 (aomg, domg, diss_v)
          !call multistep_explicit(aomg, domg, fac_ab3bdf3)
          if (magneto) then
             call ab3 (apsi, dpsi, diss_b)
             if (di /= 0.) then
                call ab3 (abz, dbz, diss_bpar)
                call ab3 (avz, dvz, diss_vpar)
             end if
          end if
       end if

    end if
 
!!$    ! vorticity eqn convective term
!!$    call lapinv (aomg(:,:,2), aphi)
!!$    call get_vector (aphi, vx, vy)
!!$    call poisson_bracket (vx, vy, -aomg(:,:,2), rtmp1)
!!$
!!$    if (magneto) then
!!$       ! vorticity eqn magnetic term
!!$       call laplacian (apsi(:,:,2), acur)
!!$       call get_vector (apsi(:,:,2), bx, by)
!!$       call poisson_bracket (bx, by, acur, rtmp2)
!!$       rtmp1 = rtmp1 + rtmp2
!!$       ! induction eqn
!!$       call poisson_bracket (vx, vy, -apsi(:,:,2), rtmp2)
!!$       call xtok (rtmp2, dpsi(:,:,1))
!!$       if (istep < 3) then
!!$          call eulerf (apsi, dpsi(:,:,1))
!!$       else
!!$          call ab3 (apsi, dpsi)
!!$       end if
!!$    end if
!!$
!!$    ! step forward omg
!!$    call xtok (rtmp1, domg(:,:,1))
!!$    if (istep < 3) then
!!$       call eulerf (aomg, domg(:,:,1))
!!$    else
!!$       call ab3 (aomg, domg)
!!$    end if

  contains

    subroutine multistep_explicit (fa, dadt, msfac)
      use run_params, only: nthreads_ab3
      complex, dimension (:,:,0:), intent (inout) :: fa
      complex, dimension (:,:,:), intent (in) :: dadt
      type (scheme_factor), intent(in) :: msfac
      integer :: i, j

!$OMP PARALLEL NUM_THREADS(nthreads_ab3)
      do j=1,size(fa,2)
!$OMP DO SCHEDULE(static, 2) 
         do i=1,size(fa,1)
            fa(i,j,0) = msfac%a(0) * ( &
                 & - sum(msfac%a(1:nsmax)*fa(i,j,1:nsmax)) &
                 & + sum(msfac%b(1:nsmax)*dadt(i,j,1:nsmax)))
         end do
!$OMP END DO
      end do
!$OMP END PARALLEL
    end subroutine multistep_explicit

    subroutine eulerf (fa, dadt)
      !
      ! y^(*) - y_{n} = dN_{n}
      !
      complex, dimension (:,:,0:), intent (inout) :: fa
      complex, dimension (:,:), intent (in) :: dadt
      type (scheme_factor), pointer :: fac => null()

!!!      fa(:,:,0) = fa(:,:,1) + dtime(1) * dadt
!      fac => fac_feuler
      fac => fac_euler
!!!      fa(:,:,0) = fa(:,:,1) + fac%a(0) * (fac%b(1) * dadt)
      fa(:,:,0) = fac%a(0) * ( - fac%a(1)*fa(:,:,1) + fac%b(1)*dadt)

    end subroutine eulerf

    subroutine ab2 (fa, dadt, dift)
      ! pure AB2:
      !                     3          1
      !   y_{n+1} - y_{n} = - dN_{n} - - dN_{n-1}
      !                     2          2
      !
      ! AB2 with BDF2:
      !   3                   1
      !   - y^(*) - 2 y_{n} + - y_{n-1} = 2 dN_{n} - dN_{n-1}
      !   2                   2
      !
      complex, dimension (:,:,0:), intent (inout) :: fa
      complex, dimension (:,:,:), intent (in) :: dadt
      real, intent(in) :: dift(:,:)
      type (scheme_factor), pointer :: fac => null()

      if (sum(abs(dift)) < epsilon(0.0)) then
!!!         fa(:,:,0) = fa(:,:,1) + dtime(1) * ( 1.5*dadt(:,:,1) - 0.5*dadt(:,:,2) )
         fac => fac_ab2
      else
!!!         fa(:,:,0) = 4./3.*fa(:,:,1) - 1./3.*fa(:,:,2) + dtime(1) * 2. / 3. &
!!!              & * ( 2.*dadt(:,:,1) - dadt(:,:,2) )
         fac => fac_ab2bdf2
      end if

      fa(:,:,0) = fac%a(0) * ( - fac%a(1)*fa(:,:,1) - fac%a(2)*fa(:,:,2) &
           & + fac%b(1)*dadt(:,:,1) + fac%b(2)*dadt(:,:,2))

    end subroutine ab2

    subroutine ab3 (fa, dadt, dift)
      ! pure AB3:
      !                     23          4             5
      !   y_{n+1} - y_{n} = -- dN_{n} - - dN_{n-1} + -- dN_{n-1}
      !                     12          3            12
      ! AB3 with BDF3:
      !   11                   3           1
      !   -- y^(*) - 3 y_{n} + - y_{n-1} - - y_{n-2} = 3 dN_{n} - 3 dN_{n-1} + dN_{n-2}
      !    6                   2           3

      use run_params, only: nthreads_ab3
      use fft, only: nkx, nky
      complex, dimension (:,:,0:), intent (inout) :: fa
      complex, dimension (:,:,:), intent (in) :: dadt
      real, intent (in) :: dift(:,:)
      type (scheme_factor), pointer :: fac => null()
      integer :: ikx, iky

      if (sum(abs(dift)) < epsilon(0.0)) then
!!!         fa(:,:,0) = fa(:,:,1) + dtime(1) / 12. &
!!!              & * ( 23.*dadt(:,:,1) - 16.*dadt(:,:,2) + 5.*dadt(:,:,3) )
         fac => fac_ab3
      else
!!!         fa(:,:,0) = 18./11.*fa(:,:,1) - 9./11.*fa(:,:,2) + 2./11.*fa(:,:,3) + dtime(1) / 11. &
!!!              & * ( 18.*dadt(:,:,1) - 18.*dadt(:,:,2) + 6.*dadt(:,:,3) )
         fac => fac_ab3bdf3
      end if

! OpenMP multi-threading for AB3 calculation
! Chunk size 2 specified in OMP DO SCHEDULE is chosen from the fact that
! Intel SIMD computation (256 bits) operates 2 elements at a time for double
! precision complex numbers.  This may vary for different architectures.
! See section 3.2.3 in Miyagawa's master thesis (UEC, 2015).
!$OMP PARALLEL NUM_THREADS(nthreads_ab3)
      do iky=1, nky
!$OMP DO SCHEDULE(static, 2) 
         do ikx=1, nkx
            fa(ikx,iky,0) = fac%a(0) * ( - fac%a(1)*fa(ikx,iky,1) &
                 & - fac%a(2)*fa(ikx,iky,2) - fac%a(3)*fa(ikx,iky,3) + fac%b(1)*dadt(ikx,iky,1) &
                 & + fac%b(2)*dadt(ikx,iky,2) + fac%b(3)*dadt(ikx,iky,3))
         end do
!$OMP END DO
      end do
!$OMP END PARALLEL

    end subroutine ab3

  end subroutine advect

  subroutine nonlinear_terms
    
    use time_fields_vars, only: vx, vy, bx, by
    use time_fields_vars, only: aomg, domg
    use time_fields_vars, only: dpsi, acur
    use time_fields_vars, only: avz, dvz, abz, dbz
    use run_params, only: nx, ny, magneto, di
    use four, only: poisson_bracket
    use fft, only: xtok
    
    real, dimension (:,:), allocatable :: rtmp1, rtmp2
      
    allocate (rtmp1(ny+1,nx+1), rtmp2(ny+1,nx+1))

    ! vorticity
    call poisson_bracket (vx, vy, -aomg(:,:,1), rtmp1)
    if (magneto) then
       call poisson_bracket (bx, by, acur, rtmp2)
    else
       rtmp2 = 0.0
    end if
    call xtok (rtmp1 + rtmp2, domg(:,:,1))

    if (magneto) then
       ! flux
       ! bx and by are already known, so poisson bracket rtmp1 can be directly calculated
       !       call poisson_bracket (vx, vy, -apsi(:,:,2), rtmp1)
       rtmp1 = vx * by - vy * bx
       if (di /= 0.) then
          call poisson_bracket (bx, by, -di*abz(:,:,1), rtmp2)
       else
          rtmp2 = 0.
       end if
       call xtok (rtmp1 + rtmp2, dpsi(:,:,1))

       if (di /= 0.) then
          ! Bz
          call poisson_bracket (vx, vy, -abz(:,:,1), rtmp1)
          call poisson_bracket (bx, by, avz(:,:,1)+di*acur, rtmp2)
          call xtok (rtmp1 + rtmp2, dbz(:,:,1))

          ! Vz
          call poisson_bracket (vx, vy, -avz(:,:,1), rtmp1)
          call poisson_bracket (bx, by, abz(:,:,1), rtmp2)
          call xtok (rtmp1 + rtmp2, dvz(:,:,1))
       endif
    end if

    deallocate (rtmp1, rtmp2)

  end subroutine nonlinear_terms

  subroutine dissipate (istep)

    use time_fields_vars, only: aomg, apsi, avz, abz
    use time_fields_vars, only: apsi_eq, abz_eq
    use time_fields_vars, only: diss_v, diss_b, diss_bpar, diss_vpar
    use run_params, only: magneto, di
    use save_restore, only: ns_saved

    integer, intent (in) :: istep

    if(eqzip) then
       if(magneto) then
          apsi(:,:,0)=apsi(:,:,0)-apsi_eq(:,:)
          if (di /= 0.) then
             abz(:,:,0)=abz(:,:,0)-abz_eq(:,:)
          endif
       endif
    endif

    if (new_method) then

       call multistep_implicit (aomg, diss_v, fac_omg)
       if (magneto) then
          call multistep_implicit (apsi, diss_b, fac_psi)
          if (di /= 0.) then
             call multistep_implicit (abz, diss_bpar, fac_bz)
             call multistep_implicit (avz, diss_vpar, fac_vz)
          end if
       end if

    else

       if (istep+(ns_saved-1) == 1) then ! Backward Euler
          if (debug) &
               write(6,'(1x,"  multistep scheme at nstep=",i0,1x"Euler")') istep
          call eulerb (aomg(:,:,0), diss_v)
          if (magneto) then
             call eulerb (apsi(:,:,0), diss_b)
             if (di /= 0.) then
                call eulerb (abz(:,:,0), diss_bpar)
                call eulerb (avz(:,:,0), diss_vpar)
             end if
          end if
       else if (istep+(ns_saved-1) == 2) then ! BDF2
          if (debug) &
               write(6,'(1x,"  multistep scheme at nstep=",i0,1x,"BDF2")') istep
          call bdf2 (aomg, diss_v)
          if (magneto) then
             call bdf2 (apsi, diss_b)
             if (di /= 0.) then
                call bdf2 (abz, diss_bpar)
                call bdf2 (avz, diss_vpar)
             end if
          end if
       else ! BDF3
          if (debug) &
               write(6,'(1x,"  multistep scheme at nstep=",i0,1x,"BDF3")') istep
          call bdf3 (aomg, diss_v)
          if (magneto) then
             call bdf3 (apsi, diss_b)
             if (di /= 0.) then
                call bdf3 (abz, diss_bpar)
                call bdf3 (avz, diss_vpar)
             end if
          end if
       end if

    end if

    if(eqzip) then
       if(magneto) then
          apsi(:,:,0)=apsi(:,:,0)+apsi_eq(:,:)
          if (di /= 0.) then
             abz(:,:,0)=abz(:,:,0)+abz_eq(:,:)
          endif
       endif
    endif

  contains

    subroutine multistep_implicit (fa, dift, msfac)
      complex, dimension (:,:,0:), intent (inout) :: fa
      real, intent (in) :: dift(:,:)
      type (scheme_factor), intent(in) :: msfac

      fa(:,:,0) = fa(:,:,0) / (1. + msfac%a(0) * msfac%b(0) * dift(:,:))
      
    end subroutine multistep_implicit

    subroutine eulerb (fa, dift)
      !
      ! y_{n+1} - y^{*} = dL_{n+1}
      ! 
      complex, dimension (:,:), intent (inout) :: fa
      real, intent (in) :: dift(:,:)
      type (scheme_factor), pointer :: fac => null()

! TT: return when dissipation is zero
      if (sum(abs(dift)) < epsilon(0.0)) return
! <TT

!      fac => fac_beuler
      fac => fac_euler
      fa = fa / (1. + fac%a(0) * fac%b(0) * dift(:,:))

    end subroutine eulerb

    subroutine bdf2 (fa, dift)
      ! 3                     1
      ! - y_{n+1} - 2 y_{n} + - y_{n-1} = dL_{n+1} 
      ! 2                     2
      !
      !                        + 2 dN_{n} - dN_{n-1}
      !
      !                        2
      ! ---> y_{n+1} - y^{*} = - dL_{n+1}
      !                        3
      complex, dimension (:,:,0:), intent (inout) :: fa
      real, intent (in) :: dift(:,:)
      type (scheme_factor), pointer :: fac => null()

! TT: return when dissipation is zero
      if (sum(abs(dift)) < epsilon(0.0)) return
! <TT

!      fac => fac_bdf2
      fac => fac_ab2bdf2
      fa(:,:,0) = fa(:,:,0) / (1. + fac%a(0) * fac%b(0) * dift(:,:))

    end subroutine bdf2

    subroutine bdf3 (fa, dift)
      ! 11                     3           1
      ! -- y_{n+1} - 3 y_{n} + - y_{n-1} - - y_{n-2} = dL_{n+1} 
      !  6                     2           3
      !
      !                        + 3 dN_{n} - 3 dN_{n-1} + dN_{n-2}
      !
      !                         6
      ! ---> y_{n+1} - y^{*} = -- dL_{n+1}
      !                        11
      complex, dimension (:,:,0:), intent (inout) :: fa
      real, intent (in) :: dift(:,:)
      type (scheme_factor), pointer :: fac => null()

! TT: return when dissipation is zero
      if (sum(abs(dift)) < epsilon(0.0)) return
! <TT

!      fac => fac_bdf3
      fac => fac_ab3bdf3
      fa(:,:,0) = fa(:,:,0) / (1. + fac%a(0) * fac%b(0) * dift(:,:))

    end subroutine bdf3

  end subroutine dissipate

  subroutine init_forcing
    use four, only: kperp2
    use four, only: integrate
    use constants, only: pi
    use time_fields_vars, only: gctmp1

    ! drv_norm -- integral of exp(-((k-k_f)/c)**2) (should be c*Erfc(-k_f/c)*2/sqrt(pi))
    gctmp1(:,:)=0.
    where(kperp2 /= 0. .and. abs(sqrt(kperp2(:,:))-drv_k) < drv_krange_sigma*drv_krange)
       gctmp1(:,:)=exp(-((sqrt(kperp2(:,:))-drv_k)/drv_krange)**2)/sqrt(kperp2(:,:))
    end where
    drv_norm=2./integrate(gctmp1)
   
  end subroutine init_forcing
  
  subroutine set_forcing
    !
    ! random forcing on omg and psi
    !
    ! - drv_option = 'random' | 'constpow'
    !  * Random Phase mode 'random'
    !    drv_amp * exp(I*2pi*[0,1])
    !  * Constant Power Input mode 'constpow'
    !    2D version of the method developed by Alvelius PoF (1999)
    !  * Test mode 'test'
    !    put drv_amp to (0,1) mode only
    !
    ! - Parameters
    !  * drv_amp(2) : amplitude for omg and psi
    !  * drv_k : modes of k +- k_range is driven
    !  * drv_k_range : 
    ! 
    use time_fields_vars, only: dtime
    use time_fields_vars, only: acur, aphi
    use time_fields_vars, only: drive_v, drive_b
    use time_fields_vars, only: grtmp1
    use ran, only: ranf
    use constants, only: zi, pi
    use fft, only: nkx, nkxh, nky
    use four, only: kperp2
    use run_params, only: magneto
    use four, only: l2square, integrate
    real :: ang1(nkx,nky)
    real :: rms, corrsum
    integer :: it, ik
    real, parameter :: small=1.e-8

    if (drv_amp(1) /= 0.) then

       drive_v(:,:,2:nsmax) = drive_v(:,:,1:nsmax-1)
       
       do it=1,nkx
          do ik=1,nky
             ang1(it,ik)=2.*pi*ranf()
          end do
       end do

       if (drv_switch == drvopt_random) then
          where(abs(sqrt(kperp2(:,:))-drv_k) < drv_krange)
             drive_v(:,:,1)=exp(zi*ang1(:,:))
          end where
          ! reality
          drive_v(nkxh+1:nkx,1,1) = conjg(drive_v(nkxh:2:-1,1,1)) ! reality condition
          rms=l2square(drive_v(:,:,1))
          drive_v(:,:,1)=drv_amp(1)*drive_v(:,:,1)/sqrt(rms)
       else if (drv_switch == drvopt_constpow) then
          grtmp1(:,:)=0.
          where (cabs(aphi(:,:)) > 0.)
             grtmp1(:,:)=atan( &
                  & (-sin(ang1(:,:))*real(aphi(:,:))+cos(ang1(:,:))*aimag(aphi(:,:))) / &
                  & (sin(ang1(:,:))*aimag(aphi(:,:))+cos(ang1(:,:))*real(aphi(:,:))) )
          end where

          where(kperp2 /= 0. .and. abs(sqrt(kperp2(:,:))-drv_k) < drv_krange_sigma*drv_krange)
             drive_v(:,:,1)= zi * exp(zi*(ang1(:,:)+grtmp1(:,:))) &
                  & * sqrt( &
                  &         exp(-((sqrt(kperp2(:,:))-drv_k)/drv_krange)**2) &
                  &         * sqrt(kperp2(:,:))*drv_norm*drv_amp(1)/dtime(1) )
          end where

          corrsum=integrate(drive_v(:,:,1),aphi)
          if (corrsum > small) write(6,*) 'v corr',corrsum
       else if (drv_switch == drvopt_test) then
          drive_v(1,2,1)=drv_amp(1)
       end if
       
       drive_v(nkxh+1:nkx,1,1) = conjg(drive_v(nkxh:2:-1,1,1)) ! reality condition
!!!       write(6,*) 'v corr',sum(real(conjg(drive_v)*aphi))

    end if

    if (magneto .and. drv_amp(2) /= 0.) then

       drive_b(:,:,2:nsmax) = drive_b(:,:,1:nsmax-1)
       
       do it=1,nkx
          do ik=1,nky
             ang1(it,ik)=2.*pi*ranf()
          end do
       end do

       if (drv_switch == drvopt_random) then
          where(abs(sqrt(kperp2(:,:))-drv_k) < drv_krange)
             drive_b(:,:,1)=exp(zi*ang1(:,:))
          end where
          ! reality
          drive_b(nkxh+1:nkx,1,1) = conjg(drive_b(nkxh:2:-1,1,1)) ! reality condition
          rms=l2square(drive_b(:,:,1))
          drive_b(:,:,1)=drv_amp(2)*drive_b(:,:,1)/sqrt(rms)
       else if (drv_switch == drvopt_constpow) then
          grtmp1(:,:)=0.
          where (cabs(acur(:,:)) > 0.)
             grtmp1(:,:)=atan( &
                  & (-sin(ang1(:,:))*real(acur(:,:))+cos(ang1(:,:))*aimag(acur(:,:))) / &
                  & (sin(ang1(:,:))*aimag(acur(:,:))+cos(ang1(:,:))*real(acur(:,:))) )
          end where

          where(kperp2 /= 0. .and. abs(sqrt(kperp2(:,:))-drv_k) < drv_krange_sigma*drv_krange)
             drive_b(:,:,1)= (-zi) * exp(zi*(ang1(:,:)+grtmp1(:,:))) &
                  & * sqrt( &
                  &         exp(-((sqrt(kperp2(:,:))-drv_k)/drv_krange)**2) &
                  &         / sqrt(kperp2(:,:))*drv_norm*drv_amp(2)/dtime(1) ) / sqrt(kperp2(:,:))
          end where

          corrsum=integrate(drive_b(:,:,1),acur)
          if (corrsum > small) write(6,*) 'b corr',corrsum
       else if (drv_switch == drvopt_test) then
          drive_b(1,2,1)=drv_amp(2)
       end if
       
       drive_b(nkxh+1:nkx,1,1) = conjg(drive_b(nkxh:2:-1,1,1)) ! reality condition
!!!       write(6,*) 'b corr',sum(real(conjg(drive_b)*acur))
       
    end if

  end subroutine set_forcing

  subroutine add_forcing(rkg_mode)
    use time_fields_vars, only: aomg, apsi
    use time_fields_vars, only: drive_v, drive_b
    use time_fields_vars, only: dtime
    use run_params, only: magneto
    implicit none
    logical, intent (in), optional :: rkg_mode
    logical :: rkg_loc
    integer :: i

    rkg_loc=.false.
    if (present(rkg_mode)) rkg_loc=rkg_mode

    if (drv_amp(1) /= 0.) then
       if (rkg_loc) then
          aomg(:,:,0) = aomg(:,:,0) + drive_v(:,:,1)*dtime(1)
       else
          do i=1,nsmax
             aomg(:,:,0) = aomg(:,:,0) + fac_omg%c(i)*drive_v(:,:,i)
          end do
       end if
    end if

    if (drv_amp(2) /= 0. .and. magneto) then
       if (rkg_loc) then
          apsi(:,:,0) = apsi(:,:,0) + drive_b(:,:,1)*dtime(1)
       else
          do i=1,nsmax
             apsi(:,:,0) = apsi(:,:,0) + fac_psi%c(i)*drive_b(:,:,i)
          end do
       end if
    end if
    
  end subroutine add_forcing
  
  subroutine dtupdate
    use time_fields_vars, only: dtime
    use time_fields_vars, only: vx, vy, bx, by
    use time_fields_vars, only: vex, vey
    use time_fields_vars, only: aphi, abz
    use four, only: delx, dely
    use four, only: get_vector
    use run_params, only: magneto, di
    use run_params, only: nx, ny
    real, parameter :: dtmargin=0.2
    
    dt_cfl = min(delx/maxval(vx),dely/maxval(vy))
    if (magneto) then
       dt_cfl = min(dt_cfl,min(delx/maxval(bx),dely/maxval(by)))
       if (di /= 0.) then
          call get_vector(aphi-di*abz(:,:,1), vex, vey)
          dt_cfl = min(dt_cfl,min(delx/maxval(vex),dely/maxval(vey)))
       end if
    end if
    dt_cfl = dtmargin * dt_cfl
    dtime(2:nsmax)=dtime(1:nsmax-1)
    if (dt_cfl < dtime(1)) then
       dtime(1)=dtime(1)*.5
       write(stdout_unit,'(1x,a,es10.3,a,es10.3)') 'Change time step to ',dtime(1),' : dt_clf = ',dt_cfl
    endif
    call update_scheme_factor
  end subroutine dtupdate

  subroutine init_cond

    use time_fields_vars, only: aomg, aphi, apsi, acur
    use time_fields_vars, only: aomg_eq, aphi_eq, apsi_eq, acur_eq
    use time_fields_vars, only: avz, abz
    use time_fields_vars, only: vx, vy, bx, by
    use constants, only: pi, zi
    use ran, only: ranf
    use fft, only: nkx, nkxh, nky, xtok, ktox
    use four, only: lx, ly, x, y, laplacian, get_vector
    use four, only: kperp2
    use run_params, only: nx, ny, magneto, di
    use file_utils, only: get_unused_unit
    use save_restore, only: restore_status
    use time_fields_vars, only: is_equil

    real, dimension (:,:), allocatable :: phiyx, psiyx
    real, dimension (:,:), allocatable :: omgyx, curyx
    real, dimension (:,:), allocatable :: vzyx, bzyx
    real :: psi_x(1:nx+1)
    integer :: i,j, mode
    real :: avg_phi, avg_psi
    real :: xmax, bymax, xa, xl1, xl2
    real :: ff
    integer :: unit_phi, unit_psi, unit_hall

    real :: spectrum, k_cutoff, klog_min, klog_max
    real, parameter :: scutoff=0.5
    integer, parameter :: spow=8
    
    !allocate
    allocate (phiyx(ny+1,nx+1), psiyx(ny+1,nx+1))
    allocate (omgyx(ny+1,nx+1), curyx(ny+1,nx+1))
    allocate (vzyx(ny+1,nx+1), bzyx(ny+1,nx+1))

    if (init_switch == initopt_restart) then
       call restore_status
       return
    end if

!!! equilibrium
    if(debug) write(stdout_unit,*) 'Set equilibrium field'
    select case (init_switch)
    case(initopt_ot)
       phiyx(1:ny+1,1:nx+1)=0.
       if(magneto) psiyx(1:ny+1,1:nx+1)=0.
    case(initopt_zero)
       phiyx(1:ny+1,1:nx+1)=0.
       if(magneto) psiyx(1:ny+1,1:nx+1)=0.
    case(initopt_recp)
       !
       !              psi_00
       ! psi(x) = -------------- * S(x)
       !           cosh(x/a)**2
       !
       phiyx(1:ny+1,1:nx+1)=0.
       if(magneto) then
          psi_x(1:nx+1)= &
               & 1./(cosh((x(1:nx+1)-.5*lx)/prof_width)**2) * ( &
               & tanh(2.*pi/lx* x(1:nx+1)    )**2 + & 
               & tanh(2.*pi/lx*(x(1:nx+1)-lx))**2 - &
               & tanh(2.*pi)**2 ) / (2.*tanh(pi)**2-tanh(2.*pi)**2)
          psiyx(1:ny+1,1:nx+1)=spread(psi_x(1:nx+1),1,ny+1)
          if(normalize_b) then
             xmax=.5*prof_width*log(2.+sqrt(3.))+.5*lx
             xmax=get_xmax(xmax)
             xa=(xmax-.5*lx)/prof_width
             xl1=2.*pi/lx*xmax
             xl2=xl1-2.*pi
             bymax=(-2./prof_width*sinh(xa)/cosh(xa)**3* &
                  & (tanh(xl1)**2+tanh(xl2)**2-tanh(2.*pi)**2) &
                  & + 1./cosh(xa)**2*4.*pi/lx* &
                  & (sinh(xl1)/cosh(xl1)**3+sinh(xl2)/cosh(xl2)**3) ) &
                  & / (2.*tanh(pi)**2-tanh(2.*pi)**2)
             psi_eq_amp=psi_eq_amp/abs(bymax)
          endif
       endif
    case(initopt_recs)
       !
       !                 2 pi n
       ! psi(x) = - cos(-------- x)
       !                   Lx
       !
       phiyx(1:ny+1,1:nx+1)=0.
       if(magneto) then
          psi_x(1:nx+1)=-cos(prof_mode*2.*pi/lx*x(1:nx+1))
          psiyx(1:ny+1,1:nx+1)=spread(psi_x(1:nx+1),1,ny+1)
          if(normalize_b) then
             psi_eq_amp=psi_eq_amp/(prof_mode*2.*pi/lx)
          endif
       endif
    case(initopt_rech)
       ! singular surfaces at x=Lx/4 (negative slope), 3Lx/4(positive slope)
       ! note that periodicity is bad if a is large
       !
       !                  x- Lx/4           x-3Lx/4
       ! psi(x) = ln cosh ------- - ln cosh ------- - ff x
       !                     a                 a
       !
       phiyx(1:ny+1,1:nx+1)=0.
       if(magneto) then
          ff=.5*(tanh(.25*lx/prof_width)+tanh(.75*lx/prof_width))
          psi_x(1:nx+1)= log( &
               & cosh((x(1:nx+1)-.25*lx)/prof_width)/ &
               & cosh((x(1:nx+1)-.75*lx)/prof_width)) - &
               & ff/prof_width*x(1:nx+1)
          psiyx(1:ny+1,1:nx+1)=spread(psi_x(1:nx+1),1,ny+1)
          if(normalize_b) then
             psi_eq_amp=psi_eq_amp*prof_width
          endif
       endif
    end select
    ! subtract (0,0) mode
    avg_phi = sum(phiyx(1:ny,1:nx)) / (nx*ny)
    phiyx(1:ny+1,1:nx+1)=phiyx(1:ny+1,1:nx+1)-avg_phi
    if(magneto) then
       avg_psi = sum(psiyx(1:ny,1:nx)) / (nx*ny)
       psiyx(1:ny+1,1:nx+1)=psiyx(1:ny+1,1:nx+1)-avg_psi
    endif

    ! amplitude
    phiyx(1:ny+1,1:nx+1)=phi_eq_amp*phiyx(1:ny+1,1:nx+1)
    if(magneto) psiyx(1:ny+1,1:nx+1)=psi_eq_amp*psiyx(1:ny+1,1:nx+1)

    ! fourier
    if (is_equil) then
       if(sum(phiyx(1:ny+1,1:nx+1)**2) /= 0.) then
          call xtok (phiyx, aphi_eq)
          call laplacian (aphi_eq, aomg_eq)
       else
          aphi_eq(:,:)=cmplx(0.,0.)
          aomg_eq(:,:)=cmplx(0.,0.)
       end if
       
       if (magneto) then
          if(sum(psiyx(1:ny+1,1:nx+1)**2) /= 0.) then
             call xtok (psiyx, apsi_eq)
             call laplacian (apsi_eq, acur_eq)
          else
             apsi_eq(:,:)=cmplx(0.,0.)
             acur_eq(:,:)=cmplx(0.,0.)
          endif
       end if
    end if

    phiyx(:,:)=0.
    if(magneto) psiyx(:,:)=0.

!!! perturbations
!!! sum_{# of modes} modeamp*e^(i 2pi/Lx modenum x)
    if(debug) write(stdout_unit,*) 'Add perturbation field'
    do mode=1,no_pt_modes
       do i=1,nx+1
          do j=1,ny+1
             phiyx(j,i)=phiyx(j,i)+ &
                  & real(phi_pt_modeamp(mode)*exp(zi* &
                  & ( 2.*pi/lx*phi_pt_modenum(1,mode)*x(i) &
                  & + 2.*pi/ly*phi_pt_modenum(2,mode)*y(j) )))
          end do
       end do
       if(magneto) then
          do i=1,nx+1
             do j=1,ny+1
                psiyx(j,i)=psiyx(j,i)+ &
                     & real(psi_pt_modeamp(mode)*exp(zi* &
                     & ( 2.*pi/lx*psi_pt_modenum(1,mode)*x(i) &
                     & + 2.*pi/ly*psi_pt_modenum(2,mode)*y(j) )))
             end do
          end do
       endif
    end do

    ! amplitude
    phiyx(1:ny+1,1:nx+1)=phi_pt_amp*phiyx(1:ny+1,1:nx+1)
    if(magneto) psiyx(1:ny+1,1:nx+1)=psi_pt_amp*psiyx(1:ny+1,1:nx+1)

    call xtok (phiyx, aphi)
    ! add noise
    if (noise(1) /= 0.0) then
       ! ky=0 mode separately
       do i=2, nkxh
          aphi(i,1) = aphi(i,1) + noise(1) * cmplx(ranf()-0.5,ranf()-0.5)
       end do
       ! reality condition
       aphi(nkxh+1:nkx,1) = conjg(aphi(nkxh:2:-1,1))
       do j=2, nky
          do i=1, nkx
             aphi(i,j) = aphi(i,j) + noise(1) * cmplx(ranf()-0.5,ranf()-0.5)
          end do
       end do
    end if
    ! add different type of noise: random phase with given spectrum
    ! almost constant energy, and high-k cutoff
    if (noise_sp(1) /= 0.0) then
       klog_min=log(sqrt(minval(kperp2,kperp2 /= 0.)))
       klog_max=log(sqrt(maxval(kperp2)))
       k_cutoff=exp((klog_max-klog_min)*scutoff+klog_min)
       do j=1, nky
          do i=1, nkx
             if (kperp2(i,j) == 0.) cycle
             spectrum=k_cutoff**spow/sqrt(k_cutoff**(2*spow)+kperp2(i,j)**spow)/kperp2(i,j)**0.75
             aphi(i,j) = aphi(i,j) + noise_sp(1) * spectrum * exp(zi*2.*pi*ranf())
          end do
       end do
       ! reality condition
       aphi(nkxh+1:nkx,1) = conjg(aphi(nkxh:2:-1,1))
    end if
    
    call laplacian (aphi, aomg(:,:,0))
    call get_vector (aphi, vx, vy)

    if (magneto) then
       call xtok (psiyx, apsi(:,:,0))

       if (noise(2) /= 0.0) then
          ! ky=0 mode separately
          do i=2, nkxh
             apsi(i,1,0) = apsi(i,1,0) + noise(2) * cmplx(ranf()-0.5,ranf()-0.5)
          end do
          ! reality condition
          apsi(nkxh+1:nkx,1,0) = conjg(apsi(nkxh:2:-1,1,0))
          do j=2, nky
             do i=1, nkx
                apsi(i,j,0) = apsi(i,j,0) + noise(2) * cmplx(ranf()-0.5,ranf()-0.5)
             end do
          end do
       end if
       if (noise_sp(2) /= 0.0) then
          klog_min=log(sqrt(minval(kperp2,kperp2 /= 0.)))
          klog_max=log(sqrt(maxval(kperp2)))
          k_cutoff=exp((klog_max-klog_min)*scutoff+klog_min)
          do j=1, nky
             do i=1, nkx
                if (kperp2(i,j) == 0.) cycle
                spectrum=k_cutoff**spow/sqrt(k_cutoff**(2*spow)+kperp2(i,j)**spow)/kperp2(i,j)**0.75
                apsi(i,j,0) = apsi(i,j,0) + noise_sp(2) * spectrum * exp(zi*2.*pi*ranf())
             end do
          end do
          ! reality condition
          apsi(nkxh+1:nkx,1,0) = conjg(apsi(nkxh:2:-1,1,0))
       end if
       
       call laplacian (apsi(:,:,0), acur)
       call get_vector (apsi(:,:,0), bx, by)
    end if

    if (is_equil) then
       aphi(:,:)  =aphi(:,:)  +aphi_eq(:,:)
       aomg(:,:,0)=aomg(:,:,0)+aomg_eq(:,:)
    end if
    call get_vector (aphi, vx, vy)

    if(magneto) then
       if (is_equil) then
          apsi(:,:,0)=apsi(:,:,0)+apsi_eq(:,:)
          acur(:,:)  =acur(:,:)  +acur_eq(:,:)
       end if
       call get_vector (apsi(:,:,0), bx, by)
    endif

    if(debug) then
       call ktox(aphi,phiyx)
       call ktox(aomg(:,:,0),omgyx)
       call get_unused_unit(unit_phi)
       open(unit_phi,file='input_check_phi.dat')
       do i=1,nx+1
          do j=1,ny+1
             write(unit_phi,'(6d20.12)') x(i),y(j), &
                  phiyx(j,i),omgyx(j,i),vx(j,i),vy(j,i)
          end do
          write(unit_phi,*)
       end do
       close(unit_phi)

       if(magneto) then
          call ktox(apsi(:,:,0),psiyx)
          call ktox(acur,curyx)
          call get_unused_unit(unit_psi)
          open(unit_psi,file='input_check_psi.dat')
          do i=1,nx+1
             do j=1,ny+1
                write(unit_psi,'(6d20.12)') x(i),y(j), &
                     psiyx(j,i),curyx(j,i),bx(j,i),by(j,i)
             end do
             write(unit_psi,*)
          end do
          close(unit_psi)

          if (di /= 0.) then
             call ktox(avz(:,:,0),vzyx)
             call ktox(abz(:,:,0),bzyx)
             call get_unused_unit(unit_hall)
             open(unit_hall,file='input_check_hall.dat')
             do i=1,nx+1
                do j=1,ny+1
                   write(unit_hall,'(6d20.12)') x(i),y(j), &
                        vzyx(j,i),bzyx(j,i)
                end do
                write(unit_hall,*)
             end do
             close(unit_hall)
          endif

       endif

    endif

    deallocate (phiyx, psiyx)
    deallocate (omgyx, curyx)
    deallocate (vzyx, bzyx)

  end subroutine init_cond

  subroutine read_parameters
    
    use file_utils, only: input_unit_exist
    use text_options, only: text_option, get_option_value
    use time_fields_vars, only: init_equil_vars, is_equil
    use four, only: lx
    use run_params, only: nstep

    logical :: ex
    integer :: unit

    type (text_option), parameter :: initopts(7) = (/ &
         & text_option('none',initopt_none), &
         & text_option('restart',initopt_restart), &
         & text_option('OT',initopt_ot), &
         & text_option('Rec_sin',initopt_recs), &
         & text_option('Rec_Porcelli',initopt_recp), &
         & text_option('Rec_Harris',initopt_rech), &
         & text_option('zero',initopt_zero) /)
    type (text_option), parameter :: drvopts(3) = (/ &
         & text_option('random',drvopt_random), &
         & text_option('constpow',drvopt_constpow), &
         & text_option('test',drvopt_test) /)

    namelist /tint_knobs/ nstep_rkg, ndtcheck, init_option, &
         & phi_eq_amp, psi_eq_amp, prof_width, prof_mode, normalize_b, &
         & phi_pt_amp, psi_pt_amp, phi_pt_modeamp, psi_pt_modeamp, &
         & phi_pt_modenum, psi_pt_modenum, &
         & eqzip, noise, noise_sp, &
         & drv_option, drv_amp, drv_k, drv_krange, &
         & nonlin

    unit = input_unit_exist('tint_knobs',ex)
    if(ex) read(unit,tint_knobs)

    call get_option_value(init_option, initopts, init_switch, &
         & error_unit(),"init_option in tint_knobs")
    call get_option_value(drv_option, drvopts, drv_switch, &
         & error_unit(),"drv_option in tint_knobs")

    if (nstep_rkg == -1 .or. nstep_rkg > nstep) then
       write(stdout_unit,*) 'INFO: use RKG advance'
       nstep_rkg = nstep
    else if (nstep_rkg < -2) then
       nstep_rkg = -2
    end if
    
    ! if prof_width < 0, |prof_width| gives ratio to lx
    if(prof_width < 0.) prof_width=-prof_width*lx

    ! check conditions
    select case(init_switch)
    case(initopt_none)
       write(stdout_unit,*) 'ERROR: Initial condition is not selected'
       write(stdout_unit,*) 'bye...'
       stop
    case(initopt_restart)
       if(debug) write(stdout_unit,*) 'Restart'
    case(initopt_ot)
       if(debug) write(stdout_unit,*) 'Orszag-Tang problem'
    case(initopt_recp)
       is_equil = .true.
       call init_equil_vars
       if(debug) write(stdout_unit,*) 'Reconnection [eq=Porcelli]'
    case(initopt_recs)
       is_equil = .true.
       call init_equil_vars
       if(debug) write(stdout_unit,*) 'Reconnection [eq=sinusoidal]'
    case(initopt_rech)
       is_equil = .true.
       call init_equil_vars
       if(debug) write(stdout_unit,*) 'Reconnection [eq=double Harris]'
       if(prof_width/lx > 0.03) &
            write(stdout_unit,*) 'WARNING: Profile width is too broad to be periodic'
    case(initopt_zero)
       if(debug) write(stdout_unit,*) 'Zero'
    end select

    select case(drv_switch)
    case(drvopt_random)
       if(debug) write(stdout_unit,*) 'Random Phase drive'
    case(drvopt_constpow)
       if(debug) write(stdout_unit,*) 'Const. Power drive'
    case(drvopt_test)
       if(debug) write(stdout_unit,*) 'test drive'
    case default
       if (sum(drv_amp) /= 0.) then
          write(stdout_unit,*) 'WARNING: driving term option is not selected, turns off'
          drv_amp=0.
       end if
    end select

  end subroutine read_parameters

  function get_xmax(xguess) result(xsol)
    real :: xsol
    real, intent(in) :: xguess
    real, parameter :: tol=1.e-20
    real :: xold
    integer :: i
    integer :: itmax=1000
    xold=xguess
    do i=1,itmax
       xsol=xold-f(xold)/fprime(xold)
       if(abs(xsol-xold) > tol) then
           xold=xsol
        else
           return
        endif
    end do
  contains
    function f(x)
      use constants, only: pi
      use four, only: lx
      real :: f
      real, intent(in) :: x
      real :: xa, xl1, xl2
      xa=(x-.5*lx)/prof_width
      xl1=2.*pi/lx*x
      xl2=xl1-2.*pi
      
      f= &
           & 2./prof_width**2*(cosh(2.*xa)-2.)/cosh(xa)**4* & ! f''
           & (tanh(xl1)**2+tanh(xl2)**2-tanh(2.*pi)**2) & ! g
           & + 2. * &
           & (-2./prof_width)*sinh(xa)/cosh(xa)**3* & ! f'
           & 4.*pi/lx*(sinh(xl1)/cosh(xl1)**3+sinh(xl2)/cosh(xl2)**3) & ! g'
           & + &
           & 1./cosh(xa)**2* & ! f
           & 8.*(pi/lx)**2*((2.-cosh(2.*xl1))/cosh(xl1)**4 & ! g''
           &               +(2.-cosh(2.*xl2))/cosh(xl2)**4)

    end function f
    function fprime(x)
      use constants, only: pi
      use four, only: lx
      real :: fprime
      real, intent(in) :: x
      real :: xa, xl1, xl2
      xa=(x-.5*lx)/prof_width
      xl1=2.*pi/lx*x
      xl2=xl1-2.*pi

      fprime = &
           & 8./prof_width**3*(2.*sinh(xa)-sinh(xa)**3)/cosh(xa)**5* & ! f''' 
           & (tanh(xl1)**2+tanh(xl2)**2-tanh(2.*pi)**2) & ! g
           & + 3. * &
           & 2./prof_width**2*(cosh(2.*xa)-2.)/cosh(xa)**4* & ! f''
           & 4.*pi/lx*(sinh(xl1)/cosh(xl1)**3+sinh(xl2)/cosh(xl2)**3) & ! g'
           & + 3. * &
           & (-2./prof_width)*sinh(xa)/cosh(xa)**3* & ! f'
           & 8.*(pi/lx)**2*((2.-cosh(2.*xl1))/cosh(xl1)**4 & ! g''
           &               +(2.-cosh(2.*xl2))/cosh(xl2)**4) &
           & + &
           & 1./cosh(xa)**2* & ! f
           & (-64.)*(pi/lx)**3 * ( & ! g'''
           & (2.*sinh(xl1)-sinh(xl1)**3)/cosh(xl1)**5 + &
           & (2.*sinh(xl2)-sinh(xl2)**3)/cosh(xl2)**5 )
    end function fprime
  end function get_xmax

  subroutine update_scheme_factor

    use time_fields_vars, only: dtime
    
    ! Euler
    fac_euler%a=0.; fac_euler%b=0.

    fac_euler%name = name_euler
    fac_euler%a(0)=dtime(1); fac_euler%a(1)=-1./dtime(1)
    fac_euler%b(0)=1.; fac_euler%b(1)=1.

    fac_euler%c=0.;
    fac_euler%c(1)=dtime(1)
!!$    ! Forward Euler
!!$    fac_feuler%a=0.; fac_feuler%b=0.
!!$
!!$    fac_feuler%name = name_feuler
!!$    fac_feuler%a(0)=dtime(1); fac_feuler%a(1)=-1./dtime(1)
!!$    fac_feuler%b(1)=1.
!!$
!!$    ! Backward Euler
!!$    fac_beuler%a=0.; fac_beuler%b=0.
!!$
!!$    fac_beuler%name = name_beuler
!!$    fac_beuler%a(0)=dtime(1); fac_beuler%a(1)=-1./dtime(1)
!!$    fac_beuler%b(0)=1.

    if (nsmax >= 2) then
       ! AB2
       fac_ab2%a=0.; fac_ab2%b=0.

       fac_ab2%name = name_ab2
       fac_ab2%a(0)=dtime(1); fac_ab2%a(1)=-1./dtime(1)
       fac_ab2%b(1)=.5*(dtime(1)+2.*dtime(2))/dtime(2)
       fac_ab2%b(2)=-.5*dtime(1)/dtime(2)

       fac_ab2%c=0.
       fac_ab2%c(1)=dtime(1)
       fac_ab2%c(2)=dtime(2)*(1.+fac_ab2%a(0)*fac_ab2%a(1))
       
!!$    ! BDF2
!!$    fac_bdf2%a=0.; fac_bdf2%b=0.
!!$
!!$    fac_bdf2%name = name_bdf2
!!$    fac_bdf2%a(0)=(dtime(1)*(dtime(1)+dtime(2)))/(2.*dtime(1)+dtime(2))
!!$    fac_bdf2%a(1)=-(1./dtime(1)+1./dtime(2))
!!$    fac_bdf2%a(2)=dtime(1)/((dtime(1)+dtime(2))*dtime(2))
!!$    fac_bdf2%b(0)=1.

       ! AB2BDF2
       fac_ab2bdf2%a=0.; fac_ab2bdf2%b=0.

       fac_ab2bdf2%name = name_ab2bdf2

       fac_ab2bdf2%a(0)=(dtime(1)*(dtime(1)+dtime(2)))/(2.*dtime(1)+dtime(2))
       fac_ab2bdf2%a(1)=-(1./dtime(1)+1./dtime(2))
       fac_ab2bdf2%a(2)=dtime(1)/((dtime(1)+dtime(2))*dtime(2))
       fac_ab2bdf2%b(0)=1.
       fac_ab2bdf2%b(1)=dtime(1)/dtime(2)+1.
       fac_ab2bdf2%b(2)=-dtime(1)/dtime(2)

       fac_ab2bdf2%c=0.
       fac_ab2bdf2%c(1)=dtime(1)
       fac_ab2bdf2%c(2)=dtime(2)*(1.+fac_ab2bdf2%a(0)*fac_ab2bdf2%a(1))
    end if

    if (nsmax >= 3) then
       ! AB3
       fac_ab3%a=0.; fac_ab3%b=0.

       fac_ab3%name = name_ab3
       fac_ab3%a(0)=dtime(1); fac_ab3%a(1)=-1./dtime(1);
       fac_ab3%b(1)=(2.*dtime(1)**2+3.*dtime(1)*(2.*dtime(2)+dtime(3)) &
            & +6.*dtime(2)*(dtime(2)+dtime(3))) / &
            & (6.*dtime(2)*(dtime(2)+dtime(3)))
       fac_ab3%b(2)=-(2.*dtime(1)**2+3.*dtime(1)*(dtime(2)+dtime(3))) / &
         & (6.*dtime(2)*dtime(3))
       fac_ab3%b(3)=(2.*dtime(1)**2+3.*dtime(1)*dtime(2)) / &
            & (6.*dtime(3)*(dtime(2)+dtime(3)))

       fac_ab3%c=0.
       fac_ab3%c(1)=dtime(1)
       fac_ab3%c(2)=dtime(2)*(1.+fac_ab3%a(0)*fac_ab3%a(1))
       fac_ab3%c(3)=dtime(3)*(1.+fac_ab3%a(0)*fac_ab3%a(1)+fac_ab3%a(0)*fac_ab3%a(2))
       
!!$    ! BDF3
!!$    fac_bdf3%a=0.; fac_bdf3%b=0.
!!$       
!!$    fac_bdf3%name = name_bdf3
!!$    fac_bdf3%a(0)=dtime(1)*(dtime(1)+dtime(2))*(dtime(1)+dtime(2)+dtime(3))/ &
!!$         & (3.*dtime(1)**2+(4.*dtime(2)+2.*dtime(3))*dtime(1)+dtime(2)*(dtime(2)+dtime(3)))
!!$    fac_bdf3%a(1)=-(dtime(1)+dtime(2))*(dtime(1)+dtime(2)+dtime(3))/(dtime(1)*dtime(2)*(dtime(2)+dtime(3)))
!!$    fac_bdf3%a(2)=dtime(1)*(dtime(1)+dtime(2)+dtime(3))/((dtime(1)+dtime(2))*dtime(2)*dtime(3))
!!$    fac_bdf3%a(3)=-dtime(1)*(dtime(1)+dtime(2))/((dtime(1)+dtime(2)+dtime(3))*(dtime(2)+dtime(3))*dtime(3))
!!$    fac_bdf3%b(0)=1.
    
       ! AB3BDF3
       fac_ab3bdf3%a=0.; fac_ab3bdf3%b=0.

       fac_ab3bdf3%name = name_ab3bdf3
       fac_ab3bdf3%a(0)=dtime(1)*(dtime(1)+dtime(2))*(dtime(1)+dtime(2)+dtime(3)) / &
            & (3.*dtime(1)**2+(4.*dtime(2)+2.*dtime(3))*dtime(1)+dtime(2)*(dtime(2)+dtime(3)))
       fac_ab3bdf3%a(1)=-(dtime(1)+dtime(2))*(dtime(1)+dtime(2)+dtime(3)) / &
            & (dtime(1)*dtime(2)*(dtime(2)+dtime(3)))
       fac_ab3bdf3%a(2)=dtime(1)*(dtime(1)+dtime(2)+dtime(3)) / &
            & ((dtime(1)+dtime(2))*dtime(2)*dtime(3))
       fac_ab3bdf3%a(3)=-dtime(1)*(dtime(1)+dtime(2)) / &
            & ((dtime(1)+dtime(2)+dtime(3))*(dtime(2)+dtime(3))*dtime(3))
       fac_ab3bdf3%b(0)=1.
       fac_ab3bdf3%b(1)=(dtime(1)+dtime(2))*(dtime(1)+dtime(2)+dtime(3)) / &
            & (dtime(2)*(dtime(2)+dtime(3)))
       fac_ab3bdf3%b(2)=-dtime(1)*(dtime(1)+dtime(2)+dtime(3)) / (dtime(2)*dtime(3))
       fac_ab3bdf3%b(3)=dtime(1)*(dtime(1)+dtime(2)) / ((dtime(2)+dtime(3))*dtime(3))

       fac_ab3bdf3%c=0.
       fac_ab3bdf3%c(1)=dtime(1)
       fac_ab3bdf3%c(2)=dtime(2)*(1.+fac_ab3bdf3%a(0)*fac_ab3bdf3%a(1))
       fac_ab3bdf3%c(3)=dtime(3)*(1.+fac_ab3bdf3%a(0)*fac_ab3bdf3%a(1)+fac_ab3bdf3%a(0)*fac_ab3bdf3%a(2))
    end if
    
    if (debug) then
       write(stdout_unit,'(1x,a15,8f11.3)') fac_euler%name,fac_euler%a,fac_euler%b
       write(stdout_unit,'(1x,a15,8f11.3)') fac_ab2%name,fac_ab2%a,fac_ab2%b
       write(stdout_unit,'(1x,a15,8f11.3)') fac_ab2bdf2%name,fac_ab2bdf2%a,fac_ab2bdf2%b
       write(stdout_unit,'(1x,a15,8f11.3)') fac_ab3%name,fac_ab3%a,fac_ab3%b
       write(stdout_unit,'(1x,a15,8f11.3)') fac_ab3bdf3%name,fac_ab3bdf3%a,fac_ab3bdf3%b
    endif

  end subroutine update_scheme_factor

!!$  subroutine select_scheme (name,msfac)
!!$    character (len=*), intent(in) :: name
!!$    type (scheme_factor), pointer :: msfac
!!$
!!$    select case (trim(name))
!!$    case(name_euler)
!!$       msfac => fac_euler
!!$       if (debug) write(stdout_unit,*) trim(msfac%name)
!!$    case(name_ab2)
!!$       msfac => fac_ab2
!!$       if (debug) write(stdout_unit,*) trim(msfac%name)
!!$    case(name_ab2bdf2)
!!$       msfac => fac_ab2bdf2
!!$       if (debug) write(stdout_unit,*) trim(msfac%name)
!!$    case(name_ab3)
!!$       msfac => fac_ab3
!!$       if (debug) write(stdout_unit,*) trim(msfac%name)
!!$    case(name_ab3bdf3)
!!$       msfac => fac_ab3bdf3
!!$       if (debug) write(stdout_unit,*) trim(msfac%name)
!!$    case default
!!$       stop 'Invalid Scheme'
!!$    end select
!!$  end subroutine select_scheme

end module tint
